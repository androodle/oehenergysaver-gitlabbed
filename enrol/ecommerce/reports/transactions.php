<?php
/**
 *-----------------------------------------------------------------------------
 * eCommerce Enrolment Plugin Transactions Report
 *-----------------------------------------------------------------------------
 * @package    enrol_ecommerce
 * @copyright  Androgogic Pty Ltd
 * @author     Androgogic Pty Ltd
 * @license    Commercial (http://www.androgogic.com)
 *
 * NOTE: navigation to this report is added via settings.php
 *       navigation is Site Admin > Reports > eCommerce Transactions Report
 *-----------------------------------------------------------------------------
 **/

require('../../../config.php');
require_once('../lib.php');
require_once($CFG->libdir.'/tablelib.php');

require_login();

$context = context_system::instance();
if (!is_siteadmin() && !has_capability('report/log:view', $context)) {
    redirect($CFG->wwwroot);
}

$download = optional_param('download', '', PARAM_ALPHA);
$tsort = optional_param('tsort', null, PARAM_RAW);

$PAGE->set_context(context_system::instance());
$PAGE->set_url('/enrol/ecommerce/reports/transactions.php');
$page_title = get_string('reportecommercetransactions', 'enrol_ecommerce');
$PAGE->set_title($page_title);
$PAGE->set_heading($page_title);
$PAGE->set_cacheable(true);

$table = new flexible_table('receipt');
$table->sortable(true, 'receipt');
$table->define_baseurl($CFG->wwwroot . '/enrol/ecommerce/reports/transactions.php');
$table->is_downloading($download, 'transactions_report', 'transactions_report');

if (!$table->is_downloading()) {
	echo $OUTPUT->header();
}

$table->define_columns(array(
	'course',
	'user',
	'payment_amount',
	'receipt',
	'payment_dt',
	'payment_status',
	'settlement_dt'
));
$table->define_headers(array(
	'Course/Module',
	'User',
	'Payment amount',
	'Receipt Number',
	'Payment Date',
	'Payment Status',
	'Settlement Date'
));

$table->set_attribute('class', 'generalbox enrol-ecommerce-receipts');
$table->setup();

// This is MySQL specific ordering
$sql = "select * from {enrol_ecommerce} order by unix_timestamp(str_to_date(created_dttm, '%d %M %Y %H:%i:%s')) desc";

$totalcount = $DB->count_records_select('enrol_ecommerce', '', array());
$table->totalrows = $totalcount;

// Turn OFF pagination - download doesn't work properly and misses pages
//$perpage = 100;
//$table->initialbars($totalcount > $perpage);
//$table->pagesize($perpage, $totalcount);

$transactions = $DB->get_records_sql(
    $sql,
    array(),
    $table->get_page_start(),
    $table->get_page_size()
);

foreach($transactions as $t) {
	$row = array();

    $course_name = $DB->get_field('course', 'shortname', array('id' => $t->courseid));
    $course_link = '<a href="' . "$CFG->wwwroot/course/view.php?id=$t->courseid" . '" target="_blank"/>' . $course_name . '</a>';

    $user_firstname = $DB->get_field('user', 'firstname', array('id' => $t->userid));
    $user_lastname = $DB->get_field('user', 'lastname', array('id' => $t->userid));
    $user_link = '<a href="' . "$CFG->wwwroot/user/view.php?id=$t->userid" . '" target="_blank"/>' . $user_firstname . ' ' . $user_lastname . '</a>';

    if (!$table->is_downloading()) {
    	$row[] = $course_link;
    }
    else {
    	$row[] = $course_name;
    }

    if (!$table->is_downloading()) {
    	$row[] = $user_link;
    }
    else {
    	$row[] = $user_firstname . ' ' . $user_lastname;
    }
    $row[] = '$' . number_format($t->payment_amount, 2);
    $row[] = $t->receipt_number;
    $row[] = $t->created_dttm;
    $row[] = $t->response_description;
    $row[] = date("d M Y", strtotime($t->settlement_dt));

    $table_rows[] = $row;
    //$table->add_data($row);
}

foreach($table_rows as $row) {
    $table->add_data($row);
}


$table->finish_output();

if (!$table->is_downloading()) {
	echo $OUTPUT->footer();
}
