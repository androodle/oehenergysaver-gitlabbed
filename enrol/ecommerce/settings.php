<?php

/**
 *-----------------------------------------------------------------------------
 * eCommerce enrolment plugin settings
 *-----------------------------------------------------------------------------
 * @package    enrol_ecommerce
 * @copyright  Androgogic Pty Ltd
 * @author     Androgogic Pty Ltd
 * @license    Commercial (http://www.androgogic.com)
 *-----------------------------------------------------------------------------
 **/

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {    

    // Defaults configuration heading
    $settings->add(new admin_setting_heading(
        'enrol_ecommerce_defaults', '', 
        get_string('defaults', 'enrol_ecommerce'))
    );
 
    // Payment Gateway Username
    $settings->add(new admin_setting_configtext(
        'enrol_ecommerce/defaultcost', 
        get_string('defaultcost', 'enrol_ecommerce'), 
        '', 0, PARAM_RAW, 4)
    );

    // Gateway configuration heading
    $settings->add(new admin_setting_heading(
        'enrol_ecommerce_gateway', '', 
        get_string('gatewaysettings', 'enrol_ecommerce'))
    );

    // Payment Gateway Username
    $settings->add(new admin_setting_configtext(
        'enrol_ecommerce/gatewayusername', 
        get_string('gatewayusername', 'enrol_ecommerce'), 
        '', '', PARAM_TEXT, 32)
    );
    
    // Payment Gateway Password
    $settings->add(new admin_setting_configtext(
        'enrol_ecommerce/gatewaypassword', 
        get_string('gatewaypassword', 'enrol_ecommerce'), 
        '', '', PARAM_TEXT, 32)
    );

    // Payment Gateway Business Code (QuickWeb specific)
    $settings->add(new admin_setting_configtext(
        'enrol_ecommerce/gatewaybusinesscode', 
        get_string('gatewaybusinesscode', 'enrol_ecommerce'), 
        '', '', PARAM_TEXT, 32)
    ); 

    // Payment Gateway notification email for any problems
    $settings->add(new admin_setting_configtext(
        'enrol_ecommerce/gatewaynotificationemail', 
        get_string('gatewaynotificationemail', 'enrol_ecommerce'), 
        '', '', PARAM_TEXT, 32)
    );

    // Payment Gateway GL account
    $settings->add(new admin_setting_configtext(
        'enrol_ecommerce/gatewayglaccount', 
        get_string('gatewayglaccount', 'enrol_ecommerce'), 
        '', '', PARAM_TEXT, 32)
    );

    // Payment Gateway Cash Journal
    $settings->add(new admin_setting_configtext(
        'enrol_ecommerce/gatewaycashjournal', 
        get_string('gatewaycashjournal', 'enrol_ecommerce'), 
        '', '', PARAM_TEXT, 32)
    );

    // Payment Gateway Community Code (QuickWeb specific)
    $settings->add(new admin_setting_configtext(
        'enrol_ecommerce/gatewaycommunitycode', 
        get_string('gatewaycommunitycode', 'enrol_ecommerce'), 
        '', '', PARAM_TEXT, 32)
    );

    // Payment Gateway Token Request URL (QuickWeb specific)
    $settings->add(new admin_setting_configtext(
        'enrol_ecommerce/gatewaytokenurl', 
        get_string('gatewaytokenurl', 'enrol_ecommerce'), 
        '', '', PARAM_TEXT, 128)
    );

    // Payment Gateway Handoff URL (QuickWeb specific)
    $settings->add(new admin_setting_configtext(
        'enrol_ecommerce/gatewayhandoffurl', 
        get_string('gatewayhandoffurl', 'enrol_ecommerce'), 
        '', '', PARAM_TEXT, 128)
    );

    // Defaults configuration heading
    $settings->add(new admin_setting_heading(
        'enrol_ecommerce_emailsettings', '', 
        get_string('invoiceemailsettings', 'enrol_ecommerce'))
    );

    // Invoice email from address
    $settings->add(new admin_setting_configtext(
        'enrol_ecommerce/invoiceemailfrom', 
        get_string('invoiceemailfrom', 'enrol_ecommerce'), 
        '', 
        get_string('invoiceemailfromdefault', 'enrol_ecommerce'), 
        PARAM_TEXT, 128)
    );
    
    // Invoice email from user
    $settings->add(new admin_setting_configtext(
        'enrol_ecommerce/invoiceemailfromuser', 
        get_string('invoiceemailfromuser', 'enrol_ecommerce'), 
        get_string('invoiceemailfromuserexplain', 'enrol_ecommerce'), 
        get_string('invoiceemailfromuserdefault', 'enrol_ecommerce'), 
        PARAM_TEXT, 128)
    );
    
    // Invoice email from address
    $settings->add(new admin_setting_configtext(
        'enrol_ecommerce/invoiceemailfrom', 
        get_string('invoiceemailfrom', 'enrol_ecommerce'), 
        get_string('invoiceemailfromexplain', 'enrol_ecommerce'), 
        get_string('invoiceemailfromdefault', 'enrol_ecommerce'), 
        PARAM_TEXT, 128)
    );

    // Invoice email subject
    $settings->add(new admin_setting_configtext(
        'enrol_ecommerce/invoiceemailsubject', 
        get_string('invoiceemailsubject', 'enrol_ecommerce'), 
        get_string('invoiceemailsubjectexplain', 'enrol_ecommerce'),
        get_string('invoiceemailsubjectdefault', 'enrol_ecommerce'), 
        PARAM_TEXT, 128)
    );

    // Invoice email message
    $settings->add(new admin_setting_configtextarea(
        'enrol_ecommerce/invoiceemailmessage', 
        get_string('invoiceemailmessage', 'enrol_ecommerce'), 
        get_string('invoiceemailmessageexplain', 'enrol_ecommerce'),
        get_string('invoiceemailmessagedefault', 'enrol_ecommerce'))
    );

}

// Navigation to ecommerce report under Site Admin > Reports > eCommerce Transactions Report
// Cheating a bit here giving the user access if they have report/log:view

$ADMIN->add(
    'reports',
    new admin_externalpage('reportecommercetransactions', get_string('reportecommercetransactions', 'enrol_ecommerce'),
    new moodle_url('/enrol/ecommerce/reports/transactions.php'),
    'report/log:view')
);