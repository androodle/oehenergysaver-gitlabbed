<?php

/**
 *-----------------------------------------------------------------------------
 * eCommerce enrolment gateway process - payment receipts
 *-----------------------------------------------------------------------------
 * @package    enrol_ecommerce
 * @copyright  Androgogic Pty Ltd
 * @author     Androgogic Pty Ltd
 * @license    Commercial (http://www.androgogic.com)
 *-----------------------------------------------------------------------------
 **/

require('../../config.php');
require_once('lib.php');
require_once($CFG->libdir.'/tablelib.php');

require_login();

// Process action (if relevant)

$paymentid = optional_param('paymentid', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_TEXT);
$userid = optional_param('userid', '', PARAM_INT);

// Security check
global $USER;
$context = context_user::instance($userid, MUST_EXIST);
if ($USER->id != $userid && !has_capability('enrol/ecommerce:manage', $context)) {
	redirect($CFG->wwwroot);
}

if (isset($action) && $action == 'send' && isset($paymentid) && $paymentid > 0) {
	redirect("invoice.php?paymentid=$paymentid");
}

$PAGE->set_url('/enrol/ecommerce/receipts.php');
$PAGE->set_title("Your enrolment receipts");
$PAGE->set_heading("Your enrolment receipts");
$PAGE->set_cacheable(true);

// Table of receipts for user payments

echo $OUTPUT->header();

echo '<div class="receipts">';

$table = new flexible_table('enrol_ecommerce_receipts');
$table->define_baseurl($CFG->wwwroot . '/enrol/ecommerce/receipts.php');
$table->define_columns(array(
	'module',
	'payment_date',
	'payment_amount',
	'receipt',
	'resend'
));
$table->define_headers(array(
	'Module',
	'Payment Date',
	'Amount',
	'Receipt Number',
	'View Invoice'
));
$table->set_attribute('class', 'generalbox enrol-ecommerce-receipts');
$table->setup();

$sql = 'select * from {enrol_ecommerce} WHERE userid = ?';
$totalcount = $DB->count_records_select('enrol_ecommerce', 'userid = ?', array('userid' => $userid));

$perpage = 50;
$table->initialbars($totalcount > $perpage);
$table->pagesize($perpage, $totalcount);

$receipts = $DB->get_records_sql($sql, array('userid' => $userid), $table->get_page_start(), $table->get_page_size());

foreach($receipts as $r) {
	$row = array();

    $module_name = $DB->get_field('course', 'shortname', array('id' => $r->courseid));
    $module_link = '<a href="' . "$CFG->wwwroot/course/view.php?id=$r->courseid" . '" target="_blank"/>' . $module_name . '</a>';

    $row[] = $module_link;
    $row[] = $r->created_dttm;
    $row[] = '$' . number_format($r->payment_amount, 2);
    $row[] = $r->receipt_number;
    $row[] = $OUTPUT->action_icon(new moodle_url('/enrol/ecommerce/invoice.php', array('paymentid' => $r->id, 'display' => 1)), new pix_icon('f/pdf', 'Resend'));

    $table->add_data($row);
}

$table->finish_html();

echo '</div><!--.receipts-->';

echo $OUTPUT->footer();
