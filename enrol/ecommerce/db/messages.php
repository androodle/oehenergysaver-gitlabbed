<?php

/**
 *-----------------------------------------------------------------------------
 * eCommerce enrolment plugin message providers
 *-----------------------------------------------------------------------------
 * @package    enrol_ecommerce
 * @copyright  Androgogic Pty Ltd
 * @author     Androgogic Pty Ltd
 * @license    Commercial (http://www.androgogic.com)
 *-----------------------------------------------------------------------------
 **/

defined('MOODLE_INTERNAL') || die();

$messageproviders = array(
    'ecommerce_enrolment' => array(),
);

