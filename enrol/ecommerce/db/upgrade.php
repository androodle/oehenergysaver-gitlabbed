<?php

/**
 *-----------------------------------------------------------------------------
 * eCommerce enrolment plugin upgrade
 *-----------------------------------------------------------------------------
 * @package    enrol_ecommerce
 * @copyright  Androgogic Pty Ltd
 * @author     Androgogic Pty Ltd
 * @license    Commercial (http://www.androgogic.com)
 *-----------------------------------------------------------------------------
 **/

function xmldb_enrol_ecommerce_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2015070702) {

        // Define field invoice_emailed to be added to enrol_ecommerce.
        $table = new xmldb_table('enrol_ecommerce');
        $field = new xmldb_field('invoice_emailed', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0', 'cp_cash_journal');

        // Conditionally launch add field invoice_emailed.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Ecommerce savepoint reached.
        upgrade_plugin_savepoint(true, 2015070702, 'enrol', 'ecommerce');
    }

    return true;
}

