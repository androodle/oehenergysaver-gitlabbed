<?php 

	$token_servlet_url = "https://ws.support.qvalent.com/services/quickweb/CommunityTokenRequestServlet";

	$parameters = array(
		'username' => 'OEHSALES',
		'password' => 'Addzv4y8r',
		'principalAmount' => '20.00',
		'supplierBusinessCode' => 'OEHSALES',
		'paymentReference' => '12345678'
	);

	$parameters_string = "";
	foreach($parameters as $key=>$value) { 
		$parameters_string .= $key.'='.$value.'&'; 
	}
	rtrim($parameters_string, '&');

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $token_servlet_url);
	curl_setopt($ch, CURLOPT_POST, count($parameters));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

	if ($token = curl_exec($ch)) {
		if (strstr($token, "token=")) {
			$servlet_url = "https://quickweb.support.qvalent.com/OnlinePaymentServlet3";
			$redirect = "";
			$redirect .= "communityCode=OEHSALES";
			$redirect .= "&".$token;
			
			header("Location: $servlet_url?$redirect");	
		}
		else {
			echo "Error: Invalid token generated: " . $token;
			die();
		}		
	}
	curl_close($ch);
	