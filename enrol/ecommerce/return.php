<?php

/**
 *-----------------------------------------------------------------------------
 * eCommerce enrolment gateway process - payment return processing
 *-----------------------------------------------------------------------------
 * @package    enrol_ecommerce
 * @copyright  Androgogic Pty Ltd
 * @author     Androgogic Pty Ltd
 * @license    Commercial (http://www.androgogic.com)
 *-----------------------------------------------------------------------------
 **/

require('../../config.php');
require_once('lib.php');
require_once($CFG->dirroot . '/enrol/locallib.php');

global $DB, $PAGE, $USER;

// Check we have a logged in user
require_login();

// Check we have return parameters from the gateway, most importantly the summary code.
if ( !$_GET || !isset($_GET['summaryCode']) ) {
	error_log("enrol/ecommerce gateway error, no parameters returned by gateway");
	redirect($CFG->wwwroot, get_string('paymentgatewayerrorreturn', 'enrol_ecommerce'));
	die();
}

// TODO: Check we are dealing with a legitimate gateway payment, not someone URL hacking.

// Check summary code first for any issues
$summary_code = $_GET['summaryCode'];

// Get response code if there is one
$response_code = isset($_GET['responseCode']) ? $_GET['responseCode'] : NULL;
$response_description = isset($_GET['responseDescription']) ? $_GET['responseDescription'] : NULL;
	
// Display error to user
if ($summary_code != 0) {
	error_log("enrol/ecommerce transaction failed with summary_code=$summary_code and response_code=$response_code");	
	echo get_string('paymentgatewaytransactionfailed', 'enrol_ecommerce');
	echo "Summary code=$summary_code : " . get_quickweb_summary_code_description($summary_code);
	echo "Response code=$response_code : $responseDescription";
}

// Process all other parameters
$community_code = isset($_GET['communityCode']) ? $_GET['communityCode'] : NULL;
$business_code = isset($_GET['supplierBusinessCode']) ? $_GET['supplierBusinessCode'] : NULL;
$payment_amount = isset($_GET['paymentAmount']) ? $_GET['paymentAmount'] : NULL;
$payment_reference = isset($_GET['paymentReference']) ? $_GET['paymentReference'] : NULL;
$receipt_number = isset($_GET['receiptNumber']) ? $_GET['receiptNumber'] : NULL;
$created_dttm = isset($_GET['createdDateTime']) ? $_GET['createdDateTime'] : NULL;
$settlement_dt = isset($_GET['settlementDate']) ? $_GET['settlementDate'] : NULL;
$card_holder_name = isset($_GET['cardholderName']) ? $_GET['cardholderName'] : NULL;
$masked_card_number = isset($_GET['maskedCardNumber']) ? $_GET['maskedCardNumber'] : NULL;
$card_scheme = isset($_GET['cardScheme']) ? $_GET['cardScheme'] : NULL;
$card_expiry_month = isset($_GET['expiryDateMonth']) ? $_GET['expiryDateMonth'] : NULL;
$card_expiry_year = isset($_GET['expiryDateYear']) ? $_GET['expiryDateYear'] : NULL;
$cp_gl_account = isset($_GET['customGLAccount']) ? $_GET['customGLAccount'] : NULL;
$cp_cash_journal = isset($_GET['customCashJournal']) ? $_GET['customCashJournal'] : NULL;

// Track enrolment status
$successfully_enrolled = FALSE;

if (isset($payment_reference)) {
	$payment_reference_data = explode('/', $payment_reference);	

	// Get User ID, Course ID and Enrolment ID from payment reference
	$userid = $payment_reference_data[0];
	$courseid = $payment_reference_data[1];	
	$enrolid = $payment_reference_data[2];

	// Check if this payment has already been processed
	$q = "select count(*) as count
			from {enrol_ecommerce}
			where userid = :userid
			and courseid = :courseid
			and enrolid = :enrolid
			and receipt_number = :receipt_number";

    $existing_rows = $DB->count_records_sql(
    	$q,
    	array(
    		'userid' => $userid,
    		'courseid' => $courseid,
    		'enrolid' => $enrolid,
    		'receipt_number' => $receipt_number
    	)
    );

	if ($existing_rows > 0) {
		$processed = TRUE;
	}
	else {
		$processed = FALSE;
	}

	$quickweb_data = new stdClass();
	$quickweb_data->userid = $userid;
	$quickweb_data->courseid = $courseid;
	$quickweb_data->enrolid = $enrolid;
	$quickweb_data->community_code = $community_code;
	$quickweb_data->business_code = $business_code;
	$quickweb_data->payment_amount = $payment_amount;
	$quickweb_data->payment_reference = $payment_reference;
	$quickweb_data->receipt_number = $receipt_number;
	$quickweb_data->response_code = $response_code;
	$quickweb_data->response_description = $response_description;
	$quickweb_data->summary_code = $summary_code;
	$quickweb_data->created_dttm = $created_dttm;
	$quickweb_data->settlement_dt = $settlement_dt;
	$quickweb_data->card_holder_name = $card_holder_name;
	$quickweb_data->masked_card_number = $masked_card_number;
	$quickweb_data->card_scheme = $card_scheme;
	$quickweb_data->card_expiry_month = $card_expiry_month;
	$quickweb_data->card_expiry_year = $card_expiry_year;
	$quickweb_data->cp_gl_account = $cp_gl_account;
	$quickweb_data->cp_cash_journal = $cp_cash_journal;

	// Save return data to database
	if (!$processed) {
	$dbinsertid = $DB->insert_record('enrol_ecommerce', $quickweb_data);
	error_log('DB insert ID='.$dbinsertid);
	}

	// Was the payment honoured? 
	if ($response_code == 8 && $summary_code == 0) {
		// YES: payment honoured by gateway
	
		$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
		$enrolment_settings = $DB->get_record('enrol', array('id'=>$enrolid));
		
		// Set up enrolment manager API for enrolling user into course
		$context = context_course::instance($courseid, MUST_EXIST);
		$manager = new course_enrolment_manager($PAGE, $course);
		$instances = $manager->get_enrolment_instances();
		$instance = $instances[$enrolid];
		$plugins = $manager->get_enrolment_plugins(true);
		$plugin = $plugins[$instance->enrol];

		/* Use enrolment period if set to determine end date */
		if ($enrolment_settings->enrolperiod > 0) {
			$enddate = date('U') + $enrolment_settings->enrolperiod;
		}
		/* Otherwise use the plugin end date - including no end date if not specified */
		else {
			$enddate = $enrolment_settings->enrolenddate;
		}

		$plugin->enrol_user(
			$instance, 
			$userid, 
			$enrolment_settings->roleid, 
			date('U'),
			$enddate
		);

		// Verify that enrolment was in fact successful
		$enrolment_status = $DB->get_field('user_enrolments', 'status', array('userid' => $userid,'enrolid'=>$enrolid));

        // Sign-in to facetoface instance if required
        $f2fmsg = '';
        if ($enrolment_settings->customint4 && $enrolment_settings->customint5
                && file_exists($CFG->dirroot . '/mod/facetoface/lib.php')) {
            require_once($CFG->dirroot . '/mod/facetoface/lib.php');
            $session = enrol_ecommerce_get_f2f_session($courseid);
            if ($session) {
                $facetoface = $DB->get_record('facetoface', array('id' => $session->facetoface));
                $cm = get_coursemodule_from_instance("facetoface", $facetoface->id, $course->id);

                $params = array();
                $params['discountcode']     = '';
                $params['notificationtype'] = $instance->customint6;

                $result = facetoface_user_import($course, $facetoface, $session, $USER->id, $params);
                if ($result['result'] === true) {
                    add_to_log($course->id, 'facetoface', 'signup', "signup.php?s=$session->id", $session->id, $cm->id);
                    $f2fmsg = get_string('f2fsignedupuser', 'enrol_ecommerce');
                } else {
                    if (isset($result['conflict']) && $result['conflict']) {
                        $f2fmsg = $result['conflict'];
                    } else {
                        add_to_log($course->id, 'facetoface', 'signup (FAILED)', "signup.php?s=$session->id", $session->id, $cm->id);
                        $f2fmsg = get_string('error:problemsigningup', 'facetoface');
                    }
                }
            }
        }

		if (isset($enrolment_status) && (int)$enrolment_status == 0) { // Active status = 0
			$successfully_enrolled = TRUE;
		}
	}	
			
}

// Display appropriate page to user depending on their enrolment status
$course_url = "$CFG->wwwroot/course/view.php?id=$courseid";
$coursename = $DB->get_field('course', 'fullname', array('id' => $courseid));
if ($successfully_enrolled) {
    $invoice_url = "$CFG->wwwroot/enrol/ecommerce/invoice.php?courseid=$courseid&display=0&paymentid=$dbinsertid";
    redirect($invoice_url, get_string('successfullyenrolled', 'enrol_ecommerce', $coursename));
}
else {
    $PAGE->set_url("$CFG->wwwroot/enrol/ecommerce/return.php");
    echo $OUTPUT->header();
    notice(get_string('enrolmentfailed', 'enrol_ecommerce', $coursename), $course_url);
}
