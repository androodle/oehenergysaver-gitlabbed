<?php

/**
 *-----------------------------------------------------------------------------
 * eCommerce enrolment plugin class
 *-----------------------------------------------------------------------------
 * @package    enrol_ecommerce
 * @copyright  Androgogic Pty Ltd
 * @author     Androgogic Pty Ltd
 * @license    Commercial (http://www.androgogic.com)
 *-----------------------------------------------------------------------------
 **/

defined('MOODLE_INTERNAL') || die();

class enrol_ecommerce_plugin extends enrol_plugin {

    public function add_course_navigation($instancesnode, stdClass $instance) {
        if ($instance->enrol !== 'ecommerce') {
             throw new coding_exception('Invalid enrol instance type!');
        }

        $context = context_course::instance($instance->courseid);
        if (has_capability('enrol/ecommerce:config', $context)) {
            $managelink = new moodle_url('/enrol/ecommerce/edit.php', array('courseid'=>$instance->courseid, 'id'=>$instance->id));
            $instancesnode->add($this->get_instance_name($instance), $managelink, navigation_node::TYPE_SETTING);
        }
    }	

	public function allow_unenrol(stdClass $instance) {
        return TRUE;
    }

    public function allow_manage(stdClass $instance) {
        return true;
    }

    public function cron() {
        $trace = new text_progress_trace();
        $this->process_expirations($trace);
    }
    
    public function enrol_page_hook(stdClass $instance) {
        global $CFG, $USER, $OUTPUT, $PAGE, $DB;

        ob_start();

        if ($DB->record_exists('user_enrolments', array('userid'=>$USER->id, 'enrolid'=>$instance->id))) {
            return ob_get_clean();
        }

        if ($instance->enrolstartdate != 0 && $instance->enrolstartdate > time()) {
            return ob_get_clean();
        }

        if ($instance->enrolenddate != 0 && $instance->enrolenddate < time()) {
            return ob_get_clean();
        }

        $course = $DB->get_record('course', array('id'=>$instance->courseid));
        $context = context_course::instance($course->id);

        $shortname = format_string($course->shortname, true, array('context' => $context));
        $strloginto = get_string("loginto", "", $shortname);
        $strcourses = get_string("courses");

        // Pass $view=true to filter hidden caps if the user cannot see them
        if ($users = get_users_by_capability($context, 'moodle/course:update', 'u.*', 'u.id ASC',
                                             '', '', '', '', false, true)) {
            $users = sort_by_roleassignment_authority($users, $context);
            $teacher = array_shift($users);
        } else {
            $teacher = false;
        }

        // Cost check, does this instance have a cost associated with it?
        if ($instance->cost <= 0) {        	
        	echo '<p>'.get_string('nocost', 'enrol_ecommerce').'</p>';
        }
        else {
            $promocode = optional_param('promocode', '', PARAM_TEXT);
            $cost = $this->get_cost($instance,$promocode);
        	$cost = format_float($cost, 2, false); // Cost in correct format for gateway

            // mod_facetoface integration
            $f2fcheckpass = true;
            if ($instance->customint4 && file_exists($CFG->dirroot . '/mod/facetoface/lib.php')) {
                require_once($CFG->dirroot . '/mod/facetoface/lib.php');
                $f2fcheckpass = enrol_ecommerce_get_f2f_session($course->id);
            }

            if ($f2fcheckpass) {
                if (isguestuser()) { // force login only for guest user, not real users with guest role
                    if (empty($CFG->loginhttps)) {
                        $wwwroot = $CFG->wwwroot;
                    } else {
                        $wwwroot = str_replace("http://", "https://", $CFG->wwwroot);
                    }
                    echo '<div class="mdl-align"><p>'.get_string('paymentrequired').'</p>';
                    echo '<p><b>'.get_string('cost').": $cost".'</b></p>';
                    //echo '<p><a href="'.$wwwroot.'/login/">'.get_string('loginsite').'</a></p>';
                    echo '</div>';
                } else {

                    $coursefullname  = format_string($course->fullname, true, array('context'=>$context));
                    $courseshortname = $shortname;
                    $userfullname    = fullname($USER);
                    $userfirstname   = $USER->firstname;
                    $userlastname    = $USER->lastname;
                    $useraddress     = $USER->address;
                    $usercity        = $USER->city;
                    $instancename    = $this->get_instance_name($instance);

                    include($CFG->dirroot.'/enrol/ecommerce/enrol.html');
                }
            } else {
                echo $OUTPUT->box(get_string('f2fsessionsunavailable', 'enrol_ecommerce'));
            }
        }

        return $OUTPUT->box(ob_get_clean());
    }

	/**
	 * Generate payment receipt for specified transaction.
	 *
	 * @param 	transaction_id (id from {enrol_ecommerce})
	 * @param 	admin overide allows an admin (using login as user) 
	 * 			to have the email receipt sent to their email
	 * @param 	send PDF receipt as email if appropriate
	 * @return 	the PDF receipt which is optionally sent via email
	 **/

	public function generate_receipt($transaction_id = NULL, $admin_override = FALSE, $send_as_email = FALSE) {
		echo 'Generate Receipt Stub';
		die();
	}

        /*
         * given a promocode (or not), get the cost
         */
        public function get_cost($instance,$promocode) {
           global $DB;
           if($promocode != ''){
                $sql = "select * from mdl_enrol
                    where enrol = 'ecommerce' 
                    and courseid = $instance->courseid
                    and customtext1 = '$promocode' OR customtext2 = '$promocode' or customtext3 = '$promocode'";

                $result = $DB->get_record_sql($sql);

                if ($result) {
                    if($promocode == $result->customtext1){
                        $cost = (float) $result->customint1;
                    }
                    if($promocode == $result->customtext2){
                        $cost = (float) $result->customint2;
                    }
                    if($promocode == $result->customtext3){
                        $cost = (float) $result->customint3;
                    }
                }
            }
            if(!isset($cost)){
                $cost = (float) $instance->cost;
            }
            return $cost;
        }

    /**
     * Returns edit icons for the page with list of instances
     * @param stdClass $instance
     * @return array
     */

    public function get_action_icons(stdClass $instance) {
        global $OUTPUT;

        if ($instance->enrol !== 'ecommerce') {
            throw new coding_exception('invalid enrol instance!');
        }
        $context = context_course::instance($instance->courseid);

        $icons = array();

        if (has_capability('enrol/ecommerce:config', $context)) {
            $editlink = new moodle_url("/enrol/ecommerce/edit.php", array('courseid'=>$instance->courseid, 'id'=>$instance->id));
            $icons[] = $OUTPUT->action_icon($editlink, new pix_icon('t/edit', get_string('edit'), 'core',
                    array('class' => 'iconsmall')));
        }

        return $icons;
    }    

 	/**
     * Gets an array of the user enrolment actions
     *
     * @param course_enrolment_manager $manager
     * @param stdClass $ue A user enrolment object
     * @return array An array of user_enrolment_actions
     */
    public function get_user_enrolment_actions(course_enrolment_manager $manager, $ue) {
        $actions = array();
        $context = $manager->get_context();
        $instance = $ue->enrolmentinstance;
        $params = $manager->get_moodlepage()->url->params();
        $params['ue'] = $ue->id;
        if ($this->allow_unenrol($instance) && has_capability("enrol/ecommerce:unenrol", $context)) {
            $url = new moodle_url('/enrol/unenroluser.php', $params);
            $actions[] = new user_enrolment_action(new pix_icon('t/delete', ''), get_string('unenrol', 'enrol'), $url, array('class'=>'unenrollink', 'rel'=>$ue->id));
        }
        if ($this->allow_manage($instance) && has_capability("enrol/ecommerce:manage", $context)) {
            $url = new moodle_url('/enrol/editenrolment.php', $params);
            $actions[] = new user_enrolment_action(new pix_icon('t/edit', ''), get_string('edit'), $url, array('class'=>'editenrollink', 'rel'=>$ue->id));
        }
        return $actions;
    }	

    public function get_info_icons(array $instances) {
        return array(new pix_icon('icon', get_string('pluginname', 'enrol_ecommerce'), 'enrol_ecommerce'));
    }

	public function get_newinstance_link($courseid) {
        $context = context_course::instance($courseid, MUST_EXIST);

        if (!has_capability('moodle/course:enrolconfig', $context) or !has_capability('enrol/ecommerce:config', $context)) {
            return NULL;
        }

        // multiple instances supported - different cost for different roles
        return new moodle_url('/enrol/ecommerce/edit.php', array('courseid'=>$courseid));
    }    

	/**
     * Restore instance and map settings.
     *
     * @param restore_enrolments_structure_step $step
     * @param stdClass $data
     * @param stdClass $course
     * @param int $oldid
     */
    public function restore_instance(restore_enrolments_structure_step $step, stdClass $data, $course, $oldid) {
        global $DB;
        if ($step->get_task()->get_target() == backup::TARGET_NEW_COURSE) {
            $merge = false;
        } else {
            $merge = array(
                'courseid'   => $data->courseid,
                'enrol'      => $this->get_name(),
                'roleid'     => $data->roleid,
                'cost'       => $data->cost,
                'currency'   => $data->currency,
            );
        }
        if ($merge and $instances = $DB->get_records('enrol', $merge, 'id')) {
            $instance = reset($instances);
            $instanceid = $instance->id;
        } else {
            $instanceid = $this->add_instance($course, (array)$data);
        }
        $step->set_mapping('enrol', $oldid, $instanceid);
    }

	/**
     * Restore user enrolment.
     *
     * @param restore_enrolments_structure_step $step
     * @param stdClass $data
     * @param stdClass $instance
     * @param int $oldinstancestatus
     * @param int $userid
     */
    public function restore_user_enrolment(restore_enrolments_structure_step $step, $data, $instance, $userid, $oldinstancestatus) {
        $this->enrol_user($instance, $userid, null, $data->timestart, $data->timeend, $data->status);
    }    

	public function roles_protected() {
        return FALSE;
    }

	/**
	 * Save transaction details back from gateway to the database.
	 *
	 * @param 	transactiond data as object
	 * @return 	TRUE if saved successfully, FALSE otherwise
	 * 			and log error in moodle log table.
	 * 
	 * NOTE: 	this is the ONLY function that should deal with the database
	 * 			table directly. 
	 **/

	public function save_transaction($transaction_data = NULL) {

		if ($transaction_data) {
			echo 'Save transaction Stub';			
			return TRUE;
		}
		else {
			// TODO: add message to error log
			return FALSE;	
		}

	}

	public function show_enrolme_link(stdClass $instance) {
        return ($instance->status == ENROL_INSTANCE_ENABLED);
    }

    /**
     * Execute synchronisation.
     * @param progress_trace $trace
     * @return int exit code, 0 means ok
     */

    public function sync(progress_trace $trace) {
        $this->process_expirations($trace);
        return 0;
    }


} // end of class enrol_ecommerce_plugin
