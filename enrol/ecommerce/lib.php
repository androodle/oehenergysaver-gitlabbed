<?php

/**
 *-----------------------------------------------------------------------------
 * eCommerce enrolment plugin library
 *-----------------------------------------------------------------------------
 * @package    enrol_ecommerce
 * @copyright  Androgogic Pty Ltd
 * @author     Androgogic Pty Ltd
 * @license    Commercial (http://www.androgogic.com)
 *-----------------------------------------------------------------------------
 **/

defined('MOODLE_INTERNAL') || die();

require_once("ecommerce.class.php");

/**
 *-----------------------------------------------------------------------------
 * Generate PDF file
 *-----------------------------------------------------------------------------
 * Use the Moodle PDFLib to generate a PDF file with the filename and HTML
 * specified.
 *
 * @param   $filename for PDF file to be created
 * @param   $html content for the PDF to generate
 * @param   $orientation of PDF (P=portrait, L=landscape)
 * @return  PDF output inline ("I"). Note change to "D" if you want to download
 *          the content instead of show it inline in the browser. Use "E" to
 *          return as base64 encoded for use as an email attachment.
 *-----------------------------------------------------------------------------
 **/

function generate_pdf($filename = 'no-filename.pdf', $html = "Nothing to see here", $orientation = "P", $output = 'I') {

    global $CFG;

    require_once $CFG->libdir . '/pdflib.php';
    set_time_limit(0);

    // Set layout (portrait, A4, UTF-8);
    $pdf = new PDF($orientation, 'mm', 'A4', true, 'UTF-8');

    $pdf->setTitle($filename);
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    $pdf->SetMargins(0, 0, 0, true);
    $pdf->SetFooterMargin(0);
    $pdf->SetAutoPageBreak(TRUE, -10);
    $pdf->AddPage();

    $pdf->WriteHTML($html, true, false, false, false, '');

    // Send PDF to browser inline
    return $pdf->Output($filename, $output);

}

/**
 *-----------------------------------------------------------------------------
 * Get course link
 *-----------------------------------------------------------------------------
 * Return a hyperlink to the given course with the course shortname as the
 * display text for that hyperlink
 *
 * @param   $course_data from course table for a specific course id
 * @return  Course hyperlink with course shortname
 *          NULL if no course data
 *-----------------------------------------------------------------------------
 **/

function get_course_link($course_data = NULL) {


    if (empty($course_data) || empty($course_data->id) || empty($course_data->shortname)) {
        return NULL;
    }

    $link = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course_data->id.'" target="_blank">';
    $link .= $course_data->shortname.'</a>';
    return $link;
}

/**
 *-----------------------------------------------------------------------------
 * Get payment data
 *-----------------------------------------------------------------------------
 * Get payment data for the specified payment ID.
 *
 * @param   $payment_id to lookup in {enrol_ecommerce}
 * @return  Payment data if found, NULL otherwise.
 *-----------------------------------------------------------------------------
 **/

function get_payment_data($paymentid = NULL) {

    global $DB;

    if ($paymentid) {
        $payment_data = $DB->get_record('enrol_ecommerce', array('id' => $paymentid));
        if ($payment_data) {
            return $payment_data;
        }
        else {
            error_log("enrol_ecommerce->lib.php->get_payment_data: no data for payment id=$paymentid");
            return NULL;
        }
    }
    else {
        error_log('enrol_ecommerce->lib.php->get_payment_data: no payment id specified');
        return NULL;
    }

}

/**
 * Get description for specified QuickWeb summary code value.
 * See QuickWeb guide for summary code mappings.
 *
 * @param   Summary code value
 * @return  Summary code description if found
 *          "Mapping not found" if a mapping is not found for code
 **/

function get_quickweb_summary_code_description($summary_code = NULL) {

    switch ($summary_code) {
        case 0:
            return "Approved by financial institution";
            break;
        case 1:
            return "Declined by financial institution";
            break;
        case 2:
            return "Unknown/pending";
            break;
        case 3:
            return "Rejected";
            break;
        default:
            return "Mapping not found";
    }

}

/**
 *-----------------------------------------------------------------------------
 * Display receipts link on moodle user profile
 *-----------------------------------------------------------------------------
 * Display a link to show receipts on the moodle user profile.
 * NOTE: this is called from user/profile.php and directly
 * outputs HTML that is intended to work from that code.
 *
 * @param   None
 * @return  Hyperlink to receipts if (student has any)
 *          Nothing if the student has not receipts.
 *-----------------------------------------------------------------------------
 **/

function display_receipts_link_on_user_profile($context, $userid) {

    global $DB, $CFG, $USER;

    if ($DB->get_records('enrol_ecommerce', array('userid' => $userid))) {

        // Security check, make sure a user can't access another user's receipts
        if ($USER->id != $userid && !has_capability('enrol/ecommerce:manage', $context)) {
            return false;
        }

        $receiptsurl = new moodle_url($CFG->wwwroot.'/enrol/ecommerce/receipts.php?userid='.$userid);
        echo html_writer::tag('dt', get_string('receiptsheading', 'enrol_ecommerce'));
        echo html_writer::tag('dd', html_writer::link($receiptsurl, get_string('receiptslink', 'enrol_ecommerce')));

    }

}

/*
 *  Search for mod_facetoface session availability in the course.
 *  Returns the first available session found, false if session not found.
 */
function enrol_ecommerce_get_f2f_session($courseid) {
    global $CFG, $DB, $USER;
    require_once($CFG->dirroot . '/mod/facetoface/lib.php');
    $foundsession = false;
    // Check if there is an instance of f2f module in the course assuming there's only 1 instance.
    $sql = "SELECT cm.*
        FROM {course_modules} cm
        JOIN {modules} m ON cm.module = m.id
        WHERE cm.course = :courseid
            AND m.name = 'facetoface'
        LIMIT 1
        ";
    if ($cm = $DB->get_record_sql($sql, array('courseid' => $courseid))) {
        $contextmodule = context_module::instance($cm->id);
        // Check if the f2f activity is available for sign-up.
        // Checks are similar to those in facetoface_cm_info_view().
        switch (true) { // just to be able to break out of this block of code at any time
            case true:
                if (!($facetoface = $DB->get_record('facetoface', array('id' => $cm->instance)))) {
                    break;
                }
                // For sign-up, there must be NO submissions for this user in the f2f activity.
                if ($submissions = facetoface_get_user_submissions($facetoface->id, $USER->id)) {
                    break;
                }
                // There must be sessions for the f2f activity.
                if (!($sessions = facetoface_get_sessions($facetoface->id))) {
                    break;
                }
                if ($facetoface->display == 0) {
                    break;
                }
                // Check if any of the sessions is available.
                $foundsession = false;
                foreach ($sessions as $session) {
                    if (!facetoface_session_has_capacity($session, $contextmodule, MDL_F2F_STATUS_WAITLISTED) && !$session->allowoverbook) {
                        continue;
                    }
                    $foundsession = $session;
                    break;
                }
                break;
        }
    }
    return $foundsession;
}

