<?php

/**
 *-----------------------------------------------------------------------------
 * eCommerce enrolment plugin add new instance
 *-----------------------------------------------------------------------------
 * @package    enrol_ecommerce
 * @copyright  Androgogic Pty Ltd
 * @author     Androgogic Pty Ltd
 * @license    Commercial (http://www.androgogic.com)
 *-----------------------------------------------------------------------------
 **/

require('../../config.php');
require_once('edit_form.php');

$courseid   = required_param('courseid', PARAM_INT);
$instanceid = optional_param('id', 0, PARAM_INT); // instanceid

$course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id, MUST_EXIST);

require_login($course);
require_capability('enrol/ecommerce:config', $context);

if (file_exists($CFG->dirroot . '/mod/facetoface/lib.php')) {
    require_once($CFG->dirroot . '/mod/facetoface/lib.php');
}

$PAGE->set_url('/enrol/ecommerce/edit.php', array('courseid'=>$course->id, 'id'=>$instanceid));
$PAGE->set_pagelayout('admin');

$return = new moodle_url('/enrol/instances.php', array('id'=>$course->id));
if (!enrol_is_enabled('ecommerce')) {
    redirect($return);
}

$plugin = enrol_get_plugin('ecommerce');

if ($instanceid) {
    $instance = $DB->get_record('enrol', array('courseid'=>$course->id, 'enrol'=>'ecommerce', 'id'=>$instanceid), '*', MUST_EXIST);
    $instance->cost = format_float($instance->cost, 2, true);
} else {
    require_capability('moodle/course:enrolconfig', $context);
    // no instance yet, we have to add new instance
    navigation_node::override_active_url(new moodle_url('/enrol/instances.php', array('id'=>$course->id)));
    $instance = new stdClass();
    $instance->id       = null;
    $instance->courseid = $course->id;
}

$mform = new enrol_ecommerce_edit_form(NULL, array($instance, $plugin, $context));

if ($mform->is_cancelled()) {
    redirect($return);

} else if ($data = $mform->get_data()) {

    $data->customint4 = (isset($data->customint4)) ? $data->customint4 : 0;
    $data->customint5 = (isset($data->customint5)) ? $data->customint5 : 0;
    $data->customint6 = (isset($data->customint6)) ? $data->customint6 : 0;

    if ($instance->id) {
        $reset = ($instance->status != $data->status);

        $instance->status         = $data->status;
        $instance->name           = $data->name;
        $instance->cost           = unformat_float($data->cost);
        $instance->customtext1    = $data->customtext1;
        $instance->customint1     = unformat_float($data->customint1);
        $instance->customtext2    = $data->customtext2;
        $instance->customint2     = unformat_float($data->customint2);
        $instance->customtext3    = $data->customtext3;
        $instance->customint3     = unformat_float($data->customint3);
        $instance->customint4     = $data->customint4;
        $instance->customint5     = $data->customint5;
        $instance->customint6     = $data->customint6;
        $instance->currency       = 'AUD';
        $instance->roleid         = $data->roleid;        
        $instance->enrolperiod    = $data->enrolperiod;
        $instance->enrolstartdate = $data->enrolstartdate;
        $instance->enrolenddate   = $data->enrolenddate;
        $instance->timemodified   = time();
        
        $DB->update_record('enrol', $instance);

        if ($reset) {
            $context->mark_dirty();
        }

    } else {
        $fields = array('status'=>$data->status, 'name'=>$data->name, 'cost'=>unformat_float($data->cost), 'currency'=>$data->currency, 'roleid'=>$data->roleid,
                        'enrolperiod'=>$data->enrolperiod, 'enrolstartdate'=>$data->enrolstartdate, 'enrolenddate'=>$data->enrolenddate,
                        'customint4' => $data->customint4, 'customint5' => $data->customint5, 'customint6' => $data->customint6);
        $plugin->add_instance($course, $fields);
    }

    redirect($return);
}

$PAGE->set_heading($course->shortname);
$PAGE->set_title(get_string('pluginname', 'enrol_ecommerce'));

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('pluginname', 'enrol_ecommerce'));
$mform->display();
echo $OUTPUT->footer();
