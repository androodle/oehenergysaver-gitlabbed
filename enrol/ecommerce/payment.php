<?php

/**
 *-----------------------------------------------------------------------------
 * eCommerce enrolment plugin payment request
 *-----------------------------------------------------------------------------
 * @package    enrol_ecommerce
 * @copyright  Androgogic Pty Ltd
 * @author     Androgogic Pty Ltd
 * @license    Commercial (http://www.androgogic.com)
 *-----------------------------------------------------------------------------
 **/

require('../../config.php');
global $USER, $DB;

// Check we have a logged in user
require_login();

$promocode = optional_param('promocode', '', PARAM_TEXT);

$plugin = enrol_get_plugin('ecommerce');

//-----------------------------------------------------------------------------
// Validation / retrieve module cost for enrolment instance
//-----------------------------------------------------------------------------

// Check we have a valid enrolment instance ID before going any further 
if ( !isset($_POST) || !isset($_POST['instanceid']) || empty($_POST['instanceid']) ) {
	error_log("enrol/ecommerce gateway error invalid enrolment instance: $instanceid");
	redirect($CFG->wwwroot, get_string('paymentgatewayerrorinvalidcost', 'enrol_ecommerce'));
	die();
}
else {
	$instanceid = $_POST['instanceid'];
	$courseid = $_POST['courseid'];
}
$enrol_instance = $DB->get_record('enrol',array('id'=>$instanceid));

$enrol_ecommerce_plugin = new enrol_ecommerce_plugin();

// Get enrolment cost from database.
// IMPORTANT: cost is not passed as POST or any other HTTP request as it could
// be tampered with. Instead instanceid is used to look up the cost in the
// enrol table, so that we know for certain we area dealing with the actual, set
// cost set in the database.

//$cost = $DB->get_field('enrol', 'cost', array('id'=>$instanceid));
$cost = $enrol_ecommerce_plugin->get_cost($enrol_instance,$promocode);

// Invalid cost, error and stop processing
if ($cost === FALSE || $cost < 0) {
	error_log("enrol/ecommerce gateway error invalid enrolment instance: $instanceid");
	redirect($CFG->wwwroot, get_string('paymentgatewayerrorinvalidcost', 'enrol_ecommerce'));
	die();
}

// Payment reference is the Moodle User ID and enrolment instance ID
$payment_reference = $USER->id.'/'.$courseid.'/'.$instanceid;

/*If free promotion code used skip payway*/
if($cost == 0)
{
	require_once($CFG->dirroot . '/enrol/locallib.php');
	$userid = $USER->id;
	$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);

	// Set up enrolment manager API for enrolling user into course
	$context = context_course::instance($courseid, MUST_EXIST);
	$manager = new course_enrolment_manager($PAGE, $course);
	$instances = $manager->get_enrolment_instances();
	$instance = $instances[$instanceid];
	$plugins = $manager->get_enrolment_plugins(true);
	$plugin = $plugins[$instance->enrol];

	/* Use enrolment period if set to determine end date */
	if ($enrol_instance->enrolperiod > 0) {
		$enddate = date('U') + $enrol_instance->enrolperiod;
	}
	/* Otherwise use the plugin end date - including no end date if not specified */
	else {
		$enddate = $enrol_instance->enrolenddate;
	}

	$plugin->enrol_user(
		$instance,
		$userid,
		$enrol_instance->roleid,
		date('U'),
		$enddate
	);

	// Verify that enrolment was in fact successful
	$enrolment_status = $DB->get_field('user_enrolments', 'status', array('userid' => $userid,'enrolid'=>$instanceid));
    // Sign-in to facetoface instance if required
    $f2fmsg = '';
    if ($enrol_instance->customint4 && $enrol_instance->customint5
            && file_exists($CFG->dirroot . '/mod/facetoface/lib.php')) {
       require_once($CFG->dirroot . '/mod/facetoface/lib.php');
        $session = enrol_ecommerce_get_f2f_session($courseid);
        if ($session) {
            $facetoface = $DB->get_record('facetoface', array('id' => $session->facetoface));
            $cm = get_coursemodule_from_instance("facetoface", $facetoface->id, $course->id);

            $params = array();
            $params['discountcode']     = '';
            $params['notificationtype'] = $instance->customint6;

            $result = facetoface_user_import($course, $facetoface, $session, $userid, $params);
            if ($result['result'] === true) {
                add_to_log($course->id, 'facetoface', 'signup', "signup.php?s=$session->id", $session->id, $cm->id);
                $f2fmsg = get_string('f2fsignedupuser', 'enrol_ecommerce');
            } else {
                if (isset($result['conflict']) && $result['conflict']) {
                    $f2fmsg = $result['conflict'];
                } else {
                    add_to_log($course->id, 'facetoface', 'signup (FAILED)', "signup.php?s=$session->id", $session->id, $cm->id);
                    $f2fmsg = get_string('error:problemsigningup', 'facetoface');
                }
            }
        }
    }
    
    
    
    $quickweb_data = new stdClass();
	$quickweb_data->userid = $USER->id;
	$quickweb_data->courseid = $courseid;
	$quickweb_data->enrolid = $instanceid;
	$quickweb_data->community_code = "";
	$quickweb_data->business_code = "";
	$quickweb_data->payment_amount = $cost;
	$quickweb_data->payment_reference = $payment_reference;
	$quickweb_data->receipt_number = "";
	$quickweb_data->response_code = "";
	$quickweb_data->response_description = "";
	$quickweb_data->summary_code = "";
	$quickweb_data->created_dttm = date("j M Y H:i:s");
	$quickweb_data->settlement_dt = "";
	$quickweb_data->card_holder_name = "";
	$quickweb_data->masked_card_number = "";
	$quickweb_data->card_scheme = "";
	$quickweb_data->card_expiry_month = "";
	$quickweb_data->card_expiry_year = "";
	$quickweb_data->cp_gl_account = "";
	$quickweb_data->cp_cash_journal = "";


	// Save promo data to database
	$dbinsertid = $DB->insert_record('enrol_ecommerce', $quickweb_data);
	error_log('DB insert ID='.$dbinsertid);
    
    
    $coursename = $DB->get_field('course', 'fullname', array('id' => $courseid));
    $course_url = "$CFG->wwwroot/course/view.php?id=$courseid";
	redirect($course_url, get_string('successfullyenrolledfree', 'enrol_ecommerce', $coursename));
    exit;
    
}

//-----------------------------------------------------------------------------
// Set up gateway
//-----------------------------------------------------------------------------

// Get gateway parameters
$parameters = array(
	'username' => $plugin->get_config('gatewayusername'),
	'password' => $plugin->get_config('gatewaypassword'),
	'principalAmount' => $cost,
	'supplierBusinessCode' => $plugin->get_config('gatewaybusinesscode'),
	'paymentReference' => $payment_reference,
	'returnUrl' => $CFG->wwwroot . '/enrol/ecommerce/return.php',
	'customGLAccount' => $plugin->get_config('gatewayglaccount'),
	'customCashJournal' => $plugin->get_config('gatewaycashjournal')
);

// Validate gateway parameters
if (empty($parameters['username']) || 
	empty($parameters['password']) ||
	empty($parameters['principalAmount']) ||
	empty($parameters['supplierBusinessCode']) ||
	empty($parameters['paymentReference']) ||
	empty($parameters['returnUrl']) ||
	empty($parameters['customGLAccount']) ||
	empty($parameters['customCashJournal'])) {
	error_log("enrol/ecommerce gateway parameters are missing");
	redirect($CFG->wwwroot, get_string('paymentgatewayerrorinvalidsetup', 'enrol_ecommerce'));
	die();	
}

$parameters_string = "";
foreach($parameters as $key=>$value) { 
	$parameters_string .= $key.'='.$value.'&'; 
}
rtrim($parameters_string, '&');

error_log("Sending call to payment gateway with this parameter string: $parameters_string");

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $plugin->get_config('gatewaytokenurl'));
curl_setopt($ch, CURLOPT_POST, count($parameters));
curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

if ($token = curl_exec($ch)) {
	if (strstr($token, "token=")) {
		$redirect = $plugin->get_config('gatewayhandoffurl');
		$redirect .= "?communityCode=OEHSALES";
		$redirect .= "&".$token;
		header("Location: $redirect");	
	}
	else {
		error_log("enrol/ecommerce gateway error has occurred: $token");
		redirect($CFG->wwwroot, get_string('paymentgatewayerror', 'enrol_ecommerce'));
		die();
	}		
}
curl_close($ch);
