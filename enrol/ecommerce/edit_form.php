<?php

/**
 *-----------------------------------------------------------------------------
 * eCommerce enrolment plugin add new instance form
 *-----------------------------------------------------------------------------
 * @package    enrol_ecommerce
 * @copyright  Androgogic Pty Ltd
 * @author     Androgogic Pty Ltd
 * @license    Commercial (http://www.androgogic.com)
 *-----------------------------------------------------------------------------
 **/

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');

class enrol_ecommerce_edit_form extends moodleform {

    function definition() {
        $mform = $this->_form;

        list($instance, $plugin, $context) = $this->_customdata;

        //$mform->addElement('text', 'name', get_string('custominstancename', 'enrol'));
        //$mform->setType('name', PARAM_TEXT);

        $options = array(ENROL_INSTANCE_ENABLED  => get_string('yes'),
                         ENROL_INSTANCE_DISABLED => get_string('no'));
        $mform->addElement('select', 'status', get_string('status', 'enrol_ecommerce'), $options);
        $mform->setDefault('status', $plugin->get_config('status'));

        $mform->addElement('text', 'cost', get_string('cost', 'enrol_ecommerce'), array('size'=>4));
        $mform->setType('cost', PARAM_RAW); // Use unformat_float to get real value.
        $mform->setDefault('cost', format_float($plugin->get_config('defaultcost'), 2, true));

        // Promotional codes (optional)

        $mform->addElement('text', 'customtext1', get_string('promocode1', 'enrol_ecommerce'), array('size'=>10));
        $mform->setType('customtext1', PARAM_TEXT);
        
        $mform->addElement('text', 'customint1', get_string('promocost1', 'enrol_ecommerce'), array('size'=>4));
        $mform->setType('customint1', PARAM_RAW); // Use unformat_float to get real value.

        $mform->addElement('text', 'customtext2', get_string('promocode2', 'enrol_ecommerce'), array('size'=>10));
        $mform->setType('customtext2', PARAM_TEXT);
        
        $mform->addElement('text', 'customint2', get_string('promocost2', 'enrol_ecommerce'), array('size'=>4));
        $mform->setType('customint2', PARAM_RAW); // Use unformat_float to get real value.
        
        $mform->addElement('text', 'customtext3', get_string('promocode3', 'enrol_ecommerce'), array('size'=>10));
        $mform->setType('customtext3', PARAM_TEXT);
        
        $mform->addElement('text', 'customint3', get_string('promocost3', 'enrol_ecommerce'), array('size'=>4));
        $mform->setType('customint3', PARAM_RAW); // Use unformat_float to get real value.
        
        if ($instance->id) {
            $roles = get_default_enrol_roles($context, $instance->roleid);
        } else {
            $roles = get_default_enrol_roles($context, $plugin->get_config('roleid'));
        }
        $mform->addElement('select', 'roleid', get_string('assignrole', 'enrol_ecommerce'), $roles);
        $mform->setDefault('roleid', $plugin->get_config('roleid'));

        $mform->addElement('duration', 'enrolperiod', get_string('enrolperiod', 'enrol_ecommerce'), array('optional' => true, 'defaultunit' => 86400));
        $mform->setDefault('enrolperiod', $plugin->get_config('enrolperiod'));
        //$mform->addHelpButton('enrolperiod', 'enrolperiod', 'enrol_ecommerce');

        $mform->addElement('date_selector', 'enrolstartdate', get_string('enrolstartdate', 'enrol_ecommerce'), array('optional' => true));
        $mform->setDefault('enrolstartdate', 0);
        //$mform->addHelpButton('enrolstartdate', 'enrolstartdate', 'enrol_ecommerce');

        $mform->addElement('date_selector', 'enrolenddate', get_string('enrolenddate', 'enrol_ecommerce'), array('optional' => true));
        $mform->setDefault('enrolenddate', 0);
        //$mform->addHelpButton('enrolenddate', 'enrolenddate', 'enrol_ecommerce');

        // mod_facetoface checking start
        global $CFG;
        if (file_exists($CFG->dirroot . '/mod/facetoface/lib.php')) {
            $mform->addElement('header', 'general', get_string('f2fheader', 'enrol_ecommerce'));
            $mform->addElement('selectyesno', 'customint4', get_string('f2fenabled', 'enrol_ecommerce'));
            $mform->setDefault('customint4', 0);
            $mform->addElement('selectyesno', 'customint5', get_string('f2fsignup', 'enrol_ecommerce'));
            $mform->setDefault('customint5', 0);
            $mform->disabledIf('customint5','customint4','neq',1);

            $options = array(MDL_F2F_BOTH => get_string('notificationboth', 'facetoface'),
                             MDL_F2F_TEXT => get_string('notificationemail', 'facetoface'),
                             MDL_F2F_NONE => get_string('notificationnone', 'facetoface'),
                             );
            $mform->addElement('select', 'customint6', get_string('defaultnotificationtype', 'enrol_ecommerce'), $options);
            $mform->addHelpButton('customint6', 'defaultnotificationtype', 'enrol_ecommerce');
            $mform->setDefault('customint6', MDL_F2F_NONE);
            $mform->disabledIf('customint6','customint5','neq',1);
            $mform->disabledIf('customint6','customint4','neq',1);
        }
        // mod_facetoface checking end


        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        $mform->addElement('hidden', 'courseid');
        $mform->setType('courseid', PARAM_INT);

        $this->add_action_buttons(true, ($instance->id ? null : get_string('addinstance', 'enrol')));

        $this->set_data($instance);
    }

    function validation($data, $files) {
        global $DB, $CFG;
        $errors = parent::validation($data, $files);

        list($instance, $plugin, $context) = $this->_customdata;

        if (!empty($data['enrolenddate']) and $data['enrolenddate'] < $data['enrolstartdate']) {
            $errors['enrolenddate'] = get_string('enrolenddaterror', 'enrol_ecommerce');
        }

        $cost = str_replace(get_string('decsep', 'langconfig'), '.', $data['cost']);
        if (!is_numeric($cost)) {
            $errors['cost'] = get_string('costerror', 'enrol_ecommerce');
        }

        return $errors;
    }
}
