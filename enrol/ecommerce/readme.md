# OVERVIEW
enrol_ecommerce is a Moodle/Totara plugin that allows a student/learner to enrol into a course after paying the specified enrolment fee through an appropriate payment gateway. It is loosely based off the enrol/paypal plugin.

# FUNCTIONALITY
This plugin includes the following core functionality.

- Configuration to connect to Westpac Payment gateway (originally developed for NABERS Learn - OEH). This might be extended in future to work with other Payment Gateways (for example SecurePay).
- Save payment gateway transaction data to the database (in {enrol_ecommerce) table).
- Generate and email PDF receipt on successful payment
- Allow user to view past invoices/receipts
