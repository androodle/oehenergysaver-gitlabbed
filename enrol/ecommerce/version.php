<?php

/**
 *-----------------------------------------------------------------------------
 * eCommerce enrolment plugin version information
 *-----------------------------------------------------------------------------
 * @package    enrol_ecommerce
 * @copyright  Androgogic Pty Ltd
 * @author     Androgogic Pty Ltd
 * @license    Commercial (http://www.androgogic.com)
 *-----------------------------------------------------------------------------
 **/

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2015070702;        	// The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2013050100;        	// Requires this Moodle version
$plugin->component = 'enrol_ecommerce';     // Full name of the plugin (used for diagnostics)
$plugin->cron      = 60;
