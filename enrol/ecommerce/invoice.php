<?php

/**
 *-----------------------------------------------------------------------------
 * eCommerce enrolment gateway process - invoice
 *-----------------------------------------------------------------------------
 * @package    enrol_ecommerce
 * @copyright  Androgogic Pty Ltd
 * @author     Androgogic Pty Ltd
 * @license    Commercial (http://www.androgogic.com)
 *-----------------------------------------------------------------------------
 **/

require('../../config.php');

require_once(dirname(dirname(__FILE__)) . '/../config.php');
require_once('lib.php');

// Payment ID (from enrol_ecommerce table)
$paymentid = optional_param('paymentid', 0, PARAM_INT);

// Should the invoice be displayed on screen (PDF) or emailed
$display = optional_param('display', 1, PARAM_INT);

require_login();

// Get payment data
global $DB;

$payment_data = get_payment_data($paymentid);

if (empty($payment_data)) {
	redirect($CFG->wwwroot, get_string('paymentdatanotfound', 'enrol_ecommerce'));
}

// Get user that made payment
$user_data = $DB->get_record('user', array('id' => $payment_data->userid));
if (empty($user_data)) {	
	redirect($CFG->wwwroot, get_string('paymentdatanotfound', 'enrol_ecommerce'));
}

// Get course
$course_data = $DB->get_record('course', array('id' => $payment_data->courseid));
if (empty($course_data)) {
	redirect($CFG->wwwroot, get_string('paymentdatanotfound', 'enrol_ecommerce'));
}

// Get the enrolment plugin configuration
$plugin = enrol_get_plugin('ecommerce');

// Get images path
$local_images_path = "$CFG->wwwroot/enrol/ecommerce/pix";

// Define PDF styles
$html = '<style>'.file_get_contents('invoice.css').'</style>';

// Define container table (3 columns, left padding, content, right padding)
$html .= '<p></p><p></p><p></p><p></p>';
$html .= '<table><tr><td style="width:10%"></td><td style="width:79%">'; 

// Invoice header table
$html .= '<table cellspacing="0" cellpadding="0" width="100%" border="0">';

// Invoice logo and company information
$html .= '<tr>';
$html .= '	<td width="52%">';
$html .= '		<img class="logo" src="'."$local_images_path/logo.png".'"/>';
$html .= '  </td>';
$html .= '	<td width="48%">';
$html .= '		<h1>'.get_string('invoicecompany', 'enrol_ecommerce').': TAX INVOICE</h1>';
$html .= '		<span class="contact"><strong>ABN:&nbsp;</strong>'.get_string('invoiceabn', 'enrol_ecommerce').'<br/>';
$html .= '		<strong>Email: </strong>'.get_string('invoiceemail', 'enrol_ecommerce').'<br/>';
$html .= '		<strong>Phone: </strong>'.get_string('invoicephone', 'enrol_ecommerce').'<br/>';
$html .= '		<strong>Address: </strong>'.get_string('invoiceaddress', 'enrol_ecommerce');
$html .= '		</span>';
$html .= '	</td>';
$html .= '</tr>';
$html .= '</table>';

$html .= '<p></p><p></p>';

// Invoice to table
$html .= '<table class="invoice-to" cellspacing="0" cellpadding="8" width="100%" border="1">';

$html .= '<tr>';
$html .= '	<td width="100%"><h2>Invoice To</h2></td>';
$html .= '</tr>';


// Student's name
$html .= '<tr>';
$html .= '	<td width="25%"><strong>Name:&nbsp;</strong></td>';
$html .= '	<td width="75%">'.fullname($user_data).'</td>';
$html .= '</tr>';

// Student's email address
$html .= '<tr>';
$html .= '	<td width="25%"><strong>Email address:&nbsp;</strong></td>';
$html .= '	<td width="75%">'.$user_data->email.'</td>';
$html .= '</tr>';

// End of invoice to table
$html .= '</table>';
$html .= '<p></p><p></p><p></p>';

// Payment summary table
$html .= '<table class="payment-summary" cellspacing="0" cellpadding="8" width="100%" border="1">';

// Payment summary 
$html .= '<tr>';
$html .= '	<td width="100%"><h2>Payment summary</h2></td>';
$html .= '</tr>';

// Date of purchase
$html .= '<tr>';
$html .= '	<td width="25%"><strong>Payment Date:&nbsp;</strong></td>';
$html .= '	<td width="75%">'.$payment_data->created_dttm.'</td>';
$html .= '</tr>';

// Payment reference
$html .= '<tr>';
$html .= '	<td width="25%"><strong>Payment Reference:&nbsp;</strong></td>';
$html .= '	<td width="75%">'.$payment_data->payment_reference.'</td>';
$html .= '</tr>';

// Receipt number
$html .= '<tr>';
$html .= '	<td width="25%"><strong>Receipt Number:&nbsp;</strong></td>';
$html .= '	<td width="75%">'.$payment_data->receipt_number.'</td>';
$html .= '</tr>';

// Payment type
$html .= '<tr>';
$html .= '	<td width="25%"><strong>Payment type:&nbsp;</strong></td>';
$html .= '	<td width="75%">Credit card - '.$payment_data->card_scheme.'</td>';
$html .= '</tr>';

// End of payment summary table
$html .= '</table>';
$html .= '<p></p><p></p><p></p>';

// Invoice details table
$html .= '<table class="invoice-details" cellspacing="0" cellpadding="8" width="100%" border="1">';

$html .= '<tr>';
$html .= '	<td width="100%"><h2>Invoice Details</h2></td>';
$html .= '</tr>';

$html .= '<tr>';
$html .= '	<td width="22%"><strong>Item</strong></td>';
$html .= '	<td width="8%"><strong>Qty</strong></td>';
$html .= '	<td width="55%"><strong>Course Name</strong></td>';
$html .= '	<td width="15%"><strong>Amount</strong></td>';
$html .= '</tr>';

// Course details
$html .= '<tr>';
$html .= '	<td width="22%">Course Enrolment</td>';
$html .= '	<td width="8%">1</td>';
$html .= '	<td width="55%">'.$course_data->shortname.'</td>';
$html .= '	<td width="15%">'."$".number_format($payment_data->payment_amount, 2).'</td>';
$html .= '</tr>';

// Total
$html .= '<tr>';
$html .= '	<td width="85%" class="right"><strong>Invoice Total (including GST)</strong></td>';
$html .= '	<td width="15%">'."$".number_format($payment_data->payment_amount, 2).'</td>';
$html .= '</tr>';

// End of invoice details table
$html .= '</table>';
$html .= '<p></p><p></p><p></p>';

// Invoice messages table
$html .= '<table class="invoice-messages" cellspacing="0" cellpadding="8" width="100%" border="1">';

// Invoice messages
$html .= '<tr>';
$html .= '	<td width="100%"><h2>Messages and Notes</h2></td>';
$html .= '</tr>';

$html .= '<tr>';
$html .= '	<td width="100%"><span class="messages">';
$html .=    get_string('invoicemessagethankyou', 'enrol_ecommerce').'<br/>';
$html .=    get_string('invoicemessageinvoices', 'enrol_ecommerce');
$html .=    '<p>'.get_string('invoicehelp', 'enrol_ecommerce').'</p>';
$html .= 	'</span></td>';
$html .= '</tr>';

// End of invoice messages table
$html .= '</table>';

// End of container table
$html .= '</td><td style="width:10%"></td></tr></table>';

$pdf_file_path = $CFG->dataroot . '/temp/';
$pdf_file_name = "Invoice_".$payment_data->id.'_'.str_replace('/', '_', $payment_data->payment_reference).'.pdf';

if ($display == 0 && $payment_data->invoice_emailed == 0) {
	
	// Email the invoice to the user

	// Email from
	$email_from_user = $plugin->get_config('invoiceemailfromuser');
	if (empty($email_from_user)) { // Use default email from user
		$email_from_user = get_string('invoiceemailfromuserdefault', 'enrol_ecommerce');
	}
        $email_from_address = $plugin->get_config('invoiceemailfrom');
	if (empty($email_from_address)) { // Use default email from
		$email_from_address = get_string('invoiceemailfromdefault', 'enrol_ecommerce');
	}
        $from = get_admin();
        $from->email = $email_from_address;
        $from->firstname = $email_from_user;
        $from->lastname = ''; //easiest way of getting the single field to cover both names
        
	// Email subject
	$email_subject = $plugin->get_config('invoiceemailsubject');
	if (empty($email_subject)) { // Use default email subject
		$email_subject = get_string('invoiceemailsubjectdefault', 'enrol_ecommerce');
	}
	$email_subject = str_replace('{course}', $course_data->shortname, $email_subject);

	// Email message
	$email_message = $plugin->get_config('invoiceemailmessage');
	if (empty($email_message)) { // Use default email message
		$email_message = get_string('invoiceemailmessagedefault', 'enrol_ecommerce');
	}
	$email_message = str_replace('{course}', $course_data->shortname, $email_message);

	// Generate the PDF file in the data directory
	generate_pdf($pdf_file_path.$pdf_file_name, $html, "P", "F");	

	$attachment_file = "temp/".$pdf_file_name; // local reference to file
    $attachment_name = $pdf_file_name;

    email_to_user($user_data, $from, $email_subject, $email_message, $email_message, $attachment_file, $attachment_name);
	
    // Update the enrol_ecommerce table to indicate that an invoice has been emailed to the user
    $q = "update {enrol_ecommerce} set invoice_emailed = :time where id = :id";
	$DB->execute($q, array('time' => time(), 'id' => $paymentid));

	// Clean up (remove the generated invoice after sending)	
	unlink($pdf_file_path.$pdf_file_name);
        if($course_data->id == 0){
            redirect($CFG->wwwroot.'/my', get_string('invoiceemailsent', 'enrol_ecommerce'));
        }
        else{
            redirect($CFG->wwwroot.'/course/view.php?id='.$course_data->id, get_string('invoiceemailsent', 'enrol_ecommerce'));
        }

}
else {
	error_log('enrol_ecommerce->invoice.php: invoice has already been emailed once');
	// Generate the PDF and display in the browser
	generate_pdf($pdf_file_name, $html, "P", "I");
}
