<?php

/**
 *-----------------------------------------------------------------------------
 * eCommerce enrolment plugin english language pack
 *-----------------------------------------------------------------------------
 * @package    enrol_ecommerce
 * @copyright  Androgogic Pty Ltd
 * @author     Androgogic Pty Ltd
 * @license    Commercial (http://www.androgogic.com)
 *-----------------------------------------------------------------------------
 **/

$string['pluginname'] = 'eCommerce';
$string['plugindesc'] = 'eCommerce paid module enrolments';

// Configuration

$string['defaults'] = 'Enrolment Defaults';
$string['defaultcost'] = 'Default enrolment cost';

$string['gatewaysettings'] = 'Payment Gateway Settings';
$string['gatewayusername'] = 'Username';
$string['gatewaypassword'] = 'Password';
$string['gatewaybusinesscode'] = 'Business code';
$string['gatewaynotificationemail'] = 'Notification email';
$string['gatewayglaccount'] = 'SAP GL Account';
$string['gatewaycashjournal'] = 'SAP Cash Journal';
$string['gatewaycommunitycode'] = 'Community code';
$string['gatewaytokenurl'] = 'Token Request URL';
$string['gatewayhandoffurl'] = 'Hand-off Redirect URL';

$string['invoiceemailsettings'] = 'Invoice Email Settings';
$string['invoiceemailfromuser'] = 'Email From User';
$string['invoiceemailfromuserdefault'] = 'NABERS Administration';
$string['invoiceemailfromuserexplain'] = 'User the email will appear to be sent from';
$string['invoiceemailfrom'] = 'Email From Address';
$string['invoiceemailfromdefault'] = 'nabers@environment.nsw.gov.au';
$string['invoiceemailfromexplain'] = 'Email address the email will appear to be sent from';
$string['invoiceemailsubject'] = 'Email Subject Prefix';
$string['invoiceemailsubjectdefault'] = 'NABERS Learn: Enrolment Invoice for {course}';
$string['invoiceemailsubjectexplain'] = 'NOTE {course} will be replaced with the course name';
$string['invoiceemailmessage'] = 'Email Message Body';
$string['invoiceemailmessagedefault'] = 'Thanks you for your enrolment into {course}. Attached with this email is your 
receipt of payment. Note that you can also access this receipt at any time through 
the NABERS site in your user profile.

Regards, NABERS Learning.
';
$string['invoiceemailmessageexplain'] = 'NOTE {course} will be replaced with the course name in this text.';

// Messages/text

$string['assignrole'] = 'Role assigned to users';
$string['cost'] = 'Standard cost to enrol in this module';
$string['discountexplanation'] = 'In some cases discounts apply. If you have been provided with a discount code by the National '
        . 'Administrator, enter it below and click the button to recalculate your payment, then click "Proceed with payment" button.';
$string['enrolenddate'] = 'Enrolment end date';
$string['enrolmentfailed'] = 'Sorry, enrolment has failed, please contact us to investigate this further.';
$string['enrolperiod'] = 'Enrolment period';
$string['enrolstartdate'] = 'Enrolment start date';
$string['invoiceemailsent'] = 'Thank you for your payment, your invoice invoice has been emailed to you.';
$string['nocost'] = 'No cost specified for module, cannot use eCommerce plugin';
$string['paymentgatewaynote'] = 'A new window will open to Westpac QuickWeb where you will be able to securely pay the nominated amount for enrolment into this course';
$string['paymentinstructions'] = 'The following form will allow you to pay for enrolment into this course online.
It uses QuickWeb, a payment gateway hosted by Westpac bank. Your payment will be securely procesed through this 
payment gateway in a new window. Once complete, you will be enrolled in the module and a receipt will be sent to
your email address.';
$string['paymentrequired'] = 'Payment required to enrol in this module.';
$string['proceedwithpayment'] = 'Proceed with Payment';
$string['getdiscount'] = 'Click here to apply your discount';
$string['discountclickhere'] = 'Click here to use a discount code';
$string['promocode1'] = 'Promotional code 1 (optional)';
$string['promocost1'] = 'Promotional cost 1 (optional)';
$string['promocode2'] = 'Promotional code 2 (optional)';
$string['promocost2'] = 'Promotional cost 2 (optional)';
$string['promocode3'] = 'Promotional code 3 (optional)';
$string['promocost3'] = 'Promotional cost 3 (optional)';
$string['receiptsheading'] = 'Receipts';
$string['receiptslink'] = 'Access receipts for your paid enrolments';
$string['status'] = 'Allow eCommerce Enrolments';
$string['successfullyenrolled'] = 'Thank you for your payment!, You will also be emailed a payment receipt. <br/><br/> You are now enrolled in your course: {$a}';
$string['successfullyenrolledfree'] = 'You are now enrolled in your course: {$a}';

// Errors

$string['paymentdatanotfound'] = 'Sorry, payment data not found for that payment ID';
$string['paymentgatewayerror'] = 'Sorry, the payment gateway is current unavailable.';
$string['paymentgatewayerrorinvalidcost'] = 'Sorry, there has been a problem retrieving the cost for this module.';
$string['paymentgatewayerrorinvalidinstanceid'] = 'Sorry, an invalid enrolment instance was used to access the payment gateway.';
$string['paymentgatewayerrorinvalidsetup'] = 'Sorry, the payment gateway has not been correctly configured, payments cannot be processed until this is corrected.';
$string['paymentgatewayerrorreturn'] = 'Sorry, invalid return parameters received from gateway.';

// Invoice

$string['invoicecompany'] = 'NABERS Learn';
$string['invoiceabn'] = '30 841 387 271';
$string['invoiceaddress'] = 'PO Box A290, Sydney South, NSW 1232';
$string['invoiceemail'] = 'nabers@environment.nsw.gov.au';
$string['invoicephone'] = '(02) 9995 5000 (ask for the NABERS team)';
$string['invoicemessagethankyou'] = 'Thank you for your purchase and enrolment into this course.';
$string['invoicemessageinvoices'] = 'You can always access this and any other invoices through your '.$string['invoicecompany'].' user profile.';
$string['invoicehelp'] = 'If you need assistance or have any questions, please contact us either <br/>by email: <strong>'.$string['invoiceemail']. '</strong> or <br/>by phone: <strong>'.$string['invoicephone'] . '</strong>';

// Refund policy

$string['refundpolicytitle'] = 'NABERS Learn Refund Policy';
$string['refundonlinecoursestitle'] = 'Online Courses and Exam Fees';
$string['refundonlinecoursesdetails'] = '<strong>No Refund:</strong> Online course and exam fees, once paid and processed, are not refundable under any circumstances.';
$string['refundfacetofacetitle'] = 'Face to Face Training Courses';
$string['refundfacetofacedetails'] = '<strong>Cancellation / Refund / Transfer Policy: </strong> A full refund will be paid on cancellations made 10 working days 
before the seminar date when made in writing nabers@environment.nsw.gov.au noting ‘Training Enquiry’ in the subject heading. Cancellations made less than 10 
working days before the seminar will not be accepted and will be charged the full fee; registrants may elect to send a replacement in their place. Trainees who 
miss a session due to illness can transfer to another session (pending availability) refunds will only be paid on the provision of a medical certificate.  
Only one transfer is given when made in writing to nabers@environment.nsw.gov.au 5 working days before the course date.';

// Reports

$string['reportecommercetransactions'] = 'eCommerce Transactions Report';

// mod_facetoface sign-up
$string['f2fheader'] = 'Face-to-face settings';
$string['f2fenabled'] = 'Enable Face-to-face availability check';
$string['f2fsignup'] = 'Enable Face-to-face auto sign-up';
$string['notificationtype'] = 'Default F2F notification type';
$string['defaultnotificationtype'] = 'Default notification type';
$string['defaultnotificationtype_help'] = 'Default notification type for users on auto sign-up to Face-to-Face';
$string['f2fsessionsunavailable'] = 'Unfortunately, there are no available sessions in this course. Sessions have either been booked, or expired.';
$string['f2fsignedupuser'] = ' You have been signed-up into a Face-to-face session';

