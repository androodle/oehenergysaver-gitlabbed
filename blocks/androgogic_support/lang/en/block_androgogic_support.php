<?php
/** 
 * Androgogic Support Block: Language Pack
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     06/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

$string['pluginname'] = 'Androgogic Support';
$string['plugintitle'] = 'Androgogic Support';

//spares
$string['support_form'] = 'Support form';
$string['uploaded_file'] = 'Upload file';
//$string['support_email_address'] = '';
$string['support_to_email_address'] = 'Support "to" email address';
$string['support_to_email_address_explanation'] = 'Support tickets will be sent here. This address must be set up for receiving tickets';
$string['support_from_email_address'] = 'Support "from" email address';
$string['support_from_email_address_explanation'] = 'Support tickets will be sent from this one';
$string['support_replyto_email_address'] = 'Support "reply to" email address';
$string['support_replyto_email_address_explanation'] = 'This address will be used for reply to field';
//faq items
$string['faq'] = 'FAQ';
$string['question'] = 'Question';
$string['answer'] = 'Answer';
$string['listing_order'] = 'Listing Order';
$string['faq_search'] = 'FAQ Admin';
$string['faq_plural'] = 'FAQs';
$string['faq_new'] = 'New FAQ';
$string['faq_edit'] = 'Edit FAQ';
//common items
$string['datasubmitted'] = 'The data has been submitted';
$string['itemdeleted'] = 'The item has been deleted';
$string['noresults'] = 'There were no results from your search';
$string['block_androgogic_support:edit'] = 'Edit objects within the Androgogic Support block';
$string['block_androgogic_support:delete'] = 'Delete objects within the Androgogic Support block';
$string['created_by'] = 'Created By';
$string['date_created'] = 'Date Created';
$string['modified_by'] = 'Modified By';
$string['date_modified'] = 'Date Modified';
$string['id'] = 'Id';
$string['faq_search_instructions'] = 'Search by Question or  Answer';
//server_status items
$string['server_status'] = 'Server Status';
$string['name'] = 'Name';
$string['summary'] = 'Summary';
$string['active'] = 'Active';


$string['server_status_new'] = 'New Server Status';
$string['server_status_edit'] = 'Edit Server Status';
$string['server_status_search_instructions'] = 'Search by Name or  Summary';

$string['server_status_search'] = 'Server Status Admin';
$string['server_status_plural'] = 'Server Statuses';
//support_log items
$string['support_log'] = 'Support Log';
$string['first_name'] = 'First Name';
$string['last_name'] = 'Last Name';
$string['email'] = 'Email';
$string['contact_number'] = 'Contact Number';
$string['problem_description'] = 'Problem Description';
$string['steps_to_reproduce'] = 'Steps To Reproduce';
$string['uploaded_file'] = 'Uploaded File';
$string['site_name'] = 'Site Name';
$string['page_before_support'] = 'Page Before Support';
$string['user'] = 'User';
$string['support_log_search'] = 'Support Logs';
$string['support_log_plural'] = 'Support Logs';
$string['support_log_new'] = 'Support Request';
$string['support_log_edit'] = 'Edit Support Log';
$string['support_log_search_instructions'] = 'Search by user details or problem description';
$string['support_admin'] = 'Support Admin';
$string['support_request_submitted'] = 'Your support request has been submitted and you should receive an automated email confirmation shortly';