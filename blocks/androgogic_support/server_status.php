<?php

/**
 * Androgogic Support Block: server status
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     06/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Display server statuses to user and link to support form
 * This page is for end users (as opposed to admins - for this go to server status search)
 *
 * */
//params
$tab = optional_param('tab', 'server_status', PARAM_TEXT);
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('tab'));

require_capability('block/androgogic_support:view', $context);

$q = "select DISTINCT a.*  
from mdl_androgogic_server_status a 
where active = 1 
order by listing_order";
$results = $DB->get_records_sql($q);

//RESULTS
if ($results) {
    foreach ($results as $result) {
        echo '<h3>' . $result->name . '</h3>';
        echo format_text($result->summary, FORMAT_HTML) . '<br/>';
    }
}
$page_before_support = '';
if(isset($_SERVER['HTTP_REFERER'])){
    $page_before_support = urlencode($_SERVER['HTTP_REFERER']);
}
//link to support form
echo "<a href='{$CFG->wwwroot}/blocks/androgogic_support/index.php?tab=support_log_new&page_before_support=$page_before_support'>" . get_string('support_form', 'block_androgogic_support') . "</a>";
?>
