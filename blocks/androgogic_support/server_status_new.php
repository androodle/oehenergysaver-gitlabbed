<?php

/** 
 * Androgogic Support Block: Create object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     06/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Create new server_status
 *
 **/

global $OUTPUT;

require_capability('block/androgogic_support:edit', $context);

require_once('server_status_edit_form.php');
$mform = new server_status_edit_form();
if ($data = $mform->get_data()){
$data->created_by = $USER->id;
$data->date_created = date('Y-m-d H:i:s');
$data->summary = format_text($data->summary['text'], $data->summary['format']);
$newid = $DB->insert_record('androgogic_server_status',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_support'), 'notifysuccess');
echo $OUTPUT->action_link($PAGE->url, 'Create another item');
}
else{
echo $OUTPUT->heading(get_string('server_status_new', 'block_androgogic_support'));
$mform->display();
}

?>
