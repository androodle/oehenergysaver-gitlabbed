<?php

/** 
 * Androgogic Support Block: Search form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     06/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides search form for the object.
 * This is used by search page
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class server_status_search_form extends moodleform {
function definition() {
global $DB;
$mform =& $this->_form;
foreach($this->_customdata as $custom_key=>$custom_value){
$$custom_key = $custom_value;
}
$mform->addElement('html','<div>');
//search controls
$mform->addElement('text','search',get_string('server_status_search_instructions', 'block_androgogic_support'));
//hiddens
$mform->addElement('hidden','tab',$tab);
$mform->addElement('hidden','sort',$sort);
$mform->addElement('hidden','dir',$dir);
$mform->addElement('hidden','perpage',$perpage);
//button
$mform->addElement('submit','submit','Search');
$mform->addElement('html','</div>');
}
}
