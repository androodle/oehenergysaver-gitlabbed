<?php

/**
 * Androgogic Training History Block: Edit object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Edit one of the training_histories
 *
 * */
global $OUTPUT;
//require_capability('block/androgogic_training_history:admin', $context);
require_once('training_history_edit_form.php');
$id = required_param('id', PARAM_INT);
//need to have a snapshot of the th record for comparison
$q = "select DISTINCT a.* , mdl_androgogic_activities.name as activity, mdl_files.contextid, mdl_files.component, mdl_files.filearea, mdl_files.filename, mdl_files.itemid, mdl_files.id as fileid, CONCAT(mdl_user.firstname,' ',mdl_user.lastname) as user 
from mdl_androgogic_training_history a 
LEFT JOIN mdl_androgogic_activities  on a.activity_id = mdl_androgogic_activities.id
LEFT JOIN mdl_files  on a.file_id = mdl_files.itemid and mdl_files.itemid != 0 and mdl_files.filename != '.'
LEFT JOIN mdl_user  on a.user_id = mdl_user.id
where a.id = $id ";
$training_history = $DB->get_record_sql($q);
$mform = new training_history_edit_form();
if ($data = $mform->get_data()) {
    $data->id = $id;
    $data->modified_by = $USER->id;
    $data->date_modified = date('Y-m-d H:i:s');
    if (!isset($data->approved) && has_capability('block/androgogic_training_history:admin', $context)) {
        $data->approved = 0;
    } else if (block_androgogic_training_history_is_manager() && $data->user_id == $USER->id) {
        //can't approve your own th!
        $data->approved = 0;
        echo $OUTPUT->notification(get_string('cantapproveowntraininghistory', 'block_androgogic_training_history'), 'notifyfailure');
    }
    $DB->update_record('androgogic_training_history', $data);
    block_androgogic_training_history_cpd_points_update($data);
    block_androgogic_training_history_dimensions_update($data);
    if ($config->approval_workflow && has_capability('block/androgogic_training_history:admin', $context)) {
        // if they just changed the approved value to 1 then advise mgr
        $old_approved_value = $training_history->approved;
        $new_approved_value = $data->approved;

        if ($new_approved_value == '1' && $old_approved_value != '1') {
            block_androgogic_training_history_advise_user($data);
        }
        // if they just changed the assessed value to 1 then advise mgr
        $old_assessed_value = $training_history->assessed;
        $new_assessed_value = $data->assessed;
        if ($new_assessed_value == '1' && $old_assessed_value != '1') {
            block_androgogic_training_history_advise_manager($data);
        }
    }
    if($config->do_cpe_hours){
        block_androgogic_training_history_cpe_hours_update($data);
    }
    
    echo $OUTPUT->notification(get_string('datasubmitted', 'block_androgogic_training_history'), 'notifysuccess');
} else {
    echo $OUTPUT->heading(get_string('training_history_edit', 'block_androgogic_training_history'));
    $mform->display();
}
?>
