<?php

/** 
 * Androgogic Training History Block: Test cron
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

require_once('../../config.php');
require_once($CFG->dirroot.'/blocks/androgogic_training_history/lib.php');
$debug = 1;
$source='test cron';
//if we are testing the cron we want it to run at whatever hour
$config->run_cron_hours = '0;1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;20;21;22;23';
block_androgogic_training_history_cron($source);

// End of blocks/androgogic_training_history/test_cron.php
