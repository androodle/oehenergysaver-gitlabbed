<?php

/** 
 * Androgogic Training History Block: Create object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Create new training_history
 *
 **/

global $OUTPUT;
//require_capability('block/androgogic_training_history:admin', $context);
require_once('training_history_edit_form.php');
$mform = new training_history_edit_form();
if ($data = $mform->get_data()){
$data->created_by = $USER->id;
$data->date_created = date('Y-m-d H:i:s');
if(!isset($data->assessed)){
    $data->assessed = 0;
}
else if(block_androgogic_training_history_is_manager() && $data->user_id == $USER->id){
    //can't approve your own th!
    $data->assessed = 0;
    echo $OUTPUT->notification(get_string('cantapproveowntraininghistory','block_androgogic_training_history'), 'notifyfailure');
}

$newid = $DB->insert_record('androgogic_training_history',$data);
$data->id = $newid;

//have we got a phantom file id?
$sql = "SELECT * FROM mdl_files
WHERE itemid = '$data->file_id'";
$file_exists = $DB->get_records_sql($sql);
if($file_exists){
    //fix file entries
    $sql = "UPDATE mdl_files
    SET filearea = 'content',component='block_androgogic_training_history'
    WHERE itemid = '$data->file_id'";
    $DB->execute($sql);
}
else{
    //get rid of the phantom out of the th table
    $sql = "UPDATE mdl_androgogic_training_history
    SET file_id = NULL
    where id = '$data->id'";
    $DB->execute($sql);
}

if($config->approval_workflow){
    $data->id = $newid;
    if($data->assessed){
        block_androgogic_training_history_advise_manager($data);
    }
}
block_androgogic_training_history_cpd_points_update($data);
block_androgogic_training_history_dimensions_update($data);

if($config->do_cpe_hours){
    block_androgogic_training_history_cpe_hours_update($data);
}
echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_training_history'), 'notifysuccess');
echo $OUTPUT->action_link($PAGE->url, 'Create another item');
}
else{
echo $OUTPUT->heading(get_string('training_history_new', 'block_androgogic_training_history'));
$mform->display();
}

?>
