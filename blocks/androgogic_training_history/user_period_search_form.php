<?php

/** 
 * Androgogic User period Block: Search form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     01/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides search form for the object.
 * This is used by search page
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class user_period_search_form extends moodleform {
function definition() {
global $DB;
$mform =& $this->_form;
foreach($this->_customdata as $custom_key=>$custom_value){
$$custom_key = $custom_value;
}
$mform->addElement('html','<div>');
//search controls
$mform->addElement('text','search',get_string('user_period_search_instructions', 'block_androgogic_training_history'));
//hiddens
$mform->addElement('hidden','tab',$tab);
$mform->addElement('hidden','sort',$sort);
$mform->addElement('hidden','dir',$dir);
$mform->addElement('hidden','perpage',$perpage);
//button
$mform->addElement('submit','submit','Search');
$mform->addElement('html','</div>');
}
}
