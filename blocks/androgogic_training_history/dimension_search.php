<?php

/**
 * Androgogic Training History Block: Search
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     20/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Search dimensions
 * Also provides access to edit and delete functions if user has sufficient permissions
 *
 * */
//params
$sort = optional_param('sort', 'name', PARAM_RAW);
$dir = optional_param('dir', 'ASC', PARAM_ALPHA);
$page = optional_param('page', 0, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$search = optional_param('search', '', PARAM_TEXT);
$tab = optional_param('tab', 'dimension_search', PARAM_FILE);
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page', 'perpage', 'search', 'tab'));
// prepare columns for results table
$columns = array(
    "name",
);
foreach ($columns as $column) {
    $string[$column] = get_string("$column", 'block_androgogic_training_history');
    if ($sort != $column) {
        $columnicon = '';
        $columndir = 'ASC';
    } else {
        $columndir = $dir == 'ASC' ? 'DESC' : 'ASC';
        $columnicon = $dir == 'ASC' ? 'down' : 'up';
    }
    if ($column != 'details') {
        $$column = "<a href='$PAGE->url&amp;dir=$columndir&amp;sort=$column'>$string[$column]</a>";
    } else {
        $$column = $string[$column];
    }
}
//figure out the and clause from what has been submitted
$and = '';
if ($search != '') {
    $and .= " and a.name like '%$search%'";
}
$q = "select DISTINCT a.*  
from mdl_androgogic_dimensions a 
where 1 = 1 
$and 
order by $sort $dir";
if (isset($_GET['debug'])) {
    echo '$query : ' . $q . '';
}
//get a page worth of records
$results = $DB->get_records_sql($q, array(), $page * $perpage, $perpage);
//also get the total number we have of these
$q = "SELECT COUNT(DISTINCT a.id)
from mdl_androgogic_dimensions a 
 where 1 = 1  $and";

if (isset($_GET['debug'])) {
    echo '$query : ' . $q . '<br>';
}

$result_count = $DB->get_field_sql($q);
if ($download == '') {

    require_once('dimension_search_form.php');
    $mform = new dimension_search_form(null, array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'search' => $search, 'tab' => $currenttab));
    $mform->display();
    echo '<table width="100%"><tr><td width="50%">';
    echo $result_count . ' ' . get_string('dimension_plural', 'block_androgogic_training_history') . " found" . '<br>';
    echo '</td><td style="text-align:right;">';
    if (has_capability('block/androgogic_training_history:admin', $context)) {
        echo "<a href='index.php?tab=dimension_new'>" . get_string('dimension_new', 'block_androgogic_training_history') . "</a>";
    }
    echo '</td></tr></table>';
}

//RESULTS
if (!$results) {
    if ($download == '') {
        echo $OUTPUT->heading(get_string('noresults', 'block_androgogic_training_history', $search));
    }
} else {
    $table = new html_table();
    $table->head = array(
        $name,
        'Action'
    );
    $table->align = array("left", "left", "left", "left", "left", "left",);
    $table->width = "95%";
    $table->size = array("17%", "17%", "17%", "17%", "17%", "17%",);
    foreach ($results as $result) {
        $view_link = ''; //enable if wanted: "<a href='index.php?tab=dimension_view&id=$result->id'>View</a> "; 
        $edit_link = "";
        $delete_link = "";
        if (has_capability('block/androgogic_training_history:admin', $context)) {
// then we show the edit link
            $edit_link = "<a href='index.php?tab=dimension_edit&id=$result->id'>Edit</a> ";
        }
        if (has_capability('block/androgogic_training_history:admin', $context)) {
// then we show the delete link
            $delete_link = "<a href='index.php?tab=dimension_delete&id=$result->id' onclick='return confirm(\"Are you sure you want to delete this item?\")'>Delete</a> ";
        }
//link for file
        $file_link = '';
        if (isset($result->filename)) {
            $file_link = new moodle_url("/pluginfile.php/{$result->contextid}/{$result->component}/{$result->filearea}/" . $result->itemid . '/' . $result->filename);
        }
        $table->data[] = array(
            "$result->name",
            $view_link . $edit_link . $delete_link
        );
    }
}

if (!empty($table)) {
    if ($download != '') {
        //export the table to whatever they asked for
        block_androgogic_training_history_export_data($download, $table, $tab); 
    }
    else{
        echo html_writer::table($table);
        $pagingbar = new paging_bar($result_count, $page, $perpage, $PAGE->url);
        $pagingbar->pagevar = 'page';
        echo $OUTPUT->render($pagingbar);        
    }
}
?>
