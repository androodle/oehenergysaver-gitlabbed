<?php

/**
 * Androgogic Training History Block: Permissions
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 * */
$capabilities = array(
    'block/androgogic_training_history:admin' => array(
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'manager' => CAP_ALLOW,
        )
    )
);

// End of blocks/androgogic_training_history/db/access.php