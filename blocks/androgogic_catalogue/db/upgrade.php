<?php

function xmldb_block_androgogic_catalogue_upgrade($oldversion = 0) {
	
    global $CFG, $DB;

	$dbman = $DB->get_manager(); // loads ddl manager and xmldb classes
	
	$result = true;
	
	if ($result && $oldversion < 2014111200) {
		
        $table1 = new xmldb_table('androgogic_catalogue_eoi_c');
        $table1->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table1->add_field('eoiid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table1->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table1->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $result = $result && $dbman->create_table($table1);
		
        $table1 = new xmldb_table('androgogic_catalogue_eoi');
        $table1->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table1->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table1->add_field('firstname', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table1->add_field('lastname', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table1->add_field('email', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table1->add_field('notes', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
        $table1->add_field('added', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table1->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $result = $result && $dbman->create_table($table1);
		
	}
	
	if ($result && $oldversion < 2014120901) {
		$table = new xmldb_table('androgogic_catalogue_eoi');
		
		$field = new xmldb_field('organisation', XMLDB_TYPE_CHAR, '255', null, null, null, null);
		if (!$dbman->field_exists($table, $field)) {
			$dbman->add_field($table, $field);
		}
		
		$field = new xmldb_field('position', XMLDB_TYPE_CHAR, '255', null, null, null, null);
		if (!$dbman->field_exists($table, $field)) {
			$dbman->add_field($table, $field);
		}
		
		$field = new xmldb_field('industry', XMLDB_TYPE_CHAR, '255', null, null, null, null);
		if (!$dbman->field_exists($table, $field)) {
			$dbman->add_field($table, $field);
		}
		
		$field = new xmldb_field('city', XMLDB_TYPE_CHAR, '255', null, null, null, null);
		if (!$dbman->field_exists($table, $field)) {
			$dbman->add_field($table, $field);
		}
		
		$field = new xmldb_field('wants_training', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
		if (!$dbman->field_exists($table, $field)) {
			$dbman->add_field($table, $field);
		}
		
		$field = new xmldb_field('att_location', XMLDB_TYPE_TEXT, 'medium', null, null, null, null);
		if (!$dbman->field_exists($table, $field)) {
			$dbman->add_field($table, $field);
		}
		
		$field = new xmldb_field('source', XMLDB_TYPE_CHAR, '255', null, null, null, null);
		if (!$dbman->field_exists($table, $field)) {
			$dbman->add_field($table, $field);
		}
		
	}
	
	if ($result && $oldversion < 2015042801) {
		$table = new xmldb_table('androgogic_catalogue_eoi');
		
		$field = new xmldb_field('deleted', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
		if (!$dbman->field_exists($table, $field)) {
			$dbman->add_field($table, $field);
		}
		
    }
    
    return $result;
}
