<?php
/** 
 * Androgogic Catalogue Block: Permissions
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

$capabilities = array(
	'block/androgogic_catalogue:edit' => array(
		'captype' => 'write',
		'contextlevel' => CONTEXT_SYSTEM,
		'archetypes' => array(
			'manager' => CAP_ALLOW,
		)
	),
	'block/androgogic_catalogue:delete' => array(
		'captype' => 'write',
		'contextlevel' => CONTEXT_SYSTEM,
		'archetypes' => array(
			'manager' => CAP_ALLOW,
		)
	),
	'block/androgogic_catalogue:read_eoi' => array(
		'captype' => 'read',
		'contextlevel' => CONTEXT_SYSTEM,
		'archetypes' => array(
			'manager' => CAP_ALLOW,
		)
	),
	'block/androgogic_catalogue:edit_eoi' => array(
		'captype' => 'write',
		'contextlevel' => CONTEXT_SYSTEM,
		'archetypes' => array(
			'manager' => CAP_ALLOW,
		)
	),
);

// End of blocks/androgogic_catalogue/db/access.php