<?php

/**
 * Androgogic Catalogue Block: Upgrade
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     14/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 * */
function xmldb_block_androgogic_catalogue_install() {
    global $CFG, $DB;
    $result = true;

    // create tables for the block
    if (!$DB->get_manager()->table_exists('androgogic_catalogue_entries')) {
        $sql = "CREATE TABLE `mdl_androgogic_catalogue_entries` (
        `id` BIGINT(10) NOT NULL AUTO_INCREMENT,
        `name` VARCHAR(255) NOT NULL,
        `end_date` DATETIME DEFAULT NULL,
        `created_by` BIGINT(10) NOT NULL COMMENT 'relates to user table',
        `date_created` DATETIME NOT NULL,
        `description` TEXT,
        `modified_by` BIGINT(10) DEFAULT NULL COMMENT 'relates to user table',
        `date_modified` DATETIME DEFAULT NULL,
        `public` TINYINT(1) DEFAULT NULL COMMENT 'can be seen without login',
        PRIMARY KEY (`id`)
      ) ENGINE=INNODB DEFAULT CHARSET=utf8";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_catalogue_entry_courses')) {
        $sql = "CREATE TABLE `mdl_androgogic_catalogue_entry_courses` (
            `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
            `catalogue_entry_id` BIGINT(11) NOT NULL COMMENT 'relates to androgogic_catalogue_entries',
            `course_id` BIGINT(11) NOT NULL COMMENT 'relates to course',
            PRIMARY KEY (`id`),
            UNIQUE KEY `unq_androgogic_catalogue_entry_course` (`catalogue_entry_id`,`course_id`)
          ) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='many to many linking table'";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_catalogue_entry_locations')) {
        $sql = "CREATE TABLE `mdl_androgogic_catalogue_entry_locations` (
            `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
            `catalogue_entry_id` BIGINT(11) NOT NULL COMMENT 'relates to androgogic_catalogue_entries table',
            `location_id` BIGINT(11) NOT NULL COMMENT 'relates to androgogic_catalogue_entry_locations table',
            PRIMARY KEY (`id`),
            UNIQUE KEY `unq_catalogue_entry_location` (`catalogue_entry_id`,`location_id`)
          ) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='many to many linking table'";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_catalogue_entry_organisations')) {
        $sql = "CREATE TABLE `mdl_androgogic_catalogue_entry_organisations` (
            `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
            `catalogue_entry_id` BIGINT(11) NOT NULL COMMENT 'relates to androgogic_catalogue_entries',
            `organisation_id` BIGINT(11) NOT NULL COMMENT 'relates to org',
            PRIMARY KEY (`id`),
            UNIQUE KEY `unq_androgogic_catalogue_entry_organisation` (`catalogue_entry_id`,`organisation_id`)
          ) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='many to many linking table'";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_catalogue_entry_positions')) {
        $sql = "CREATE TABLE `mdl_androgogic_catalogue_entry_positions` (
            `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
            `catalogue_entry_id` BIGINT(11) NOT NULL COMMENT 'relates to androgogic_catalogue_entries',
            `position_id` BIGINT(11) NOT NULL COMMENT 'relates to pos',
            PRIMARY KEY (`id`),
            UNIQUE KEY `unq_androgogic_catalogue_entry_position` (`catalogue_entry_id`,`position_id`)
          ) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='many to many linking table'";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_catalogue_entry_programs')) {
        $sql = "CREATE TABLE `mdl_androgogic_catalogue_entry_programs` (
            `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
            `catalogue_entry_id` BIGINT(11) NOT NULL COMMENT 'relates to androgogic_catalogue_entries table',
            `program_id` BIGINT(11) NOT NULL COMMENT 'relates to prog table',
            PRIMARY KEY (`id`),
            UNIQUE KEY `unq_catalogue_entry_program` (`catalogue_entry_id`,`program_id`)
          ) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='many to many linking table'";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_catalogue_entry_competencies')) {
        $sql = "CREATE TABLE `mdl_androgogic_catalogue_entry_competencies` (
            `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
            `catalogue_entry_id` BIGINT(11) NOT NULL COMMENT 'relates to androgogic_catalogue_entries table',
            `competency_id` BIGINT(11) NOT NULL COMMENT 'relates to comp table',
            PRIMARY KEY (`id`),
            UNIQUE KEY `unq_catalogue_entry_competency` (`catalogue_entry_id`,`competency_id`)
          ) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='many to many linking table'";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_catalogue_locations')) {
        $sql = "CREATE TABLE `mdl_androgogic_catalogue_locations` (
            `id` INT(10) NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(50) DEFAULT NULL,
            `created_by` BIGINT(10) DEFAULT NULL,
            `date_created` DATE DEFAULT NULL,
            `modified_by` BIGINT(10) DEFAULT NULL,
            `date_modified` DATE DEFAULT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=INNODB DEFAULT CHARSET=utf8";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_catalogue_eoi')) {
        $sql = "CREATE TABLE `mdl_androgogic_catalogue_eoi` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`userid` int(11) DEFAULT NULL,
			`firstname` varchar(255) DEFAULT NULL,
			`lastname` varchar(255) DEFAULT NULL,
			`email` varchar(255) DEFAULT NULL,
			`notes` text,
			`added` datetime DEFAULT NULL,
			PRIMARY KEY (`id`)
		  ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_catalogue_eoi')) {
        $sql = "CREATE TABLE `mdl_androgogic_catalogue_eoi` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`userid` int(11) DEFAULT NULL,
			`firstname` varchar(255) DEFAULT NULL,
			`lastname` varchar(255) DEFAULT NULL,
			`email` varchar(255) DEFAULT NULL,
			`notes` text,
			`added` datetime DEFAULT NULL,
            `deleted` int(11) DEFAULT 0,
			PRIMARY KEY (`id`)
		  ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('mdl_androgogic_catalogue_eoi_c')) {
        $sql = "CREATE TABLE `mdl_androgogic_catalogue_eoi_c` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`eoiid` int(10) unsigned DEFAULT NULL,
			`courseid` int(10) unsigned DEFAULT NULL,
			PRIMARY KEY (`id`)
		  ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $DB->execute($sql);
    }
    return $result;
}

// End of blocks/androgogic_catalogue/db/upgrade.php

    