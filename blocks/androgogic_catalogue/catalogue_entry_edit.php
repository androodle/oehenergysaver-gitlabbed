<?php

/**
 * Androgogic Catalogue Block: Edit object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     15/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Edit one of the catalogue_entries
 *
 * */
global $OUTPUT;
require_once('catalogue_entry_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.*  
from mdl_androgogic_catalogue_entries a 
where a.id = $id ";
$catalogue_entry = $DB->get_record_sql($q);
$mform = new catalogue_entry_edit_form();
if ($data = $mform->get_data()) {
    $data->id = $id;
    $data->modified_by = $USER->id;
    $data->date_modified = date('Y-m-d H:i:s');
    $data->end_date = date('Y-m-d', $data->end_date);
    $data->description = format_text($data->description['text'], $data->description['format']);
    $DB->update_record('androgogic_catalogue_entries', $data);
    $DB->delete_records('androgogic_catalogue_entry_locations', array('catalogue_entry_id' => $id));
//many to many relationship: androgogic_catalogue_entry_locations
    if (isset($data->location_id)) {
        foreach ($data->location_id as $location_id) {
            $insert = new stdClass();
            $insert->catalogue_entry_id = $id;
            $insert->location_id = $location_id;
            $DB->insert_record('androgogic_catalogue_entry_locations', $insert);
        }
    }
    $DB->delete_records('androgogic_catalogue_entry_courses', array('catalogue_entry_id' => $id));
//many to many relationship: androgogic_catalogue_entry_courses
    if (isset($data->course_id)) {
        foreach ($data->course_id as $course_id) {
            $insert = new stdClass();
            $insert->catalogue_entry_id = $id;
            $insert->course_id = $course_id;
            $DB->insert_record('androgogic_catalogue_entry_courses', $insert);
        }
    }
    $DB->delete_records('androgogic_catalogue_entry_programs', array('catalogue_entry_id' => $id));
//many to many relationship: androgogic_catalogue_entry_programs
    if (isset($data->program_id)) {
        foreach ($data->program_id as $program_id) {
            $insert = new stdClass();
            $insert->catalogue_entry_id = $id;
            $insert->program_id = $program_id;
            $DB->insert_record('androgogic_catalogue_entry_programs', $insert);
        }
    }
    $DB->delete_records('androgogic_catalogue_entry_organisations', array('catalogue_entry_id' => $id));
//many to many relationship: androgogic_catalogue_entry_organisations
    if (isset($data->organisation_id)) {
        foreach ($data->organisation_id as $organisation_id) {
            $insert = new stdClass();
            $insert->catalogue_entry_id = $id;
            $insert->organisation_id = $organisation_id;
            $DB->insert_record('androgogic_catalogue_entry_organisations', $insert);
        }
    }
    $DB->delete_records('androgogic_catalogue_entry_positions', array('catalogue_entry_id' => $id));
//many to many relationship: androgogic_catalogue_entry_positions
    if (isset($data->position_id)) {
        foreach ($data->position_id as $position_id) {
            $insert = new stdClass();
            $insert->catalogue_entry_id = $id;
            $insert->position_id = $position_id;
            $DB->insert_record('androgogic_catalogue_entry_positions', $insert);
        }
    }
    $DB->delete_records('androgogic_catalogue_entry_competencies', array('catalogue_entry_id' => $id));
//many to many relationship: androgogic_catalogue_entry_competencies
    if (isset($data->competency_id)) {
        foreach ($data->competency_id as $competency_id) {
            $insert = new stdClass();
            $insert->catalogue_entry_id = $id;
            $insert->competency_id = $competency_id;
            $DB->insert_record('androgogic_catalogue_entry_competencies', $insert);
        }
    }
    echo $OUTPUT->notification(get_string('datasubmitted', 'block_androgogic_catalogue'), 'notifysuccess');
	redirect(
		new moodle_url('/blocks/androgogic_catalogue/index.php'), 
		get_string('you_will_be_redirected', 'block_androgogic_catalogue'),
		3
	);
} else {
    echo $OUTPUT->heading(get_string('catalogue_entry_edit', 'block_androgogic_catalogue'));
    $mform->display();
}
?>
