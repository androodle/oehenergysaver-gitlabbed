<?php

/**
 * Androgogic Catalogue Block: Search
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     15/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 * */

require_once('eoi_confirm_form.php');

$courses = optional_param('courses', null, PARAM_RAW);
$email = optional_param('email', null, PARAM_RAW);
$firstname = optional_param('firstname', null, PARAM_RAW);
$lastname = optional_param('lastname', null, PARAM_RAW);
$notes = optional_param('notes', null, PARAM_RAW);
$organisation = optional_param('organisation', null, PARAM_RAW);
$position = optional_param('position', null, PARAM_RAW);
$industry = optional_param('industry', null, PARAM_RAW);
$city = optional_param('city', null, PARAM_RAW);
$wants_training = optional_param('wants_training', null, PARAM_RAW);
$att_location = optional_param('att_location', null, PARAM_RAW);
$source = optional_param('source', null, PARAM_RAW);
$source_other = optional_param('source_other', null, PARAM_RAW);

echo $OUTPUT->heading(get_string('eoi_register_yours', 'block_androgogic_catalogue'));

if (!is_array($att_location)) {
    $att_location = explode(',', $att_location);
}

$mform = new eoi_confirm_form(null, array(
    'courses' => $courses,
    'email' => $email, 
    'firstname' => $firstname, 
    'lastname' => $lastname, 
    'notes' => $notes, 
    'organisation' => $organisation, 
    'position' => $position, 
    'industry' => $industry, 
    'city' => $city, 
    'wants_training' => $wants_training, 
    'att_location' => $att_location, 
    'source' => $source, 
    'source_other' => $source_other, 
));
$data = $mform->get_data();

if ($data) {
    $success = catalogue_eoi_register($data);
    if (!$success) {
        print_error(
            'eoi_error_saving', 
            'block_androgogic_catalogue',
            new moodle_url('/blocks/androgogic_catalogue/index.php')
        );
    } else {
        echo $OUTPUT->notification(get_string('eoi_success', 'block_androgogic_catalogue'), 'notifysuccess');
        redirect(
            new moodle_url('/blocks/androgogic_catalogue/index.php'), 
            get_string('you_will_be_redirected', 'block_androgogic_catalogue'),
            3
        );
    }
}

echo '<h3>'. get_string('eoi_your_details', 'block_androgogic_catalogue') .'</h3>';
echo get_string('eoi_form_label', 'block_androgogic_catalogue');
$mform->display();
