<?php
/** 
 * Androgogic Catalogue Block: Lib
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

define('CATALOGUE_EOI_LEVEL_NONE', 0);
define('CATALOGUE_EOI_LEVEL_REGISTERED', 1);
define('CATALOGUE_EOI_LEVEL_ALL', 2);

function user_can_see_catalogue_entry($catalogue_entry,$user_pos_org_data){
    global $DB;
    $user_can_see = true;
    // any org entries for this cat_entry?
    $q = "SELECT o.* FROM mdl_androgogic_catalogue_entry_organisations co 
        INNER JOIN mdl_org o ON co.organisation_id = o.id 
        WHERE catalogue_entry_id = $catalogue_entry->id";
    $cat_org_result = $DB->get_records_sql($q);
    
    // any pos entries for this cat_entry?
    $q = "SELECT o.* FROM mdl_androgogic_catalogue_entry_positions co 
        INNER JOIN mdl_pos o ON co.position_id = o.id 
        WHERE catalogue_entry_id = $catalogue_entry->id";
    $cat_pos_result = $DB->get_records_sql($q);
    
    if(!$cat_org_result && !$cat_pos_result){
        //no filters on this catalogue entry, so nothing else to do here
        return true;
    }
    else{
        $passed_org_test = false;
        $passed_pos_test = false;
        if($cat_org_result){
           if(!isset($user_pos_org_data->user_org_result)){
                //fail: no user org, but we do have cat org, so let's short circuit this
                return false;
            }
            foreach ($user_pos_org_data->user_org_result as $user_org) {
                // are any of the cat org paths upstream from the user org path?
                foreach ($cat_org_result as $cat_org) {
                    //close the paths, in case of false positives
                    if(stristr($user_org->path.'/',$cat_org->path.'/')){
                        //in other words, does the user belong to an organisation that either is the same as the cat org, or is among the descendants of it?
                        $passed_org_test = true;
                    }
                }
            }  
        }
        else{
            $passed_org_test = true;
        }
        if($cat_pos_result){
            if(!isset($user_pos_org_data->user_pos_result)){
                //fail: no user pos, but we do have cat pos, so let's short circuit this
                return false;
            }
            foreach ($user_pos_org_data->user_pos_result as $user_pos) {
                //does the user pos path match any of the cat_pos paths?
                foreach ($cat_pos_result as $cat_pos) {
                    if($user_pos->path == $cat_pos->path){
                        $passed_pos_test = true;
                    }
                    elseif(stristr($cat_pos->path,$user_pos->path.'/')){
                        //in other words, does the user hold a higher position within the hierarchy than the specified catalogue position?
                        $passed_pos_test = true;
                    }
                }
            }            
        }
        else{
            $passed_pos_test = true;
        }
    }    
    //if we got down to here then we should have the results of the 2 tests. if both are true then the user can see, if not, not
    if($passed_org_test && $passed_pos_test){
        $user_can_see = true;
    }
    else{
        $user_can_see = false;
    }
    return $user_can_see;
}
function get_user_pos_org_data(){
    global $USER,$DB;
    //get org data for the user
    $q = "SELECT o.* FROM mdl_pos_assignment pa
        INNER JOIN mdl_org o ON pa.organisationid = o.id
        WHERE  userid = {$USER->id} AND TYPE IN (1,2)";
    $user_org_result = $DB->get_records_sql($q);
    
    //get pos data for the user
    $q = "SELECT p.* FROM mdl_pos_assignment pa
        INNER JOIN mdl_pos p ON pa.positionid = p.id
        WHERE  userid = {$USER->id} AND TYPE IN (1,2)";
    $user_pos_result = $DB->get_records_sql($q);
    
    $return = new stdClass();
    $return->user_org_result = $user_org_result;
    $return->user_pos_result = $user_pos_result;
    return $return;
}

// Returns true if Expressions of Interest are allowed.
function catalogue_eoi_is_allowed() {
	
	global $USER, $CFG;
	
	if (empty($CFG->catalogue_eoi_level)) {
		// Expressions of Interest are disabled.
		return false;
	}
	
	if ($CFG->catalogue_eoi_level == CATALOGUE_EOI_LEVEL_REGISTERED) {
		// Expressions of Interest are allowed for registered users only.
		return (!empty($USER->id));
	}
	
	if ($CFG->catalogue_eoi_level == CATALOGUE_EOI_LEVEL_ALL) {
		// Expressions of Interest are allowed for everyone.
		return true;
	}
	
	// Just in case.
	return false;
}

function catalogue_eoi_build(&$data) {

	global $USER;
	
	// Populate EOI data if user is registered and logged in.
	if (!empty($USER->id)) {
		$data->firstname = $USER->firstname;
		$data->lastname = $USER->lastname;
		$data->email = $USER->email;
	}
	
	$eoi = new stdClass();
	$eoi->userid = (!empty($USER->id) ? $USER->id : 0);
	$eoi->firstname = $data->firstname;
	$eoi->lastname = $data->lastname;
	$eoi->email = $data->email;
	$eoi->notes = $data->notes;
	$eoi->organisation = (!empty($USER->institution) ? $USER->institution : $data->organisation);
	$eoi->position = $data->position;
	$eoi->industry = $data->industry;
	$eoi->city = $data->city;
	$eoi->wants_training = $data->wants_training;
	$eoi->att_location = implode(',', $data->att_location);
	$eoi->source = $data->source;
	$eoi->added = time();

	return $eoi;
}

function catalogue_eoi_load_for_course($courseid) {
	
	global $DB;
	
	$sql = "SELECT
            eoi.id,
			eoi.userid,
			eoi.firstname,
			eoi.lastname,
			eoi.email
		FROM {androgogic_catalogue_eoi_c} AS eoi_c
		LEFT JOIN {androgogic_catalogue_eoi} AS eoi ON eoi.id = eoi_c.eoiid
		WHERE eoi_c.courseid = ?
            AND eoi.deleted = 0";
	
	return $DB->get_records_sql($sql, array($courseid));
	
}

function catalogue_eoi_load_courses($eoiid) {
    global $DB;
    $sql = "SELECT 
            c.id,
            c.shortname
        FROM {androgogic_catalogue_eoi_c} ec
        LEFT JOIN {course} c ON c.id = ec.courseid
        WHERE ec.eoiid = ?";
    return $DB->get_records_sql($sql, array($eoiid));
}

function catalogue_eoi_register(&$data) {
	
	global $DB, $USER, $CFG;
	
	if (!catalogue_eoi_is_allowed()) {
		return false;
	}

	// Check if guests are not allowed to post.
	if ($CFG->catalogue_eoi_level == CATALOGUE_EOI_LEVEL_REGISTERED && empty($USER->id)) {
		return false;
	}
	
	$eoi = catalogue_eoi_build($data);
	// Save EOI data.
	$eoiid = $DB->insert_record('androgogic_catalogue_eoi', $eoi);
	if (!$eoiid) {
		return false;
	}
	
	$result = true;
	foreach ($data->courses as $courseid) {
        if (empty($courseid)) {
            continue;
        }
		$eoicourse = new stdClass();
		$eoicourse->eoiid = $eoiid;
		$eoicourse->courseid = $courseid;
		$result = $result && $DB->insert_record('androgogic_catalogue_eoi_c', $eoicourse);
	}
	
	if ($result) {
		catalogue_eoi_send_notice($eoi, $data->courses);
	}
	
	return $result;
}

function catalogue_eoi_all($sort_field = null) {
	
    global $DB, $SESSION;
    
    // Hacking into flexible_table class here.
    $sess_sortorder = null;
    if (!empty($SESSION->flextable['eoi-list']->sortby[$sort_field])) {
        $sess_sortorder = $SESSION->flextable['eoi-list']->sortby[$sort_field];
    }
    $sortorder = (!empty($sess_sortorder) && $sess_sortorder == 3) ? 'DESC' : 'ASC';
    
    if (empty($sort_field)) {
        $sort_field = 'id';
    }
	
	$results = $DB->get_records('androgogic_catalogue_eoi', null, $sort_field.' '.$sortorder);
	if (empty($results)) {
		return false;
	}
	
	foreach ($results as &$result) {
		
		$result->courses = array();
		
        $courses = catalogue_eoi_load_courses($result->id);
		if (empty($courses)) {
			continue;
		}
		
		foreach ($courses as $course) {
			$result->courses[] = $course;
			$result->courses_plain[] = $course->shortname; // for CSV
		}
		
	}
	
	return $results;
}

function catalogue_eoi_export_csv($sort_field = null) {
	
    global $CFG;

    require_once($CFG->libdir . '/csvlib.class.php');

    $filename = 'expressions_of_interest_' . date("Y-m-d");
	$fields = array(
        get_string('date'),
		get_string('firstname'),
		get_string('lastname'),
		get_string('organisation', 'block_androgogic_catalogue'),
		get_string('position', 'block_androgogic_catalogue'),
		get_string('courses'),
		get_string('custom_training', 'block_androgogic_catalogue'),
		get_string('comments'),
		get_string('email'),
		get_string('city'),
		get_string('preferred_location', 'block_androgogic_catalogue'),
		get_string('industry', 'block_androgogic_catalogue'),
		get_string('source', 'block_androgogic_catalogue'),
		get_string('inactive'),
	);
	
	$eois = catalogue_eoi_all($sort_field);
	if (empty($eois)) {
		die;
	}
	
    $csvexport = new csv_export_writer();
    $csvexport->set_filename($filename);
    $csvexport->add_data($fields);
	
    foreach ($eois as $eoi) {
		$line = new stdClass();
		$line->added = date('Y-m-d H:i:s', $eoi->added);;
		$line->firstname = $eoi->firstname;
		$line->lastname = $eoi->lastname;
		$line->organisation = $eoi->organisation;
		$line->position = $eoi->position;
		$line->courses = implode(", ", $eoi->courses_plain);
		$line->wants_training = (!empty($eoi->wants_training) ? 'Yes' : 'No');
		$line->notes = $eoi->notes;
		$line->email = $eoi->email;
		$line->city = $eoi->city;
		$line->att_location = $eoi->att_location;
		$line->industry = $eoi->industry;
		$line->source = $eoi->source;
		$line->deleted = (!empty($eoi->deleted) ? 'Yes' : 'No');
		$csvexport->add_data((array) $line);
    }
    $csvexport->download_file();
    die;
	
}

function catalogue_eoi_delete($id) {
	
	global $DB;
	
	if (empty($id)) {
		return false;
	}
    
    $data = new stdClass();
    $data->id = $id;
    $data->deleted = 1;
	
	return $DB->update_record('androgogic_catalogue_eoi', $data);
}

function catalogue_eoi_restore($id) {
	
	global $DB;
	
	if (empty($id)) {
		return false;
	}
    
    $data = new stdClass();
    $data->id = $id;
    $data->deleted = 0;
	
	return $DB->update_record('androgogic_catalogue_eoi', $data);
}

function catalogue_eoi_send_notice(&$data, $courses) {
	
	global $CFG, $DB;
	
	if (empty($CFG->catalogue_eoi_emails)) {
		// No emails entered.
		return true;
	}

	$result = true;
	$emails = explode(',', $CFG->catalogue_eoi_emails);
	foreach ($emails as $email) {
		
		$email = trim($email);
		if (!validate_email($email)) {
			continue;
		}
		
		$select = $DB->sql_like('email', ':email', false, true, false, '|') .
				" AND mnethostid = :mnethostid AND deleted=0 AND suspended=0";
		$params = array('email' => $DB->sql_like_escape($email, '|'), 'mnethostid' => $CFG->mnet_localhost_id);
		$user = $DB->get_record_select('user', $select, $params, '*', IGNORE_MULTIPLE);
		
		$supportuser = core_user::get_support_user();
		$subject = get_string('eoi_email_subject', 'block_androgogic_catalogue');
		$messagehtml = catalogue_eoi_email_build($data, $courses);
		$message = strip_tags($messagehtml);
		
		$result = $result && email_to_user($user, $supportuser, $subject, $message, $messagehtml);
		
	}
	
	return $result;
}

/**
 * Sends the notifications for all users who have expressed their interest
 * in the course.
 * 
 * @param type $labeleoi Label EOI activity data.
 * @return boolean
 */
function catalogue_eoi_send_update_notice($labeleoi) {
	
	if (empty($labeleoi->emailsend)) {
		return true;
	}
	
	// We have to find the course ID for the given Label EOI activity.
	$cm = get_coursemodule_from_instance('labeleoi', $labeleoi->id);
	
	$result = true;
	$receivers = catalogue_eoi_load_for_course($cm->course);
	if (empty($receivers)) {
		return true;
	}
		
	$supportuser = core_user::get_support_user();
	$subject = $labeleoi->emailsubject;

	foreach ($receivers as $data) {
		
		if (!validate_email($data->email)) {
			continue;
		}
        
        $unsub_params = array(
            'eoi_unsubscribe' => catalogue_eoi_user_signature($data),
        );
        $unsub_url = new moodle_url('/blocks/androgogic_catalogue/index.php', $unsub_params);
        
    	$messagehtml  = $labeleoi->emailtext;
        $messagehtml .= "<br>\n<br>\n";
        $messagehtml .= get_string('clickheretounsubscribe', 'block_androgogic_catalogue').': ';
        $messagehtml .= $unsub_url;
        
        $message = strip_tags($messagehtml);
		
		// Because Moodle only lets us email to registered users,
		// we have to mimic one, based on the data from EOI table.
		$user = clone $supportuser;
		$user->email = $data->email;
		$user->firstname = $data->firstname;
		$user->lastname = $data->lastname;
		
		$result = email_to_user($user, $supportuser, $subject, $message, $messagehtml)
			&& $result;
		
	}
	
	return $result;
}

function catalogue_eoi_user_signature($data) {
    $buf  = $data->id;
    $buf .= $data->firstname;
    $buf .= $data->lastname;
    $buf .= $data->email;
    return md5($buf);
}

function catalogue_eoi_unsubscribe($hash) {
    global $DB;
    
    // TODO very MySQL-specific!
    $sql = "UPDATE {androgogic_catalogue_eoi}
        SET deleted = 1
        WHERE MD5(CONCAT(id, firstname, lastname, email)) = ?";
    $params = array($hash);
    $DB->execute($sql, $params);
    
    $sql = "SELECT id 
        FROM {androgogic_catalogue_eoi}
        WHERE MD5(CONCAT(id, firstname, lastname, email)) = ?
            AND deleted = 1";
    return $DB->get_record_sql($sql, $params);
}

function catalogue_eoi_email_build(&$data, $courses) {
	
	global $DB;
	
	ob_start();
	
	echo '<b>'. get_string('course') .'</b>:'."\n";
	echo "<ul>\n";
	foreach ($courses as $courseid) {
		$course = $DB->get_record('course', array('id' => $courseid), 'shortname');
		if (!empty($course)) {
			$url = new moodle_url('/course/view.php', array('id' => $courseid));
			echo '<li><a href="'. $url .'">'. $course->shortname .'</a></li>'."\n";
		}
	}
	echo "</ul>\n";
	echo '<b>'. get_string('firstname') .'</b>: ' . $data->firstname . "<br>\n";
	echo '<b>'. get_string('lastname') .'</b>: ' . $data->lastname . "<br>\n";
	echo '<b>'. get_string('email') .'</b>: ' . $data->email . "<br>\n";
	echo '<b>'. get_string('organisation', 'block_androgogic_catalogue') .'</b>: ' . $data->organisation . "<br>\n";
	echo '<b>'. get_string('position', 'block_androgogic_catalogue') .'</b>: ' . $data->position . "<br>\n";
	echo '<b>'. get_string('industry', 'block_androgogic_catalogue') .'</b>: ' . $data->industry . "<br>\n";
	echo '<b>'. get_string('city') .'</b>: ' . $data->city . "<br>\n";
	echo '<b>'. get_string('wants_training', 'block_androgogic_catalogue') .'</b>: ' . (!empty($data->wants_training) ? 'Yes' : 'No') . "<br>\n";
	echo '<b>'. get_string('att_location', 'block_androgogic_catalogue') .'</b>: ' . $data->att_location . "<br>\n";
	echo '<b>'. get_string('source', 'block_androgogic_catalogue') .'</b>: ' . $data->source . "<br>\n";
	echo '<b>'. get_string('eoi_notes', 'block_androgogic_catalogue') .'</b>: ' . $data->notes . "<br>\n";
	
	$all_url = new moodle_url('/blocks/androgogic_catalogue/index.php', array('tab' => 'eoi_list'));
	echo "<br>\n";
	echo '<a href="'. $all_url .'">'. get_string('eoi_view_all', 'block_androgogic_catalogue') .'</a>';
	
	return ob_get_clean();
}

function catalogue_eoi_available_courses() {
    global $DB;
    
    $sql = "SELECT 
            c.id,
            c.fullname
        FROM {androgogic_catalogue_entry_courses} cec
        LEFT JOIN {course} c ON c.id = cec.course_id
        ORDER BY c.fullname ASC";
    
    return $DB->get_records_sql($sql);
}
