// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines the version of Lynda module
 *
 * @package    block
 * @subpackage androgogic_catalogue
 * @author     Sergey Vidusov
 * @copyright  2014 onwards Androgogic {@link http://androgogic.com.au}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

M.block_androgogic_catalogue = {
	Y: null,
	transaction: []
};

M.block_androgogic_catalogue.init_search = function(Y) {
	
	var courses = Y.one('#all-courses-form');
	var all_button = Y.one('#ac-search-results-trigger');
	
	if (typeof courses === 'undefined') {
		return;
	}
	
	var all_button_click = function() {
		courses.show();
		all_button.hide();
	}
	
	courses.hide();
	all_button.on('click', all_button_click);
}

M.block_androgogic_catalogue.search_is_on = function(Y) {

	Y.one('#all-courses-form').show();
	Y.one('#ac-search-results-trigger').hide();

	location.hash = "#ac-search-results";
}

M.block_androgogic_catalogue.init_eoi = function(Y) {
	
	var source_select_watchdog = function() {
		var val = s_select.get('value');
		if (val != 'Other') {
			s_input.set('value', '').hide();
			return;
		}
		s_input.show();
	}
	
	var att_select_watchdog = function() {		
		var options = att_select.get('options');
		var other_selected = false;
		for (var i = 0; i < options.size(); i++) {
			if (!options.item(i).get('selected')) {
				continue;
			}
			if (options.item(i).get('value') == 'Other') {
				other_selected = true;
			}
		}
		if (other_selected) {
			att_input.show();
		} else {
			att_input.set('value', '').hide();
		}
	}
	
	var s_select = Y.one('#mform1 select#id_source');
	var s_input = Y.one('#mform1 input#id_source_other');
	var att_select = Y.one('#mform1 select#id_att_location');
	var att_input = Y.one('#mform1 input#id_att_location_other');
	
	source_select_watchdog();
	att_select_watchdog();
	
	s_select.on('change', source_select_watchdog);
	att_select.on('change', att_select_watchdog);
	
}

