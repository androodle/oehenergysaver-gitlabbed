<?php
defined('MOODLE_INTERNAL') || die(); 

require_once $CFG->dirroot.'/blocks/androgogic_catalogue/lib.php';

if ($ADMIN->fulltree) {
	
    // Layout for the catalogue pages.
	$page_layout_values = array(
		'course' => 'Course',
		'coursecategory' => 'Course category',
		'frontpage' => 'Front page',
		'incourse' => 'In course',
		'mydashboard' => 'My dashboard',
		'mypublic' => 'My public',
		'noblocks' => 'No blocks',
		'report' => 'Report',
		'standard' => 'Standard',
	);
	$settings->add(new admin_setting_configselect(
		'catalogue_page_layout', 
		get_string('catalogue_page_layout', 'block_androgogic_catalogue'),
		get_string('catalogue_page_layout_explanation', 'block_androgogic_catalogue'), 
		'standard', 
		$page_layout_values
	));
	
	// Levels of access for Expressions of interest
	$eoi_levels = array(
		CATALOGUE_EOI_LEVEL_NONE => 'Not allowed',
		CATALOGUE_EOI_LEVEL_REGISTERED => 'Allowed only for registered users',
		CATALOGUE_EOI_LEVEL_ALL => 'Allowed for everyone',
	);
    $settings->add(new admin_setting_configselect(
		'catalogue_eoi_level', 
		get_string('catalogue_eoi_level', 'block_androgogic_catalogue'),
		get_string('catalogue_eoi_level_explanation', 'block_androgogic_catalogue'), 
		CATALOGUE_EOI_LEVEL_NONE, 
		$eoi_levels
	));
    
	// Emails for Expressions
    $settings->add(new admin_setting_configtextarea(
		'catalogue_eoi_emails', 
		get_string('catalogue_eoi_emails', 'block_androgogic_catalogue'),
		get_string('catalogue_eoi_emails_explanation', 'block_androgogic_catalogue'), 
		'', 
		PARAM_RAW
	));
    
}
