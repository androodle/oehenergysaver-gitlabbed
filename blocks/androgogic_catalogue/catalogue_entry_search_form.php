<?php

/** 
 * Androgogic Catalogue Block: Search form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     15/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides search form for the object.
 * This is used by search page
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
	die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->libdir . '/formslib.php');

class catalogue_entry_search_form extends moodleform {
	function definition() {
		
		global $DB, $TOTARA_COURSE_TYPES, $PAGE;
		
		$PAGE->requires->js('/blocks/androgogic_catalogue/lib.js');
		$PAGE->requires->js_init_call('M.block_androgogic_catalogue.init_search');
		
		$mform =& $this->_form;
		foreach($this->_customdata as $custom_key=>$custom_value){
			$$custom_key = $custom_value;
		}
		$debug = optional_param('debug', 0, PARAM_INT);
		$mform->addElement('html','<div>');
		//search controls
		$mform->addElement('text','search',get_string('catalogue_entry_search_instructions', 'block_androgogic_catalogue'));
		$mform->setType('search', PARAM_RAW);
		
		$dboptions = $DB->get_records_menu('androgogic_catalogue_locations',array(),'name','id,name');
		unset($options);
		$options[0] = 'Any';
		foreach($dboptions as $key=>$value){
			$options[$key] = $value;
		}
		$select = $mform->addElement('select', 'androgogic_catalogue_locations_id', get_string('location','block_androgogic_catalogue'), $options);
		$mform->setType('androgogic_catalogue_locations_id', PARAM_RAW);
		$dboptions = $DB->get_records_menu('comp',array('visible' => 1),'fullname','id,fullname');
		unset($options);
		$options[0] = 'Any';
		foreach($dboptions as $key=>$value){
			$options[$key] = $value;
		}
		$select = $mform->addElement('select', 'comp_id', get_string('competency','block_androgogic_catalogue'), $options);
		$mform->setType('comp_id', PARAM_RAW);

		//Course type
		$coursetypeoptions = array();
		$coursetypeoptions[-1] = 'Select';
		foreach($TOTARA_COURSE_TYPES as $k => $v) {
			$coursetypeoptions[$v] = get_string($k, 'totara_core');
		}
		$mform->addElement('select', 'coursetype', get_string('coursetype', 'block_androgogic_catalogue'), $coursetypeoptions);
		$mform->setType('coursetype', PARAM_RAW);

		// date filters
		$mform->addElement('date_selector','startdate',get_string('startdate', 'block_androgogic_catalogue'), array('optional'=>true));
		$mform->setType('startdate', PARAM_RAW);
		$mform->addElement('date_selector','enddate',get_string('enddate', 'block_androgogic_catalogue'), array('optional'=>true));
		$mform->setType('enddate', PARAM_RAW);

		//hiddens
		$mform->addElement('hidden','tab',$tab);
		$mform->setType('tab', PARAM_RAW);
		$mform->addElement('hidden','sort',$sort);
		$mform->setType('sort', PARAM_RAW);
		$mform->addElement('hidden','dir',$dir);
		$mform->setType('dir', PARAM_RAW);
		$mform->addElement('hidden','perpage',$perpage);
		$mform->setType('perpage', PARAM_RAW);
		$mform->addElement('hidden','debug',$debug);
		$mform->setType('debug', PARAM_RAW);
		
		$buttons = array();
		$cancel_url = new moodle_url('/blocks/androgogic_catalogue/');
		$buttons[] = $mform->createElement('submit', 'submit', get_string('search'));
		$buttons[] = $mform->createElement(
			'button', 
			'cancel', 
			get_string('clearsearch', 'block_androgogic_catalogue'), 
			array('onclick' => 'location.href="'. $cancel_url .'"')
		);
		$mform->addGroup($buttons, 'buttons', '', array(' '), false);
		
		$mform->addElement('html','</div>');
	}
}
