<?php
/** 
 * Androgogic Catalogue Block: Class
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

class block_androgogic_catalogue extends block_base {

	function init(){
		$this->title = get_string('plugintitle','block_androgogic_catalogue');
		$this->cron = 1;
	} //end function init

	function get_content(){
		global $CFG;		if ($this->content !== null) {
			return $this->content;
		}
		$this->content = new stdClass;
		$this->content->text = '<a href="'.$CFG->wwwroot.'/blocks/androgogic_catalogue/index.php">'.get_string('catalogue_entry_search','block_androgogic_catalogue').'</a><br>';
		return $this->content;

	} //end function get_content

	function cron(){

	} //end function cron
	
	function has_config() {
		return true;
	}

} //end class block_androgogic_catalogue

// End of blocks/androgogic_catalogue/block_androgogic_catalogue.php