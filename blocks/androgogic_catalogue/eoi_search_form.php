<?php

/** 
 * Androgogic Catalogue Block: Search form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     15/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides search form for the object.
 * This is used by search page
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
	die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->libdir . '/formslib.php');

class eoi_search_form extends moodleform {
    
	function definition() {
		
		$mform =& $this->_form;
        
        $mform->addElement('hidden', 'tab', 'eoi_list');
        $mform->setType('tab', PARAM_RAW);
		
		$mform->addElement('text', 'name', get_string('name'));
		$mform->setType('name', PARAM_RAW);
		
        $options = array(
            '-1' => '',
            0 => get_string('no'),
            1 => get_string('yes'),
        );
		$mform->addElement('select', 'deleted', get_string('deleted'), $options);
		$mform->setType('deleted', PARAM_INT);
		
		$buttons = array();
		$cancel_url = new moodle_url('/blocks/androgogic_catalogue/', array('tab' => 'eoi_list'));
		$buttons[] = $mform->createElement('submit', 'submit', get_string('search'));
		$buttons[] = $mform->createElement(
			'button', 
			'cancel', 
			get_string('clearsearch', 'block_androgogic_catalogue'), 
			array('onclick' => 'location.href="'. $cancel_url .'"')
		);
		$mform->addGroup($buttons, 'buttons', '', array(' '), false);
		
	}
}
