<?php
/** 
 * Androgogic Catalogue Block: Language Pack
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

$string['pluginname'] = 'Androgogic Catalogue';
$string['plugintitle'] = 'Androgogic Catalogue';

//spares
$string['noresults'] = 'There were no results from your search';
$string['startdate'] = 'Course starts between: ';
$string['enddate'] = 'and';

//catalogue_entry items
$string['catalogue'] = 'Catalogue';
$string['catalogue_entry'] = 'Catalogue Entry';
$string['catalogue_page_layout'] = 'Page Layout';
$string['catalogue_page_layout_explanation'] = 'Choose a predefined page layout for the catalogue pages.';
$string['catalogue_eoi_level'] = 'Expression of interest';
$string['catalogue_eoi_level_explanation'] = 'Determines the availability of users to register their interest in Catalogue courses.';
$string['catalogue_eoi_emails'] = 'Expression recipients';
$string['catalogue_eoi_emails_explanation'] = 'Enter email address (or several, comma-separated) which will receive the Expressions of Interest created by visitors. These emails <b>MUST</b> belong to one or several system users.';
$string['clearsearch'] = 'Clear search';
$string['coursetype'] = 'Course delivery method';
$string['downloadcsv'] = 'Download in CSV format';
$string['name'] = 'Name';
$string['emailinvalid'] = 'Invalid email address';
$string['end_date'] = 'End Date';
$string['eoi_confirm'] = 'Register your Interest';
$string['eoi_edit'] = 'Edit the Expression of Interest';
$string['eoi_error_saving'] = 'An error occurred while trying to save your Expression of Interest. Please try again later.';
$string['eoi_finalise'] = 'Submit';
$string['eoi_form_label'] = 'The Energy Saver training program is constantly updated to reflect current areas of interest. Please enter your details to proceed with registering your interest for future training courses.';
$string['eoi_list'] = 'Expressions of interest';
$string['eoi_success'] = 'Your interest in the courses you\'ve chosen has been registered.';
$string['eoi_notes'] = '3. Do you have any suggestions or comments?';
$string['eoi_nothing_chosen'] = 'You have not selected any courses.';
$string['eoi_nothing_found'] = 'No Expressions of Interest found.';
$string['eoi_register_yours'] = 'Register your Interest';
$string['eoi_your_courses'] = 'You indicated that you are interested in the following courses:';
$string['eoi_your_details'] = 'Enter your details';
$string['eoi_email_subject'] = 'New Expression of Interest has been registered';
$string['eoi_view_all'] = 'View all entries';
$string['description'] = 'Description';
$string['public'] = 'Public';
$string['location'] = 'Location';
$string['course'] = 'Course';
$string['program'] = 'Program';
$string['organisation'] = 'Organisation';
$string['position'] = 'Position';
$string['industry'] = 'Industry sector';
$string['jobtitle'] = 'Job title';
$string['capability_element'] = 'Capability Element';
$string['catalogue_entry_search'] = 'Catalogue Entries';
$string['catalogue_entry_plural'] = 'Catalogue Entries';
$string['catalogue_entry_new'] = 'New Catalogue Entry';
$string['catalogue_entry_edit'] = 'Edit Catalogue Entry';
//common items
$string['datasubmitted'] = 'The data has been submitted';
$string['itemdeleted'] = 'The item has been deleted';
$string['block_androgogic_catalogue:edit'] = 'Edit objects within the Androgogic Catalogue block';
$string['block_androgogic_catalogue:delete'] = 'Delete objects within the Androgogic Catalogue block';
$string['created_by'] = 'Created By';
$string['date_created'] = 'Date Created';
$string['modified_by'] = 'Modified By';
$string['date_modified'] = 'Date Modified';
$string['id'] = 'Id';
$string['you_will_be_redirected'] = 'You will now be redirected back to the catalogue.';
$string['register_eoi'] = 'If a course session isn’t available, please select the course(s) above and register your interest here';
$string['wants_training'] = '1. Are you interested in customised training in-house of any of the selected courses? (Note: You must have a minimum of 15 participants.)';
$string['wants_training_short'] = 'Interested in training?';
$string['att_location'] = '2. Where would you like to attend your selected courses? (You can select more than one preference.)';
$string['att_location_short'] = 'Attendance location';
$string['source'] = '4. How did you hear about the Energy Saver training program?';
$string['source_other'] = '';
$string['source_short'] = 'Source';
$string['search_helptext'] = 'Click the course name to book a course now.<br>
To be notified when new dates become available, select the course(s) and submit your Register of Interest below.';
$string['clickheretounsubscribe'] = 'Click here to unsubscribe';
$string['unsubscribe_success'] = 'You have been unsubscribed from the notifications for this course.';
$string['unsubscribe_error'] = 'An error occurred while trying to unsubscribe you from the notifications for this course. Please try again later.';
$string['gotoeoi'] = 'Register my interest';

//location items
$string['location_search'] = 'Locations';
$string['location_plural'] = 'Locations';
$string['location_new'] = 'New Location';
$string['location_edit'] = 'Edit Location';
$string['competency'] = 'Competency';
$string['catalogue_entry_search_instructions'] = 'Search by Name or Description';
$string['location_search_instructions'] = 'Search by Name';
$string['missingorganisation'] = 'You have to specify the organisation.';
$string['missingcity'] = 'You have to specify the city/town.';
$string['missingjobtitle'] = 'You have to specify the job title.';
$string['missingindustry'] = 'You have to specify the industry sector.';
$string['missingatt_location'] = 'You have to select the location(s).';
$string['missingatt_location_other'] = 'You have to specify the &quot;Other&quot; location.';
$string['missingsource'] = 'Please tell us how did you hear about the training program.';
$string['nocompsfound'] = 'No competencies found.';
$string['pleasespecify'] = 'Please specify...';
$string['custom_training'] = 'Custom training';
$string['preferred_location'] = 'Preferred location';
$string['source'] = 'Source';
