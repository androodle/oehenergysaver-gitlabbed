<?php

/** 
 * Androgogic Catalogue Block: Search form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     15/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides search form for the object.
 * This is used by search page
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
	die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->libdir . '/formslib.php');

class eoi_confirm_form extends moodleform {
	
	function definition() {
		
		global $USER, $PAGE, $DB, $OUTPUT;
		
		$PAGE->requires->js('/blocks/androgogic_catalogue/lib.js');
		$PAGE->requires->js_init_call('M.block_androgogic_catalogue.init_eoi');
		
		$mform =& $this->_form;
		foreach($this->_customdata as $custom_key=>$custom_value){
			$$custom_key = $custom_value;
		}
        
        $available_courses = catalogue_eoi_available_courses();
        if (empty($available_courses)) {
            // Nothing to offer!
            return;
        }
        
		$mform->addElement('html', '<div>');
        
        foreach ($available_courses as $course) {
            $courses_inputs[$course->id] =& $mform->createElement(
                'advcheckbox', 
                'courses['.$course->id.']', 
                '', 
                $course->fullname, 
                array('group' => 1), 
                array(false, $course->id)
            );
            if (!empty($courses) && array_search($course->id, $courses)) {
                $courses_inputs[$course->id]->setChecked(true);
            }
        }
		$mform->addGroup(
			$courses_inputs, 
			'courses_group', 
			get_string('courses') . '<img class="req" title="'.get_string('requiredelement', 'form').'" alt="'.get_string('requiredelement', 'form').'" src="'.$OUTPUT->pix_url('req') .'" />', 
			array('<br />'), 
			false
		);
		
		$mform->addElement('hidden', 'tab', 'eoi_confirm');
		$mform->setType('tab', PARAM_RAW);
		
		if (empty($firstname) && !empty($USER->firstname)) {
			$firstname = $USER->firstname;
		} 
		$mform->addElement('text', 'firstname', get_string('firstname'));
		$mform->setDefault('firstname', $firstname);
		$mform->setType('firstname', PARAM_RAW);
		$mform->addRule('firstname', get_string('missingfirstname'), 'required', null, 'server');

		if (!empty($USER->lastname)) {
			$lastname = $USER->lastname;
		}
		$mform->addElement('text', 'lastname', get_string('lastname'));
		$mform->setDefault('lastname', $lastname);
		$mform->setType('lastname', PARAM_RAW);
		$mform->addRule('lastname', get_string('missinglastname'), 'required', null, 'server');

		if (!empty($USER->email)) {
			$email = $USER->email;
		}
		$mform->addElement('text', 'email', get_string('email'));
		$mform->setDefault('email', $email);
		$mform->setType('email', PARAM_RAW);
		$mform->addRule('email', get_string('missingemail'), 'required', null, 'server');
		$mform->addRule('email', get_string('emailinvalid', 'block_androgogic_catalogue'), 'email', null, 'server');
		
		if (empty($organisation) && !empty($USER->profile['organisation'])) {
			$organisation = $USER->profile['organisation'];
		}
		$mform->addElement('text', 'organisation', get_string('organisation', 'block_androgogic_catalogue'));
		$mform->setDefault('organisation', $organisation);
		$mform->setType('organisation', PARAM_RAW);
		$mform->addRule('organisation', get_string('missingorganisation', 'block_androgogic_catalogue'), 'required', null, 'server');

		if (empty($position) && !empty($USER->profile['jobtitle'])) {
			$position = $USER->profile['jobtitle'];
		}
		$mform->addElement('text', 'position', get_string('jobtitle', 'block_androgogic_catalogue'));
		$mform->setDefault('position', $position);
		$mform->setType('position', PARAM_RAW);
		$mform->addRule('position', get_string('missingjobtitle', 'block_androgogic_catalogue'), 'required', null, 'server');
		
		if (empty($industry) && !empty($USER->profile['industrysector'])) {
			$industry = $USER->profile['industrysector'];
		}
		
		// Get Industry options (user profile custom field)
		// TODO: separate function for this
		$industry_field = $DB->get_record('user_info_field', array('shortname' => 'industrysector'), 'param1');
		if ($industry_field) {
			// Display choices if the custom field is found.
			$industry_field->param1 = str_replace("\r", '', $industry_field->param1);
			$industry_options = explode("\n", $industry_field->param1);
			// Add an empty value at the beginning.
			array_unshift($industry_options, '');
			// Assign array values to its keys.
			$industry_options = array_combine($industry_options, $industry_options);
			$mform->addElement('select', 'industry', get_string('industry', 'block_androgogic_catalogue'), $industry_options);
			$mform->getElement('industry')->setSelected($industry);
		} else {
			// Display text field.
			$mform->addElement('text', 'industry', get_string('industry', 'block_androgogic_catalogue'));
			$mform->setDefault('industry', $industry);
		}
		$mform->setType('industry', PARAM_RAW);
		$mform->addRule('industry', get_string('missingindustry', 'block_androgogic_catalogue'), 'required', null, 'server');
		
		if (!empty($USER->city)) {
			$city = $USER->city;
		} 
		$mform->addElement('text', 'city', get_string('city'));
		$mform->setDefault('city', $city);
		$mform->setType('city', PARAM_RAW);
		$mform->addRule('city', get_string('missingcity', 'block_androgogic_catalogue'), 'required', null, 'server');

		$wants_training_options = array(
			0 => 'No',
			1 => 'Yes',
		);
		$wt = $mform->addElement('select', 'wants_training', get_string('wants_training', 'block_androgogic_catalogue'), $wants_training_options);
		$mform->setType('wants_training', PARAM_RAW);
		$wt->setSelected('wants_training', $wants_training);
		
		$att_location_inputs = array();
		$att_location_options = array(
			'Sydney' => 'Sydney',
			'Mid-North coast' => 'Mid-North coast',
			'Far-North coast' => 'Far-North coast',
			'Riverina' => 'Riverina',
			'Central Tablelands' => 'Central Tablelands',
			'Hunter' => 'Hunter',
			'Illawarra' => 'Illawarra',
			'Other' => 'Other',
		);
		$att_location_inputs[0] =& $mform->createElement(
			'select', 
			'att_location', 
			get_string('att_location', 'block_androgogic_catalogue'), 
			$att_location_options,
			array('size' => count($att_location_options))
		);
		$att_location_inputs[0]->setSelected('att_location', $att_location);
		$att_location_inputs[0]->setMultiple('att_location', true);
		$att_location_inputs[1] =& $mform->createElement(
			'text', 
			'att_location_other', 
			null,
			array('placeholder' => get_string('pleasespecify', 'block_androgogic_catalogue'))
		);
		$mform->addGroup(
			$att_location_inputs, 
			'att_location_inputs', 
			get_string('att_location', 'block_androgogic_catalogue'), 
			array('<br />'), 
			false
		);
		$mform->setType('att_location', PARAM_RAW);
		$mform->setType('att_location_other', PARAM_RAW);
		
		$mform->addElement('textarea', 'notes', get_string('eoi_notes', 'block_androgogic_catalogue'), 'rows="6" ', $notes);
		$mform->setType('notes', PARAM_RAW);
		
		$source_inputs = array();
		$source_options = array(
			'Office of Environment and Heritage (OEH) website' => 'Office of Environment and Heritage (OEH) website',
			'Other Energy Saver training course' => 'Other Energy Saver training course',
			'Energy Saver eBulletin' => 'Energy Saver eBulletin',
			'OEH event' => 'OEH event',
			'Email' => 'Email',
			'Other website' => 'Other website',
			'Google/Search engine' => 'Google/Search engine',
			'Referral/Friend recommended' => 'Referral/Friend recommended',
			'LinkedIn' => 'LinkedIn',
			'Other' => 'Other',
		);
		$source_inputs[0] =& $mform->createElement(
			'select', 
			'source', 
			get_string('source', 'block_androgogic_catalogue'), 
			$source_options
		);
		$source_inputs[1] =& $mform->createElement(
			'text', 
			'source_other', 
			get_string('source_other', 'block_androgogic_catalogue'),
			array('placeholder' => get_string('pleasespecify', 'block_androgogic_catalogue'))
		);
		
		$mform->addGroup(
			$source_inputs, 
			'source_inputs', 
			get_string('source', 'block_androgogic_catalogue'), 
			array(' '), 
			false
		);
		$mform->setType('source', PARAM_RAW);
		$source_inputs[0]->setSelected('source', $source);
		$mform->setDefault('source_other', $source_other);
		$mform->setType('source_other', PARAM_RAW);
		
		$mform->addElement('submit', 'submit', get_string('eoi_finalise', 'block_androgogic_catalogue'));
		
		$mform->addElement('html', '</div>');
	}
    
    function validation($data, $files = null) {
		
		$errors = parent::validation($data, $files);
        
        // Clean up 'courses'; empty values not needed.
        $courses = $data['courses'];
        foreach ($data['courses'] as $key => $value) {
            if (empty($value)) {
                unset($courses[$key]);
            }
        }
        $data['courses'] = $courses;
        
        if (empty($data['courses'])) {
            $errors['courses_group'] = get_string('eoi_nothing_chosen', 'block_androgogic_catalogue');
        }
		
		if (is_array($data['att_location']) && in_array('Other', $data['att_location']) && empty($data['att_location_other'])) {
			$errors['att_location_inputs'] = get_string('missingatt_location_other', 'block_androgogic_catalogue');
		}
		
		if ($data['source'] == 'Other' && empty($data['source_other'])) {
			$errors['source_inputs'] = get_string('missingsource', 'block_androgogic_catalogue');
		}
		
		if (empty($data['industry'])) {
			$errors['industry'] = get_string('missingindustry', 'block_androgogic_catalogue');
		}
		
		return $errors;
	}
	
}
