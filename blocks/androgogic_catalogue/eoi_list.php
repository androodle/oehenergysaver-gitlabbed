<?php

require_capability('block/androgogic_catalogue:read_eoi', $context);

$perpage = 20;
$can_edit = has_capability('block/androgogic_catalogue:edit_eoi', $context);

require_once 'eoi_search_form.php';
$sform = new eoi_search_form();

$table = new flexible_table('eoi-list');
$table->set_attribute('class', 'eoi-list');
$table->define_baseurl($PAGE->url);
$table->sortable(true, 'added', SORT_DESC);
$table->define_columns(array(
    '',
    'added',
    'firstname',
    'lastname',
    'organisation',
    'courses',
    'wants_training',
    'comments',
    'email',
));
$table->define_headers(array(
    '',
    get_string('date'),
    get_string('firstname'),
    get_string('lastname'),
    get_string('organisation', 'block_androgogic_catalogue'),
    get_string('courses'),
    get_string('custom_training', 'block_androgogic_catalogue'),
    get_string('comments'),
    get_string('email'),
));
$table->no_sorting('comments');
$table->no_sorting('courses');
$table->setup();

$params = array();
$where = array();
$sql = "SELECT eoi.* FROM {androgogic_catalogue_eoi} eoi";
list($twhere, $tparams) = $table->get_sql_where();
if (!empty($twhere)) {
    $where[] = $twhere;
}

$formdata = $sform->get_data();
if (!empty($formdata)) {
    if ($formdata->deleted > -1) {
        $where[] = ' eoi.deleted = :deleted ';
        $params['deleted'] = $formdata->deleted;
    }
    if (!empty($formdata->name)) {
        $where[] .= ' '.$DB->sql_like(
            $DB->sql_concat('eoi.firstname', 'eoi.lastname'), 
            ':name',
            false,
            false,
            false,
            '%'
        ).' ';
        $params['name'] = '%'.$formdata->name.'%';
    }
}

if (!empty($where)) {
    $sql .= ' WHERE '.implode(' AND ', $where);
    $params = array_merge($params, $tparams);
}
if ($table->get_sql_sort()) {
    $sql .= ' ORDER BY '.$table->get_sql_sort();
}

$countsql = "SELECT COUNT(id) FROM {androgogic_catalogue_eoi} eoi";
$totalcount = $DB->count_records_sql($countsql, $params);
if (!empty($where)) {
    $matchcount = $DB->count_records_sql($countsql.' WHERE '.implode(' AND ', $where), $params);
} else {
    $matchcount = $totalcount;
}

$table->initialbars($totalcount > $perpage);
$table->pagesize($perpage, $matchcount);

$sform->display();

$eois = $DB->get_records_sql($sql, $params, $table->get_page_start(), $table->get_page_size());
if (!$eois) {
    $eois = array();
}
	
$params = array('csv' => 1, 'tsort' => $tsort);
$csvurl = new moodle_url('/blocks/androgogic_catalogue/index.php', $params);
echo '<p><a href="'. $csvurl .'">'. get_string('downloadcsv', 'block_androgogic_catalogue') .'</a></p>';

foreach ($eois as $eoi) {

    $row = array();

    if ($can_edit) {
        $params = array('tab' => 'eoi_list');
        if ($eoi->deleted) {
            $params['eoi_restore'] = $eoi->id;
            $linklabel = html_writer::empty_tag('img', array(
                'src' => $OUTPUT->pix_url('t/recycle'), 
                'alt' => get_string('restore'), 
                'title' => get_string('restore'), 
                'class'=>'iconsmall'
            ));
        } else {
            $params['eoi_del'] = $eoi->id;
            $linklabel = html_writer::empty_tag('img', array(
                'src' => $OUTPUT->pix_url('t/delete'), 
                'alt' => get_string('delete'), 
                'title' => get_string('delete'), 
                'class'=>'iconsmall'
            ));
        }
        $linkurl = new moodle_url('/blocks/androgogic_catalogue/index.php', $params);
        $delhref = '<a href="'. $linkurl .'" onclick="javascript:return confirm(\''. get_string('areyousure') .'\')">'. $linklabel .'</a>';
        $row[] = $delhref;
    } else {
        $row[] = '';
    }
    
    $courses = catalogue_eoi_load_courses($eoi->id);
    $courses_html = '';
    if (!empty($courses)) {
        $courses_html .= '<ul>';
        foreach ($courses as $course) {
            $params = array('id' => $course->id);
            $courseurl = new moodle_url('/course/view.php', $params);
            $courses_html .= '<li><a href="'. $courseurl .'">'. $course->shortname .'</a></li>';
        }
        $courses_html .= '</ul>';
    }

    $row[] = date('d/m/Y H:i:s', $eoi->added);
    $row[] = $eoi->firstname;
    $row[] = $eoi->lastname;
    $row[] = $eoi->organisation;
    $row[] = $courses_html;
    $row[] = (!empty($eoi->wants_training) ? 'Yes' : 'No');
    $row[] = $eoi->notes;
    $row[] = $eoi->email;

    $classname = ($eoi->deleted) ? 'deleted' : '';
    $table->add_data($row, $classname);
}

//echo html_writer::table($table);
$table->print_html();
