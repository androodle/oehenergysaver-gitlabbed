<?php

/** 
 * Androgogic Catalogue Block: Delete object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Delete one of the locations
 *
 **/

$id = required_param('id', PARAM_INT);
$DB->delete_records('androgogic_catalogue_locations',array('id'=>$id));
echo $OUTPUT->notification(get_string('itemdeleted','block_androgogic_catalogue'), 'notifysuccess');

?>
