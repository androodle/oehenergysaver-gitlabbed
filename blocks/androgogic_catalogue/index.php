<?php
/** 
 * Androgogic Catalogue Block: Index
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

require_once('../../config.php');
require_once('lib.php');
global $CFG,$USER,$DB,$PAGE, $OUTPUT;

$context = context_system::instance();

$currenttab = optional_param('tab', 'catalogue_entry_search', PARAM_TEXT);
$csv = optional_param('csv', null, PARAM_INT);
$eoi_del = optional_param('eoi_del', null, PARAM_INT);
$eoi_restore = optional_param('eoi_restore', null, PARAM_INT);
$eoi_unsubscribe = optional_param('eoi_unsubscribe', null, PARAM_TEXT);
$tsort = optional_param('tsort', null, PARAM_TEXT);

$page_url = new moodle_url("/blocks/androgogic_catalogue/index.php", array('tab'=>$currenttab));

if (!empty($csv)) {
	// Script dies inside the function.
	catalogue_eoi_export_csv($tsort);
}

if (!empty($eoi_del)) {
    require_capability('block/androgogic_catalogue:edit_eoi', $context);
	catalogue_eoi_delete($eoi_del);
	redirect($page_url);
}

if (!empty($eoi_restore)) {
    require_capability('block/androgogic_catalogue:edit_eoi', $context);
	catalogue_eoi_restore($eoi_restore);
	redirect($page_url);
}

if (!empty($eoi_unsubscribe)) {
	echo $OUTPUT->header();
	if (catalogue_eoi_unsubscribe($eoi_unsubscribe)) {
        notice(
            get_string('unsubscribe_success', 'block_androgogic_catalogue'),
            $page_url
        );
    } else {
        notice(
            get_string('unsubscribe_error', 'block_androgogic_catalogue'),
            $page_url
        );
    }
    echo $OUTPUT->footer();
    die;
}

$PAGE->set_context($context);
$PAGE->set_pagelayout($CFG->catalogue_page_layout);
$PAGE->set_url($page_url);
$PAGE->set_title(get_string($currenttab, 'block_androgogic_catalogue'));
$PAGE->set_heading(get_string('plugintitle', 'block_androgogic_catalogue'));
$PAGE->navbar->add(
	get_string('catalogue_entry_search','block_androgogic_catalogue'), 
	new moodle_url('/blocks/androgogic_catalogue/index.php')
);
if (!in_array($currenttab, array('catalogue_entry_search', 'eoi_confirm'))) {
    require_login();
}
echo $OUTPUT->header();
//show tabs
include('tabs.php');
// include current page
include $currenttab.'.php';
echo $OUTPUT->footer();

// End of blocks/androgogic_catalogue/index.php