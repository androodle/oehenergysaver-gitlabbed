<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    report
 * @subpackage f2fstr
 * @copyright  2015 onwards Androgogic Pty Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['cancellationdate'] = 'Cancellation date';
$string['cancellationreason'] = 'Cancellation reason';
$string['coursename'] = 'Course name';
$string['facetoface_report'] = 'Face-to-face Report';
$string['f2fstr:view'] = 'View Face-to-face Sessions Trainer\'s Report';
$string['jobtitle'] = 'Job title';
$string['industrysector'] = 'Industry sector';
$string['nothingfound'] = 'Nothing found.';
$string['organisation'] = 'Organisation';
$string['pluginname'] = 'Face-to-face Sessions Trainer\'s Report';
$string['postcode'] = 'Postcode';
$string['sessionstartdate'] = 'Session start date';
$string['sessionstartdate_fromto'] = 'Session start date (from/to)';
$string['status'] = 'Status';
$string['useraddress'] = 'User\'s address';
$string['useremail'] = 'User\'s email';
$string['userfullname'] = 'User\'s fullname';
$string['userphone'] = 'User\'s phone number';
