<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    report
 * @subpackage f2fstr
 * @copyright  2015 onwards Androgogic Pty Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_f2fstr;

class report {
    
    const PAGE_LIMIT = 20;
    
    private $userid;
    private $data;
    private $config;
    
    public function __construct($userid, $data) 
    {
        $this->userid = $userid;
        $this->data = $data;
        $this->config = get_config('report_f2fstr');
    }
    
    public function display($pageurl)
    {
        global $CFG, $OUTPUT;
        
        $count = $this->count();
        if (!$count) {
            echo $OUTPUT->notification(get_string('nothingfound', 'report_f2fstr'));
            echo $OUTPUT->footer();
            die;
        }
        
        $table = new \report_f2fstr\report_viewer($pageurl);
        $table->pagesize(self::PAGE_LIMIT, $count);
        
        try {
            $results = $this->results($table);
        } catch (dml_read_exception $e) {
            $debuginfo = $CFG->debugdeveloper ? $e->debuginfo : '';
            print_error('error:problemobtainingreportdata', 'totara_reportbuilder', '', $debuginfo);
        }
        
        $table->set_data($results);
        $table->display();
        
        // Free the resources from $DB->get_recordset_sql(). Must stay.
        $results->close();
    }
    
    public function count()
    {
        global $DB;

        list($where, $params) = $this->get_where_and_params();

        $sql = "SELECT 
                COUNT(base.id)
            FROM {facetoface_signups} base 
            LEFT JOIN {facetoface_sessions} sessions ON sessions.id = base.sessionid 
            LEFT JOIN {user} auser ON auser.id = base.userid 
            LEFT JOIN {facetoface_signups_status} status ON status.signupid = base.id 
                AND status.superceded = 0
            LEFT JOIN {facetoface_signups_status} cancellationstatus ON cancellationstatus.signupid = base.id 
                AND cancellationstatus.superceded = 0 
                AND cancellationstatus.statuscode = 10 
            LEFT JOIN {facetoface} facetoface ON facetoface.id = sessions.facetoface 
            LEFT JOIN {user_info_data} user_2 ON user_2.userid = auser.id AND user_2.fieldid = 2 
            LEFT JOIN {user_info_data} user_3 ON user_3.userid = auser.id AND user_3.fieldid = 3 
            LEFT JOIN {facetoface_sessions_dates} sessiondate ON (sessiondate.sessionid = base.sessionid AND sessions.datetimeknown = 1) 
            LEFT JOIN {user_info_data} user_6 ON user_6.userid = auser.id AND user_6.fieldid = 6 
            LEFT JOIN {user_info_data} user_5 ON user_5.userid = auser.id AND user_5.fieldid = 5 
            LEFT JOIN {course} course ON course.id = facetoface.course ";
        if (!empty($where)) {
            $sql .= ' WHERE '.implode(' AND ', $where);
        }

        return $DB->count_records_sql($sql, $params);
    }
    
    public function results(&$viewer = null)
    {
        global $DB;

        list($where, $params) = $this->get_where_and_params();
        
        $sql = "SELECT 
                base.id, 
                course.fullname AS coursename, 
                course.id AS courseid, 
                course.visible AS course_visible, 
                course.audiencevisible AS course_audiencevisible, 
                ".$DB->sql_concat_join("' '", array('auser.firstname', 'auser.lastname'))." AS user_fullname, 
                auser.id AS user_id, 
                user_2.data AS user_organisation, 
                user_3.data AS user_jobtitle, 
                sessiondate.timestart AS date_sessiondate, 
                base.sessionid AS session_id, 
                sessiondate.sessiontimezone AS timezone, 
                status.statuscode AS status_statuscode, 
                cancellationstatus.timecreated AS session_cancellationdate, 
                auser.email AS user_email, 
                auser.phone1 AS user_phone, 
                auser.address AS user_address, 
                user_6.data AS user_postcode, 
                user_5.data AS user_industrysector, 
                cancellationstatus.note AS session_cancellationreason 
            FROM {facetoface_signups} base 
            LEFT JOIN {facetoface_sessions} sessions ON sessions.id = base.sessionid 
            LEFT JOIN {user} auser ON auser.id = base.userid 
            LEFT JOIN {facetoface_signups_status} status ON status.signupid = base.id 
                AND status.superceded = 0
            LEFT JOIN {facetoface_signups_status} cancellationstatus ON cancellationstatus.signupid = base.id 
                AND cancellationstatus.superceded = 0 
                AND cancellationstatus.statuscode = 10 
            LEFT JOIN {facetoface} facetoface ON facetoface.id = sessions.facetoface 
            LEFT JOIN {user_info_data} user_2 ON user_2.userid = auser.id AND user_2.fieldid = 2 
            LEFT JOIN {user_info_data} user_3 ON user_3.userid = auser.id AND user_3.fieldid = 3 
            LEFT JOIN {facetoface_sessions_dates} sessiondate ON (sessiondate.sessionid = base.sessionid AND sessions.datetimeknown = 1) 
            LEFT JOIN {user_info_data} user_6 ON user_6.userid = auser.id AND user_6.fieldid = 6 
            LEFT JOIN {user_info_data} user_5 ON user_5.userid = auser.id AND user_5.fieldid = 5 
            LEFT JOIN {course} course ON course.id = facetoface.course ";
        if (!empty($where)) {
            $sql .= ' WHERE '.implode(' AND ', $where);
        }

        if (!empty($viewer)) {
            $sql .= ' ORDER BY '.$viewer->get_sql_sort();
        }
        
        if (!empty($viewer)) {
            return $DB->get_recordset_sql($sql, $params, $viewer->get_page_start(), $viewer->get_page_size());
        }

        return $DB->get_recordset_sql($sql, $params);
    }
    
    private function find_available_courses()
    {
        global $DB;
        
        $sql = "SELECT 
                DISTINCT ctx.instanceid AS courseid
            FROM {role_assignments} ra
            LEFT JOIN {context} ctx ON ctx.id = ra.contextid
                AND ctx.contextlevel = 50
            WHERE ra.userid = :userid
                AND ra.roleid = :roleid";
        $params = array(
            'userid' => $this->userid,
            'roleid' => 4, // TODO move to Settings
        );
        
        $results = $DB->get_records_sql($sql, $params);
        if (empty($results)) {
            return false;
        }
        
        $courses = array();
        foreach ($results as $result) {
            $courses[] = $result->courseid;
        }
        
        return $courses;
    }
    
    private function user_can_view_all()
    {
        return has_capability('moodle/user:create', \context_system::instance(), $this->userid);
    }
    
    private function get_where_and_params()
    {
        global $DB;
        
        $params = array();
        $where = array();
        
        // Find courses where this user has an eligible role.
        // If the user is Site Admin or similar, just fetch them all.
        if (!$this->user_can_view_all()) {
            // If not, find out which courses should be available.
            // If none, show nothing to user.
            $courses = $this->find_available_courses();
            if (empty($courses)) {
                return array(array(' 1 = 0 '), array());
            }
            list ($tsql, $tparams) = $DB->get_in_or_equal($courses, SQL_PARAMS_NAMED, 'courseid');
            $where[] = "course.id ".$tsql;
            foreach ($tparams as $tpk => $tpv) {
                $params[$tpk] = $tpv;
            }
        }
        
        if (!empty($this->data->userfullname)) {
            $where[] = $DB->sql_like($DB->sql_concat_join("' '", array('auser.firstname', 'auser.lastname')), ':userfullname', false, false);
            $params['userfullname'] = '%'.$this->data->userfullname.'%';
        }
        
        if (!empty($this->data->coursename)) {
            $where[] = $DB->sql_like('course.fullname', ':coursename', false, false);
            $params['coursename'] = '%'.$this->data->coursename.'%';
        }
    
        if (!empty($this->data->sessiondate_start) && !empty($this->data->sessiondate_end)) {
            $where[] = " sessiondate.timestart BETWEEN :sessiondatestart AND :sessiondateend";
            $params['sessiondatestart'] = $this->data->sessiondate_start;
            $params['sessiondateend'] = $this->data->sessiondate_end;
        } elseif (!empty($this->data->sessiondate_start) && empty($this->data->sessiondate_end)) {
            $where[] = " sessiondate.timestart >= :sessiondatestart";
            $params['sessiondatestart'] = $this->data->sessiondate_start;
        } elseif (empty($this->data->sessiondate_start) && !empty($this->data->sessiondate_end)) {
            $where[] = " sessiondate.timestart <= :sessiondateend";
            $params['sessiondateend'] = $this->data->sessiondate_end;
        }
        
        if (!empty($this->data->status)) {
            $where[] = "status.statuscode = :status";
            $params['status'] = $this->data->status;
        }

        return array($where, $params);
    }
    
}
