<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    report
 * @subpackage f2fstr
 * @copyright  2015 onwards Androgogic Pty Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_f2fstr;

class report_viewer {
    
    private $data;
    private $pageurl;
    private $table;
    
    public function __construct($pageurl) 
    {
        $this->pageurl = $pageurl;
        $this->init_table();
    }
    
    public function set_data(&$data)
    {
        $this->data = $data;
    }
    
    public function pagesize($perpage, $total)
    {
        if (empty($this->table)) {
            return false;
        }
        return $this->table->pagesize($perpage, $total);
    }
    
    public function get_page_start()
    {
        if (empty($this->table)) {
            return false;
        }
        return $this->table->get_page_start();
    }
    
    public function get_page_size()
    {
        if (empty($this->table)) {
            return false;
        }
        return $this->table->get_page_size();
    }
    
    public function get_sql_sort()
    {
        if (empty($this->table)) {
            return false;
        }
        return $this->table->get_sql_sort();
    }
    
    public function display() 
    {
        if (!$this->data->valid()) {
            return;
        }
        
        foreach ($this->data as $result) {
            $this->table->add_data($this->build_row($result));
        }
        
        $this->table->print_html();
    }
    
    private function display_facetoface_status($status) 
    {
        global $MDL_F2F_STATUS;

        // if status doesn't exist just return the status code
        if (!isset($MDL_F2F_STATUS[$status])) {
            return $status;
        }
        // otherwise return the string
        return get_string('status_'.facetoface_get_status($status),'facetoface');
    }
    
    private function build_row(&$result)
    {
        $row = array();

        $courseurl = new \moodle_url('/course/view.php', array('id' => $result->courseid));
        $row[] = '<a href="'.$courseurl.'" target="_blank">'.$result->coursename.'</a>';

        $userurl = new \moodle_url('/user/profile.php', array('id' => $result->user_id));
        $row[] = '<a href="'.$userurl.'" target="_blank">'.$result->user_fullname.'</a>';

        $row[] = $result->user_organisation;
        $row[] = $result->user_jobtitle;
        
        $sessionurl = new \moodle_url('/mod/facetoface/attendees.php', array('s' => $result->session_id));
        if (!empty($result->date_sessiondate)) {
            $row[] = '<a href="'.$sessionurl.'" target="_blank">'.date('d F Y', $result->date_sessiondate).'</a>';
        } else {
            $row[] = '';
        }
        
        $row[] = $this->display_facetoface_status($result->status_statuscode);
        $row[] = (!empty($result->session_cancellationdate)) ? date('d F Y', $result->session_cancellationdate) : '';
        $row[] = '<a href="mailto:'.$result->user_email.'">'.$result->user_email.'</a>';
        $row[] = $result->user_phone;
        $row[] = $result->user_address;
        $row[] = $result->user_postcode;
        $row[] = $result->user_industrysector;
        $row[] = $result->session_cancellationreason;

        return $row;
    }
    
    private function init_table() 
    {
        $table = new \flexible_table('f2fstr-list');
        $table->set_attribute('class', 'f2fstr-list');
        $table->define_baseurl($this->pageurl);
        $table->sortable(true, 'coursename', SORT_ASC);
        $table->define_columns(array(
            'coursename',
            'user_fullname',
            'user_organisation',
            'user_jobtitle',
            'date_sessiondate',
            'status_statuscode',
            'session_cancellationdate',
            'user_email',
            'user_phone',
            'user_address',
            'user_postcode',
            'user_industrysector',
            'session_cancellationreason',
        ));
        $table->define_headers(array(
            get_string('coursename', 'report_f2fstr'),
            get_string('userfullname', 'report_f2fstr'),
            get_string('organisation', 'report_f2fstr'),
            get_string('jobtitle', 'report_f2fstr'),
            get_string('sessionstartdate', 'report_f2fstr'),
            get_string('status', 'report_f2fstr'),
            get_string('cancellationdate', 'report_f2fstr'),
            get_string('useremail', 'report_f2fstr'),
            get_string('useraddress', 'report_f2fstr'),
            get_string('userphone', 'report_f2fstr'),
            get_string('postcode', 'report_f2fstr'),
            get_string('industrysector', 'report_f2fstr'),
            get_string('cancellationreason', 'report_f2fstr'),
        ));
        $table->setup();
        $this->table = $table;
    }
    
}
