<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    report
 * @subpackage f2fstr
 * @copyright  2015 onwards Androgogic Pty Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    // It must be included from a Moodle page.
}

require_once($CFG->dirroot.'/lib/formslib.php');

class f2fstr_search_form extends moodleform 
{
    public function definition() 
    {
        global $MDL_F2F_STATUS;

        $mform =& $this->_form;
        
        $mform->addElement('text', 'userfullname', get_string('userfullname', 'report_f2fstr'));
        $mform->setType('userfullname', PARAM_TEXT);
        
        $mform->addElement('text', 'coursename', get_string('coursename', 'report_f2fstr'));
        $mform->setType('coursename', PARAM_TEXT);
        
        $mform->addElement('date_selector', 'sessiondate_start', get_string('sessionstartdate_fromto', 'report_f2fstr'), array('optional' => true));
        $mform->setType('sessiondate_start', PARAM_INT);
        
        $mform->addElement('date_selector', 'sessiondate_end', '', array('optional' => true));
        $mform->setType('sessiondate_end', PARAM_INT);
        
        $status_values = array(0 => '');
        foreach ($MDL_F2F_STATUS as $k => $v) {
            $status_values[$k] = get_string('status_'.$v,'facetoface');
        }
        asort($status_values);
        $mform->addElement('select', 'status', get_string('status'), $status_values);
        $mform->setType('status', PARAM_INT);
        
        $this->add_action_buttons();
    }
    
    function add_action_buttons($cancel = true, $submitlabel=null)
    {
        $mform =& $this->_form;
        $this_url = new moodle_url('/report/f2fstr/');
        
        $buttonarray=array();
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('search'));
        $buttonarray[] = &$mform->createElement('button', 'resetbutton', get_string('clear'), array(
            'onclick' => 'javascript:location.href=\''.$this_url.'\';'
        ));
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');
    }
   
}