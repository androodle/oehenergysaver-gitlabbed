<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    report
 * @subpackage f2fstr
 * @copyright  2015 onwards Androgogic Pty Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once($CFG->dirroot . '/mod/facetoface/lib.php');
require_once($CFG->dirroot . '/report/f2fstr/classes/report.php');
require_once($CFG->dirroot . '/report/f2fstr/search_form.php');

require_login();

$context = context_system::instance();
$pageurl = new moodle_url('/report/f2fstr/index.php');

$PAGE->set_context($context);
$PAGE->set_url($pageurl);
$PAGE->set_heading($SITE->fullname);
$PAGE->set_title(get_string('pluginname', 'report_f2fstr'));
$PAGE->navbar->add(get_string('pluginname', 'report_f2fstr'));

require_capability('report/f2fstr:view', $context);

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('pluginname', 'report_f2fstr'), 3, 'main');

$mform = new f2fstr_search_form();
$mform->display();

$data = $mform->get_data();
$report = new \report_f2fstr\report($USER->id, $data);

$report->display($pageurl);

echo $OUTPUT->footer();
