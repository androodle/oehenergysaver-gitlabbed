<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Add labeleoi form
 *
 * @package    mod
 * @subpackage labeleoi
 * @copyright  2006 Jamie Pratt
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once ($CFG->dirroot.'/course/moodleform_mod.php');

class mod_labeleoi_mod_form extends moodleform_mod {

    function definition() {

        $mform =& $this->_form;
		
        $mform->addElement('header', 'generalhdr', get_string('general'));
        $this->add_intro_editor(true, get_string('labeleoitext', 'labeleoi'));
		
        $mform->addElement('checkbox', 'emailsend', get_string('emailsend', 'labeleoi'));
		$mform->addHelpButton('emailsend', 'emailsend', 'labeleoi');
        $mform->setType('emailsend', PARAM_RAW);
		
        $mform->addElement('text', 'emailsubject', get_string('emailsubject', 'labeleoi'), 'size="80"');
        $mform->setType('emailsubject', PARAM_TEXT);
		
        $mform->addElement('editor', 'emailtext', get_string('emailtext', 'labeleoi'), null, 
			$this->labeleoi_emailtext_options());
        $mform->setType('emailtext', PARAM_RAW);

        $this->standard_coursemodule_elements();

        $this->add_action_buttons(true, false, null);

    }
	
	function validation($data, $files) {
		$errors = parent::validation($data, $files);
		
		$data['emailsubject'] = trim($data['emailsubject']);
		// If "send emails" is checked, email subject or text cannot be empty.
		if (!empty($data['emailsend']) && (empty($data['emailsubject']) || empty($data['emailtext']['text']))) {
			$errors['emailsend'] = get_string('emailsend_subjtextempty', 'labeleoi');
		}
		
		return $errors;
	}

    function data_preprocessing(&$default_values) {
		
		parent::data_preprocessing($default_values);
		
		$emailtext = $default_values['emailtext'];
		$default_values['emailtext'] = array(
			'format' => FORMAT_HTML,
			'text' => $emailtext,
		);

	}
	
	function labeleoi_emailtext_options() {
		return array(
			'component' => 'mod_labeleoi',
			'context' => $this->context,
			'subdirs' => 1, 
			'maxfiles'=> -1, 
			'changeformat' => 1,
			'noclean' => 1, 
			'trusttext' => 0
		);
	}
	
}
