function set_item_focus(itemid) {
    var item = document.getElementById(itemid);
    if(item){
        item.focus();
    }
}

function feedback2Go2delete(form) {
    form.action = M.cfg.wwwroot+'/mod/feedback2/delete_completed.php';
    form.submit();
}

function setcourseitemfilter(item, item_typ) {
    document.report.courseitemfilter.value = item;
    document.report.courseitemfiltertyp.value = item_typ;
    document.report.submit();
}


M.mod_feedback2 = {};

M.mod_feedback2.init_sendmessage = function(Y) {
    Y.on('click', function(e) {
        Y.all('input.usercheckbox').each(function() {
            this.set('checked', 'checked');
        });
    }, '#checkall');

    Y.on('click', function(e) {
        Y.all('input.usercheckbox').each(function() {
            this.set('checked', '');
        });
    }, '#checknone');
};

M.mod_feedback2.init = function(Y) {
	
	Y.all('.feedback2_complete_depend').each(function() {
		
		var inputs = this.all('input');
		if (!inputs) {
			return;
		}
		
		var can_hide = true;
		for (var i = 0; i < inputs.size(); i++) {
			var type = inputs.item(i).getAttribute('type');
			if (type == 'radio' || type == 'checkbox') {
				if (inputs.item(i).get('checked')) {
					can_hide = false;
					break;
				}
			} else if (type == 'text') {
				can_hide = !inputs.item(i).getAttribute('value');
			}
		}
		
		if (can_hide) {
			this.hide();
		}
	});
	
	function feedback_inputs_watchdog(obj) {
		var name = obj.getAttribute('name');
		if (name.indexOf('_') === -1) {
			return;
		}
		
		var name_arr = name.split('_');
		var id = parseInt(name_arr[1]);
		
		// Hide the dependable items.
		Y.all('.parent_' + id).hide();
		
		// Look for this ID in parent_ids array. If it is there,
		// then it has dependable item(s).
		var children = [];
		for (key in parent_ids) {
			if (parent_ids[key] == id) {
				children.push(parseInt(key));
			}
		}
		if (!children.length) {
			return;
		}
		
		// Find the selected label and see if it fits.
		var obj_label = obj.ancestor().ancestor().one('label').get('text').trim();
		for (var i = 0; i < children.length; i++) {
			if (obj_label == parent_values[children[i]]) {
				Y.one('.item_' + children[i]).show();
			}
		}
	}
	
	Y.on('click', function() {
		feedback_inputs_watchdog(this);
	}, 'input');
	
	Y.all('input:checked').each(function() {
		feedback_inputs_watchdog(this);
	});
};
