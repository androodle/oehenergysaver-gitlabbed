<?php
// lang file for mod/feedback2/rb_sources/rb_source_feedback2_summary.php
$string['sourcetitle'] = 'Feedback 2.0 Summary';
// columns
$string['timecompleted'] = 'Time Completed';
$string['feedback2activity'] = 'Feedback 2.0 Activity';
$string['trainerid'] = 'Trainer ID';
$string['trainerfullname'] = 'Trainer Full Name';
$string['trainerorgid'] = 'Trainer Organisation ID';
$string['trainerorg'] = 'Trainer Organisation';
$string['trainerposid'] = 'Trainer Position ID';
$string['trainerpos'] = 'Trainer Position';
$string['user'] = 'User';
$string['coursename'] = 'Course Name';
// filters
$string['feedback2name'] = 'Feedback 2.0 Name';
// content
$string['theuser'] = 'The user';
$string['currentpos'] = 'The user\'s current position';
$string['currentorg'] = 'The user\'s current organisation';
$string['course'] = 'The course';
$string['responsetime'] = 'The response time';

// column types for this source, as strings
$string['type_feedback2'] = 'Feedback 2.0';
$string['type_responses'] = 'Responses';
$string['type_trainer'] = 'Trainer';
