<?php
// lang file for mod/feedback2/rb_sources/rb_source_graphical_feedback2_questions.php
$string['sourcetitle'] = 'Feedback 2.0 Graphical Questions';
// columns
$string['timecompleted'] = 'Time Completed';
$string['feedback2activity'] = 'Feedback 2.0 Activity';
$string['ftfsessionid'] = 'Face to face Session ID';
$string['trainerid'] = 'Trainer ID';
$string['trainerfullname'] = 'Trainer Full Name';
$string['trainerorgid'] = 'Trainer Organisation ID';
$string['trainerorg'] = 'Trainer Organisation';
$string['trainerposid'] = 'Trainer Position ID';
$string['trainerpos'] = 'Trainer Position';
$string['numfeedback2responses'] = '# Feedback 2.0 Responses';
$string['percentoption'] = ': % option';
$string['numoption'] = ': # option';
$string['numresponses'] = ': # Responses';
$string['average'] = ' Average';
$string['numanswers'] = ': # answers';
$string['allanswers'] = ': All answers';
$string['sum'] = ': Sum';
$string['min'] = ': Min';
$string['max'] = ': Max';
$string['stddev'] = ': Standard Deviation';

// filters
$string['feedback2name'] = 'Feedback 2.0 Name';
$string['numofresponses'] = 'Number of responses';
$string['timecompleted'] = 'Time completed';


// content
$string['theuser'] = 'The user';
$string['thetrainer'] = 'The trainer';
$string['currentpos'] = 'The user\'s current position';
$string['currentorg'] = 'The user\'s current organisation';
$string['course'] = 'The course';
$string['responsetime'] = 'The response time';

// column types for this source, as strings
$string['type_feedback2'] = 'Feedback 2.0';
$string['type_responses'] = 'Responses';
$string['type_trainer'] = 'Trainer';
