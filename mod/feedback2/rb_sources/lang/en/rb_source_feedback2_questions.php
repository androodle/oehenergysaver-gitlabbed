<?php
// lang file for mod/feedback2/rb_sources/rb_source_feedback2_questions
$string['sourcetitle'] = 'Feedback 2.0 Questions';
// columns
$string['numfeedback2responses'] = '# Feedback 2.0 Responses';
$string['timecompleted'] = 'Time Completed';
$string['feedback2activity'] = 'Feedback 2.0 Activity';
$string['ftfsessionid'] = 'Face to Face Session ID';
$string['trainerid'] = 'Trainer ID';
$string['trainerfullname'] = 'Trainer Full Name';
$string['trainerorgid'] = 'Trainer Organisation ID';
$string['trainerorg'] = 'Trainer Organisation';
$string['trainerposid'] = 'Trainer Position ID';
$string['trainerpos'] = 'Trainer Position';
$string['percentoption'] = ': % option ';
$string['numoption'] = ': # option ';
$string['numresponses'] = ': # Responses';
$string['average'] = ' Average';
$string['numanswers'] = ': # answers';
$string['allanswers'] = ': All answers';
$string['sum'] = ': Sum';
$string['min'] = ': Min';
$string['max'] = ': Max';
$string['stddev'] = ': Standard Deviation';
$string['coursename'] = 'Course Name';
// filters
$string['feedback2name'] = 'Feedback 2.0 Name';
$string['numofresponses'] = 'Number of responses';
$string['timecompleted'] = 'Time completed';
// content
$string['user'] = 'The user';
$string['currentpos'] = 'The user\'s current position';
$string['currentorg'] = 'The user\'s current organisation';
$string['course'] = 'The course';
$string['trainer'] = 'The trainer';
$string['responsetime'] = 'The response time';

// column types for this source, as strings
$string['type_feedback2'] = 'Feedback 2.0';
$string['type_responses'] = 'Responses';
$string['type_trainer'] = 'Trainer';
$string['type_session'] = 'Session';
$string['type_questions'] = 'Questions';
