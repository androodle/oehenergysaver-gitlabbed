<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * print a printview of feedback2-items
 *
 * @author Andreas Grabs
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package feedback2
 */

require_once("../../config.php");
require_once("lib.php");

$id = required_param('id', PARAM_INT);

$PAGE->set_url('/mod/feedback2/print.php', array('id'=>$id));

if (! $cm = get_coursemodule_from_id('feedback2', $id)) {
    print_error('invalidcoursemodule');
}

if (! $course = $DB->get_record("course", array("id"=>$cm->course))) {
    print_error('coursemisconf');
}

if (! $feedback2 = $DB->get_record("feedback2", array("id"=>$cm->instance))) {
    print_error('invalidcoursemodule');
}

$context = context_module::instance($cm->id);

require_login($course, true, $cm);

require_capability('mod/feedback2:view', $context);
$PAGE->set_pagelayout('embedded');

/// Print the page header
$strfeedback2s = get_string("modulenameplural", "feedback2");
$strfeedback2  = get_string("modulename", "feedback2");

$feedback2_url = new moodle_url('/mod/feedback2/index.php', array('id'=>$course->id));
$PAGE->navbar->add($strfeedback2s, $feedback2_url);
$PAGE->navbar->add(format_string($feedback2->name));

$PAGE->set_title($feedback2->name);
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();

/// Print the main part of the page
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
echo $OUTPUT->heading(format_text($feedback2->name));

$feedback2items = $DB->get_records('feedback2_item', array('feedback2'=>$feedback2->id), 'position');
echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');
echo $OUTPUT->continue_button('view.php?id='.$id);
if (is_array($feedback2items)) {
    $itemnr = 0;
    $align = right_to_left() ? 'right' : 'left';

    echo $OUTPUT->box_start('feedback2_items printview');
    //check, if there exists required-elements
    $params = array('feedback2'=>$feedback2->id, 'required'=>1);
    $countreq = $DB->count_records('feedback2_item', $params);
    if ($countreq > 0) {
        echo '<div class="fdescription required">';
        echo get_string('somefieldsrequired', 'form', '<img alt="'.get_string('requiredelement', 'form').
            '" src="'.$OUTPUT->pix_url('req') .'" class="req" />');
        echo '</div>';
    }
    //print the inserted items
    $itempos = 0;
    foreach ($feedback2items as $feedback2item) {
        echo $OUTPUT->box_start('feedback2_item_box_'.$align);
        $itempos++;
        //Items without value only are labels
        if ($feedback2item->hasvalue == 1 AND $feedback2->autonumbering) {
            $itemnr++;
                echo $OUTPUT->box_start('feedback2_item_number_'.$align);
                echo $itemnr;
                echo $OUTPUT->box_end();
        }
        echo $OUTPUT->box_start('box generalbox boxalign_'.$align);
        if ($feedback2item->typ != 'pagebreak') {
            feedback2_print_item_complete($feedback2item, false, false);
        } else {
            echo $OUTPUT->box_start('feedback2_pagebreak');
            echo '<hr class="feedback2_pagebreak" />';
            echo $OUTPUT->box_end();
        }
        echo $OUTPUT->box_end();
        echo $OUTPUT->box_end();
    }
    echo $OUTPUT->box_end();
} else {
    echo $OUTPUT->box(get_string('no_items_available_yet', 'feedback2'),
                    'generalbox boxaligncenter boxwidthwide');
}
echo $OUTPUT->continue_button('view.php?id='.$id);
echo $OUTPUT->box_end();
/// Finish the page
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

echo $OUTPUT->footer();

