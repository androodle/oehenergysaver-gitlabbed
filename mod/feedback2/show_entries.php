<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * print the single entries
 *
 * @author Andreas Grabs
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package feedback2
 */

require_once("../../config.php");
require_once("lib.php");
require_once($CFG->libdir.'/tablelib.php');

////////////////////////////////////////////////////////
//get the params
////////////////////////////////////////////////////////
$id = required_param('id', PARAM_INT);
$userid = optional_param('userid', false, PARAM_INT);
$do_show = required_param('do_show', PARAM_ALPHA);
$perpage = optional_param('perpage', FEEDBACK_DEFAULT_PAGE_COUNT, PARAM_INT);  // how many per page
$showall = optional_param('showall', false, PARAM_INT);  // should we show all users
// $SESSION->feedback2->current_tab = $do_show;
$current_tab = $do_show;

////////////////////////////////////////////////////////
//get the objects
////////////////////////////////////////////////////////

if (! $cm = get_coursemodule_from_id('feedback2', $id)) {
    print_error('invalidcoursemodule');
}

if (! $course = $DB->get_record("course", array("id"=>$cm->course))) {
    print_error('coursemisconf');
}

if (! $feedback2 = $DB->get_record("feedback2", array("id"=>$cm->instance))) {
    print_error('invalidcoursemodule');
}

$url = new moodle_url('/mod/feedback2/show_entries.php', array('id'=>$cm->id, 'do_show'=>$do_show));

$PAGE->set_url($url);

$context = context_module::instance($cm->id);

require_login($course, true, $cm);

require_capability('mod/feedback2:viewreports', $context);

////////////////////////////////////////////////////////
//get the responses of given user
////////////////////////////////////////////////////////
if ($do_show == 'showoneentry') {
    //get the feedback2items
    $feedback2items = $DB->get_records('feedback2_item', array('feedback2'=>$feedback2->id), 'position');

    $params = array('feedback2'=>$feedback2->id,
                    'userid'=>$userid,
                    'anonymous_response'=>FEEDBACK_ANONYMOUS_NO);

    $feedback2completed = $DB->get_record('feedback2_completed', $params); //arb
}

/// Print the page header
$strfeedback2s = get_string("modulenameplural", "feedback2");
$strfeedback2  = get_string("modulename", "feedback2");

$PAGE->set_heading($course->fullname);
$PAGE->set_title($feedback2->name);
echo $OUTPUT->header();
echo $OUTPUT->heading(format_string($feedback2->name));

require('tabs.php');

/// Print the main part of the page
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////
/// Print the links to get responses and analysis
////////////////////////////////////////////////////////
if ($do_show == 'showentries') {
    //print the link to analysis
    if (has_capability('mod/feedback2:viewreports', $context)) {
        //get the effective groupmode of this course and module
        if (isset($cm->groupmode) && empty($course->groupmodeforce)) {
            $groupmode =  $cm->groupmode;
        } else {
            $groupmode = $course->groupmode;
        }

        $groupselect = groups_print_activity_menu($cm, $url->out(), true);
        $mygroupid = groups_get_activity_group($cm);

        // preparing the table for output
        $baseurl = new moodle_url('/mod/feedback2/show_entries.php');
        $baseurl->params(array('id'=>$id, 'do_show'=>$do_show, 'showall'=>$showall));

        $tablecolumns = array('userpic', 'fullname', 'completed_timemodified');
        $tableheaders = array(get_string('userpic'), get_string('fullnameuser'), get_string('date'));

        if (has_capability('mod/feedback2:deletesubmissions', $context)) {
            $tablecolumns[] = 'deleteentry';
            $tableheaders[] = '';
        }

        $table = new flexible_table('feedback2-showentry-list-'.$course->id);

        $table->define_columns($tablecolumns);
        $table->define_headers($tableheaders);
        $table->define_baseurl($baseurl);

        $table->sortable(true, 'lastname', SORT_DESC);
        $table->set_attribute('cellspacing', '0');
        $table->set_attribute('id', 'showentrytable');
        $table->set_attribute('class', 'generaltable generalbox');
        $table->set_control_variables(array(
                    TABLE_VAR_SORT    => 'ssort',
                    TABLE_VAR_IFIRST  => 'sifirst',
                    TABLE_VAR_ILAST   => 'silast',
                    TABLE_VAR_PAGE    => 'spage'
                    ));
        $table->setup();

        if ($table->get_sql_sort()) {
            $sort = $table->get_sql_sort();
        } else {
            $sort = '';
        }

        list($where, $params) = $table->get_sql_where();
        if ($where) {
            $where .= ' AND';
        }

        //get students in conjunction with groupmode
        if ($groupmode > 0) {
            if ($mygroupid > 0) {
                $usedgroupid = $mygroupid;
            } else {
                $usedgroupid = false;
            }
        } else {
            $usedgroupid = false;
        }

        $matchcount = feedback2_count_complete_users($cm, $usedgroupid);
        $table->initialbars(true);

        if ($showall) {
            $startpage = false;
            $pagecount = false;
        } else {
            $table->pagesize($perpage, $matchcount);
            $startpage = $table->get_page_start();
            $pagecount = $table->get_page_size();
        }

        $students = feedback2_get_complete_users($cm, $usedgroupid, $where, $params, $sort, $startpage, $pagecount);
        $str_analyse = get_string('analysis', 'feedback2');
        $str_complete = get_string('completed_feedback2s', 'feedback2');
        $str_course = get_string('course');

        $completed_fb_count = feedback2_get_completeds_group_count($feedback2, $mygroupid);
        if ($feedback2->course == SITEID) {
            $analysisurl = new moodle_url('/mod/feedback2/analysis_course.php', array('id'=>$id, 'courseid'=>$courseid));
            echo $OUTPUT->box_start('mdl-align');
            echo '<a href="'.$analysisurl->out().'">';
            echo $str_course.' '.$str_analyse.' ('.$str_complete.': '.intval($completed_fb_count).')';
            echo '</a>';
            echo $OUTPUT->help_icon('viewcompleted', 'feedback2');
            echo $OUTPUT->box_end();
        } else {
            $analysisurl = new moodle_url('/mod/feedback2/analysis.php', array('id'=>$id, 'courseid'=>$courseid));
            echo $OUTPUT->box_start('mdl-align');
            echo '<a href="'.$analysisurl->out().'">';
            echo $str_analyse.' ('.$str_complete.': '.intval($completed_fb_count).')';
            echo '</a>';
            echo $OUTPUT->box_end();
        }
    }

    //####### viewreports-start
    if (has_capability('mod/feedback2:viewreports', $context)) {
        //print the list of students
        echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');
        echo isset($groupselect) ? $groupselect : '';
        echo '<div class="clearer"></div>';
        echo $OUTPUT->box_start('mdl-align');
        if (!$students) {
            $table->print_html();
        } else {
            echo print_string('non_anonymous_entries', 'feedback2');
            echo ' ('.count($students).')<hr />';

            foreach ($students as $student) {
                $params = array('userid'=>$student->id,
                                'feedback2'=>$feedback2->id,
                                'anonymous_response'=>FEEDBACK_ANONYMOUS_NO);

                $completed_count = $DB->count_records('feedback2_completed', $params);
                if ($completed_count > 0) {

                    //userpicture and link to the profilepage
                    $fullname_url = $CFG->wwwroot.'/user/view.php?id='.$student->id.'&amp;course='.$course->id;
                    $profilelink = '<strong><a href="'.$fullname_url.'">'.fullname($student).'</a></strong>';
                    $data = array ($OUTPUT->user_picture($student, array('courseid'=>$course->id)), $profilelink);

                    //link to the entry of the user
                    $params = array('feedback2'=>$feedback2->id,
                                    'userid'=>$student->id,
                                    'anonymous_response'=>FEEDBACK_ANONYMOUS_NO);

                    $feedback2completed = $DB->get_record('feedback2_completed', $params);
                    $showentryurl_params = array('userid'=>$student->id, 'do_show'=>'showoneentry');
                    $showentryurl = new moodle_url($url, $showentryurl_params);
                    $showentrylink = '<a href="'.$showentryurl->out().'">'.userdate($feedback2completed->timemodified).'</a>';
                    $data[] = $showentrylink;

                    //link to delete the entry
                    if (has_capability('mod/feedback2:deletesubmissions', $context)) {
                        $delete_url_params = array('id' => $cm->id,
                                            'completedid' => $feedback2completed->id,
                                            'do_show' => 'showoneentry');

                        $deleteentryurl = new moodle_url($CFG->wwwroot.'/mod/feedback2/delete_completed.php', $delete_url_params);
                        $deleteentrylink = '<a href="'.$deleteentryurl->out().'">'.
                                                get_string('delete_entry', 'feedback2').
                                            '</a>';
                        $data[] = $deleteentrylink;
                    }
                    $table->add_data($data);
                }
            }
            $table->print_html();

            $allurl = new moodle_url($baseurl);

            if ($showall) {
                $allurl->param('showall', 0);
                echo $OUTPUT->container(html_writer::link($allurl, get_string('showperpage', '', FEEDBACK_DEFAULT_PAGE_COUNT)),
                                        array(),
                                        'showall');

            } else if ($matchcount > 0 && $perpage < $matchcount) {
                $allurl->param('showall', 1);
                echo $OUTPUT->container(html_writer::link($allurl, get_string('showall', '', $matchcount)),
                                        array(),
                                        'showall');
            }
        }
        ?>
        <hr />
        <table width="100%">
            <tr>
                <td align="left" colspan="2">
                    <?php
                    $params = array('feedback2' => $feedback2->id,
                                    'anonymous_response' => FEEDBACK_ANONYMOUS_YES);

                    $feedback2_completeds_count = $DB->count_records('feedback2_completed', $params);
                    print_string('anonymous_entries', 'feedback2');
                    echo '&nbsp;('.$feedback2_completeds_count.')';
                    ?>
                </td>
                <td align="right">
                    <?php
                        $url_params = array('sesskey'=>sesskey(),
                                        'userid'=>0,
                                        'do_show'=>'showoneentry',
                                        'id'=>$id);
                        $aurl = new moodle_url('show_entries_anonym.php', $url_params);
                        echo $OUTPUT->single_button($aurl, get_string('show_entries', 'feedback2'));
                    ?>
                </td>
            </tr>
        </table>
        <?php
        echo $OUTPUT->box_end();
        echo $OUTPUT->box_end();
    }

}
////////////////////////////////////////////////////////
/// Print the responses of the given user
////////////////////////////////////////////////////////
if ($do_show == 'showoneentry') {
    echo $OUTPUT->heading(format_text($feedback2->name));

    //print the items
    if (is_array($feedback2items)) {
        $align = right_to_left() ? 'right' : 'left';
        $usr = $DB->get_record('user', array('id'=>$userid));

        if ($feedback2completed) {
            echo $OUTPUT->heading(userdate($feedback2completed->timemodified).' ('.fullname($usr).')', 3);
        } else {
            echo $OUTPUT->heading(get_string('not_completed_yet', 'feedback2'), 3);
        }

        echo $OUTPUT->box_start('feedback2_items');
        $itemnr = 0;
        foreach ($feedback2items as $feedback2item) {
            //get the values
            $params = array('completed'=>$feedback2completed->id, 'item'=>$feedback2item->id);
            $value = $DB->get_record('feedback2_value', $params);
            echo $OUTPUT->box_start('feedback2_item_box_'.$align);
            if ($feedback2item->hasvalue == 1 AND $feedback2->autonumbering) {
                $itemnr++;
                echo $OUTPUT->box_start('feedback2_item_number_'.$align);
                echo $itemnr;
                echo $OUTPUT->box_end();
            }

            if ($feedback2item->typ != 'pagebreak') {
                echo $OUTPUT->box_start('box generalbox boxalign_'.$align);
                if (isset($value->value)) {
                    feedback2_print_item_show_value($feedback2item, $value->value);
                } else {
                    feedback2_print_item_show_value($feedback2item, false);
                }
                echo $OUTPUT->box_end();
            }
            echo $OUTPUT->box_end();
        }
        echo $OUTPUT->box_end();
    }
    echo $OUTPUT->continue_button(new moodle_url($url, array('do_show'=>'showentries')));
}
/// Finish the page
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

echo $OUTPUT->footer();

