<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package moodlecore
 * @subpackage backup-moodle2
 * @copyright 2010 onwards Eloy Lafuente (stronk7) {@link http://stronk7.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the restore steps that will be used by the restore_feedback2_activity_task
 */

/**
 * Structure step to restore one feedback2 activity
 */
class restore_feedback2_activity_structure_step extends restore_activity_structure_step {

    protected function define_structure() {

        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');

        $paths[] = new restore_path_element('feedback2', '/activity/feedback2');
        $paths[] = new restore_path_element('feedback2_item', '/activity/feedback2/items/item');
        if ($userinfo) {
            $paths[] = new restore_path_element('feedback2_completed', '/activity/feedback2/completeds/completed');
            $paths[] = new restore_path_element('feedback2_completed_history', '/activity/feedback2/completeds_history/completed_history');
            $paths[] = new restore_path_element('feedback2_value', '/activity/feedback2/completeds/completed/values/value');
            $paths[] = new restore_path_element('feedback2_value_history', '/activity/feedback2/completeds_history/completed_history/values_history/value_history');
            $paths[] = new restore_path_element('feedback2_tracking', '/activity/feedback2/trackings/tracking');
        }

        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }

    protected function process_feedback2($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();

        $data->timeopen = $this->apply_date_offset($data->timeopen);
        $data->timeclose = $this->apply_date_offset($data->timeclose);
        $data->timemodified = $this->apply_date_offset($data->timemodified);

        // insert the feedback2 record
        $newitemid = $DB->insert_record('feedback2', $data);
        // immediately after inserting "activity" record, call this
        $this->apply_activity_instance($newitemid);
    }

    protected function process_feedback2_item($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->feedback2 = $this->get_new_parentid('feedback2');

        //dependitem
        $data->dependitem = $this->get_mappingid('feedback2_item', $data->dependitem);

        $newitemid = $DB->insert_record('feedback2_item', $data);
        $this->set_mapping('feedback2_item', $oldid, $newitemid, true); // Can have files
    }

    protected function process_feedback2_completed($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->feedback2 = $this->get_new_parentid('feedback2');
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->timemodified = $this->apply_date_offset($data->timemodified);

        $newitemid = $DB->insert_record('feedback2_completed', $data);
        $this->set_mapping('feedback2_completed', $oldid, $newitemid);
    }

    protected function process_feedback2_completed_history($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->feedback2 = $this->get_new_parentid('feedback2');
        $data->userid = $this->get_mappingid('user', $data->userid);

        $newitemid = $DB->insert_record('feedback2_completed_history', $data);
        $this->set_mapping('feedback2_completed_history', $oldid, $newitemid);
    }

    protected function process_feedback2_value($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->completed = $this->get_new_parentid('feedback2_completed');
        $data->item = $this->get_mappingid('feedback2_item', $data->item);
        $data->course_id = $this->get_courseid();

        $newitemid = $DB->insert_record('feedback2_value', $data);
        $this->set_mapping('feedback2_value', $oldid, $newitemid);
    }

    protected function process_feedback2_value_history($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->completed = $this->get_new_parentid('feedback2_completed_history');
        $data->item = $this->get_mappingid('feedback2_item', $data->item);
        $data->course_id = $this->get_courseid();

        $newitemid = $DB->insert_record('feedback2_value_history', $data);
        $this->set_mapping('feedback2_value_history', $oldid, $newitemid);
    }

    protected function process_feedback2_tracking($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->feedback2 = $this->get_new_parentid('feedback2');
        $data->completed = $this->get_mappingid('feedback2_completed', $data->completed);
        $data->userid = $this->get_mappingid('user', $data->userid);

        $newitemid = $DB->insert_record('feedback2_tracking', $data);
    }


    protected function after_execute() {
        // Add feedback2 related files, no need to match by itemname (just internally handled context)
        $this->add_related_files('mod_feedback2', 'intro', null);
        $this->add_related_files('mod_feedback2', 'page_after_submit', null);
        $this->add_related_files('mod_feedback2', 'item', 'feedback2_item');
    }
}
