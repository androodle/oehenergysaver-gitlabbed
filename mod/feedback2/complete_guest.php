<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * prints the form so an anonymous user can fill out the feedback2 on the mainsite
 *
 * @author Andreas Grabs
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package feedback2
 */

require_once("../../config.php");
require_once("lib.php");

feedback2_init_feedback2_session();

$id = required_param('id', PARAM_INT);
$completedid = optional_param('completedid', false, PARAM_INT);
$preservevalues  = optional_param('preservevalues', 0,  PARAM_INT);
$courseid = optional_param('courseid', false, PARAM_INT);
$gopage = optional_param('gopage', -1, PARAM_INT);
$lastpage = optional_param('lastpage', false, PARAM_INT);
$startitempos = optional_param('startitempos', 0, PARAM_INT);
$lastitempos = optional_param('lastitempos', 0, PARAM_INT);

$url = new moodle_url('/mod/feedback2/complete_guest.php', array('id'=>$id));
if ($completedid !== false) {
    $url->param('completedid', $completedid);
}
if ($preservevalues !== 0) {
    $url->param('preservevalues', $preservevalues);
}
if ($courseid !== false) {
    $url->param('courseid', $courseid);
}
if ($gopage !== -1) {
    $url->param('gopage', $gopage);
}
if ($lastpage !== false) {
    $url->param('lastpage', $lastpage);
}
if ($startitempos !== 0) {
    $url->param('startitempos', $startitempos);
}
if ($lastitempos !== 0) {
    $url->param('lastitempos', $lastitempos);
}
$PAGE->set_url($url);

$highlightrequired = false;

if (($formdata = data_submitted()) AND !confirm_sesskey()) {
    print_error('invalidsesskey');
}

//if the use hit enter into a textfield so the form should not submit
if (isset($formdata->sesskey) AND
   !isset($formdata->savevalues) AND
   !isset($formdata->gonextpage) AND
   !isset($formdata->gopreviouspage)) {

    $gopage = (int) $formdata->lastpage;
}
if (isset($formdata->savevalues)) {
    $savevalues = true;
} else {
    $savevalues = false;
}

if ($gopage < 0 AND !$savevalues) {
    if (isset($formdata->gonextpage)) {
        $gopage = $lastpage + 1;
        $gonextpage = true;
        $gopreviouspage = false;
    } else if (isset($formdata->gopreviouspage)) {
        $gopage = $lastpage - 1;
        $gonextpage = false;
        $gopreviouspage = true;
    } else {
        print_error('parameters_missing', 'feedback2');
    }
} else {
    $gonextpage = $gopreviouspage = false;
}

if (! $cm = get_coursemodule_from_id('feedback2', $id)) {
    print_error('invalidcoursemodule');
}

if (! $course = $DB->get_record("course", array("id"=>$cm->course))) {
    print_error('coursemisconf');
}

if (! $feedback2 = $DB->get_record("feedback2", array("id"=>$cm->instance))) {
    print_error('invalidcoursemodule');
}

$context = context_module::instance($cm->id);

$feedback2_complete_cap = false;

if (isset($CFG->feedback2_allowfullanonymous)
            AND $CFG->feedback2_allowfullanonymous
            AND $course->id == SITEID
            AND (!$courseid OR $courseid == SITEID)
            AND $feedback2->anonymous == FEEDBACK_ANONYMOUS_YES ) {
    $feedback2_complete_cap = true;
}

//check whether the feedback2 is anonymous
if (isset($CFG->feedback2_allowfullanonymous)
                AND $CFG->feedback2_allowfullanonymous
                AND $feedback2->anonymous == FEEDBACK_ANONYMOUS_YES
                AND $course->id == SITEID ) {
    $feedback2_complete_cap = true;
}
if ($feedback2->anonymous != FEEDBACK_ANONYMOUS_YES) {
    print_error('feedback2_is_not_for_anonymous', 'feedback2');
}

//check whether the user has a session
// there used to be a sesskey test - this could not work - sorry

//check whether the feedback2 is located and! started from the mainsite
if ($course->id == SITEID AND !$courseid) {
    $courseid = SITEID;
}

require_course_login($course);

if ($courseid AND $courseid != SITEID) {
    $course2 = $DB->get_record('course', array('id'=>$courseid));
    require_course_login($course2); //this overwrites the object $course :-(
    $course = $DB->get_record("course", array("id"=>$cm->course)); // the workaround
}

if (!$feedback2_complete_cap) {
    print_error('error');
}


/// Print the page header
$strfeedback2s = get_string("modulenameplural", "feedback2");
$strfeedback2  = get_string("modulename", "feedback2");

$PAGE->set_cm($cm, $course); // set's up global $COURSE
$PAGE->set_pagelayout('incourse');

$urlparams = array('id'=>$course->id);
$PAGE->navbar->add($strfeedback2s, new moodle_url('/mod/feedback2/index.php', $urlparams));
$PAGE->navbar->add(format_string($feedback2->name));
$PAGE->set_heading($course->fullname);
$PAGE->set_title($feedback2->name);
echo $OUTPUT->header();

//ishidden check.
//hidden feedback2s except feedback2s on mainsite are only accessible with related capabilities
if ((empty($cm->visible) AND
        !has_capability('moodle/course:viewhiddenactivities', $context)) AND
        $course->id != SITEID) {
    notice(get_string("activityiscurrentlyhidden"));
}

//check, if the feedback2 is open (timeopen, timeclose)
$checktime = time();

$feedback2_is_closed = ($feedback2->timeopen > $checktime) ||
                      ($feedback2->timeclose < $checktime &&
                            $feedback2->timeclose > 0);

if ($feedback2_is_closed) {
    echo $OUTPUT->box_start('generalbox boxaligncenter');
    echo $OUTPUT->notification(get_string('feedback2_is_not_open', 'feedback2'));
    echo $OUTPUT->continue_button($CFG->wwwroot.'/course/view.php?id='.$course->id);
    echo $OUTPUT->box_end();
    echo $OUTPUT->footer();
    exit;
}

//additional check for multiple-submit (prevent browsers back-button).
//the main-check is in view.php
$feedback2_can_submit = true;
if ($feedback2->multiple_submit == 0 ) {
    if (feedback2_is_already_submitted($feedback2->id, $courseid)) {
        $feedback2_can_submit = false;
    }
}
if ($feedback2_can_submit) {
    //preserving the items
    if ($preservevalues == 1) {
        if (!$SESSION->feedback2->is_started == true) {
            print_error('error', 'error', $CFG->wwwroot.'/course/view.php?id='.$course->id);
        }
        //check, if all required items have a value
        if (feedback2_check_values($startitempos, $lastitempos)) {
            $userid = $USER->id; //arb
            if ($completedid = feedback2_save_guest_values(sesskey())) {
                //now it can be saved
                if (!$gonextpage AND !$gopreviouspage) {
                    $preservevalues = false;
                }

            } else {
                $savereturn = 'failed';
                if (isset($lastpage)) {
                    $gopage = $lastpage;
                } else {
                    print_error('parameters_missing', 'feedback2');
                }
            }
        } else {
            $savereturn = 'missing';
            $highlightrequired = true;
            if (isset($lastpage)) {
                $gopage = $lastpage;
            } else {
                print_error('parameters_missing', 'feedback2');
            }
        }
    }

    //saving the items
    if ($savevalues AND !$preservevalues) {
        //exists there any pagebreak, so there are values in the feedback2_valuetmp
        //arb changed from 0 to $USER->id
        //no strict anonymous feedback2s
        //if it is a guest taking it then I want to know that it was
        //a guest (at least in the data saved in the feedback2 tables)
        $userid = $USER->id;

        $params = array('id'=>$completedid);
        $feedback2completedtmp = $DB->get_record('feedback2_completedtmp', $params);

        //fake saving for switchrole
        $is_switchrole = feedback2_check_is_switchrole();
        if ($is_switchrole) {
            $savereturn = 'saved';
            feedback2_delete_completedtmp($completedid);
        } else {
            $new_completed_id = feedback2_save_tmp_values($feedback2completedtmp, false, $userid);
            if ($new_completed_id) {
                $savereturn = 'saved';
                feedback2_send_email_anonym($cm, $feedback2, $course, $userid);
                unset($SESSION->feedback2->is_started);

            } else {
                $savereturn = 'failed';
            }
        }
    }

    if ($allbreaks = feedback2_get_all_break_positions($feedback2->id)) {
        if ($gopage <= 0) {
            $startposition = 0;
        } else {
            if (!isset($allbreaks[$gopage - 1])) {
                $gopage = count($allbreaks);
            }
            $startposition = $allbreaks[$gopage - 1];
        }
        $ispagebreak = true;
    } else {
        $startposition = 0;
        $newpage = 0;
        $ispagebreak = false;
    }

    //get the feedback2items after the last shown pagebreak
    $select = 'feedback2 = ? AND position > ?';
    $params = array($feedback2->id, $startposition);
    $feedback2items = $DB->get_records_select('feedback2_item', $select, $params, 'position');

    //get the first pagebreak
    $params = array('feedback2'=>$feedback2->id, 'typ'=>'pagebreak');
    if ($pagebreaks = $DB->get_records('feedback2_item', $params, 'position')) {
        $pagebreaks = array_values($pagebreaks);
        $firstpagebreak = $pagebreaks[0];
    } else {
        $firstpagebreak = false;
    }
    $maxitemcount = $DB->count_records('feedback2_item', array('feedback2'=>$feedback2->id));
    $feedback2completedtmp = feedback2_get_current_completed($feedback2->id,
                                                           true,
                                                           $courseid,
                                                           sesskey());

    /// Print the main part of the page
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    $analysisurl = new moodle_url('/mod/feedback2/analysis.php', array('id'=>$id));
    if ($courseid > 0) {
        $analysisurl->param('courseid', $courseid);
    }
    echo $OUTPUT->heading(format_text($feedback2->name));

    if ( (intval($feedback2->publish_stats) == 1) AND
            ( has_capability('mod/feedback2:viewanalysepage', $context)) AND
            !( has_capability('mod/feedback2:viewreports', $context)) ) {
        echo $OUTPUT->box_start('mdl-align');
        echo '<a href="'.$analysisurl->out().'">';
        echo get_string('completed_feedback2s', 'feedback2');
        echo '</a>';
        echo $OUTPUT->box_end();
    }

    if (isset($savereturn) && $savereturn == 'saved') {
        if ($feedback2->page_after_submit) {
            require_once($CFG->libdir . '/filelib.php');

            $page_after_submit_output = file_rewrite_pluginfile_urls($feedback2->page_after_submit,
                                                                    'pluginfile.php',
                                                                    $context->id,
                                                                    'mod_feedback2',
                                                                    'page_after_submit',
                                                                    0);

            echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');
            echo format_text($page_after_submit_output,
                             $feedback2->page_after_submitformat,
                             array('overflowdiv' => true));
            echo $OUTPUT->box_end();
        } else {
            echo '<p align="center"><b><font color="green">';
            echo get_string('entries_saved', 'feedback2');
            echo '</font></b></p>';
            if ( intval($feedback2->publish_stats) == 1) {
                echo '<p align="center"><a href="'.$analysisurl->out().'">';
                echo get_string('completed_feedback2s', 'feedback2').'</a>';
                echo '</p>';
            }
        }
        if ($feedback2->site_after_submit) {
            $url = feedback2_encode_target_url($feedback2->site_after_submit);
        } else {
            if ($courseid) {
                if ($courseid == SITEID) {
                    $url = $CFG->wwwroot;
                } else {
                    $url = $CFG->wwwroot.'/course/view.php?id='.$courseid;
                }
            } else {
                if ($course->id == SITEID) {
                    $url = $CFG->wwwroot;
                } else {
                    $url = $CFG->wwwroot.'/course/view.php?id='.$course->id;
                }
            }
        }
        echo $OUTPUT->continue_button($url);
    } else {
        if (isset($savereturn) && $savereturn == 'failed') {
            echo $OUTPUT->box_start('mform');
            echo '<span class="error">'.get_string('saving_failed', 'feedback2').'</span>';
            echo $OUTPUT->box_end();
        }

        if (isset($savereturn) && $savereturn == 'missing') {
            echo $OUTPUT->box_start('mform');
            echo '<span class="error">'.get_string('saving_failed_because_missing_or_false_values', 'feedback2').'</span>';
            echo $OUTPUT->box_end();
        }

        //print the items
        if (is_array($feedback2items)) {
            echo $OUTPUT->box_start('feedback2_form');
            echo '<form action="complete_guest.php" class="mform" method="post" onsubmit=" ">';
            echo '<fieldset>';
            echo '<input type="hidden" name="anonymous" value="0" />';
            $inputvalue = 'value="'.FEEDBACK_ANONYMOUS_YES.'"';
            echo '<input type="hidden" name="anonymous_response" '.$inputvalue.' />';
            echo '<input type="hidden" name="sesskey" value="'.sesskey().'" />';
            //check, if there exists required-elements
            $params = array('feedback2'=>$feedback2->id, 'required'=>1);
            $countreq = $DB->count_records('feedback2_item', $params);
            if ($countreq > 0) {
                echo '<div class="fdescription required">';
                echo get_string('somefieldsrequired', 'form', '<img alt="'.get_string('requiredelement', 'form').
                    '" src="'.$OUTPUT->pix_url('req') .'" class="req" />');
                echo '</div>';
            }
            echo $OUTPUT->box_start('feedback2_items');

            $startitem = null;
            $select = 'feedback2 = ? AND hasvalue = 1 AND position < ?';
            $params = array($feedback2->id, $startposition);
            $itemnr = $DB->count_records_select('feedback2_item', $select, $params);
            $lastbreakposition = 0;
            $align = right_to_left() ? 'right' : 'left';

            foreach ($feedback2items as $feedback2item) {
                if (!isset($startitem)) {
                    //avoid showing double pagebreaks
                    if ($feedback2item->typ == 'pagebreak') {
                        continue;
                    }
                    $startitem = $feedback2item;
                }

                if ($feedback2item->dependitem > 0) {
                    //chech if the conditions are ok
                    $fb_compare_value = feedback2_compare_item_value($feedback2completedtmp->id,
                                                                    $feedback2item->dependitem,
                                                                    $feedback2item->dependvalue,
                                                                    true);
                    if (!isset($feedback2completedtmp->id) OR !$fb_compare_value) {
                        $lastitem = $feedback2item;
                        $lastbreakposition = $feedback2item->position;
                        continue;
                    }
                }

                if ($feedback2item->dependitem > 0) {
                    $dependstyle = ' feedback2_complete_depend';
                } else {
                    $dependstyle = '';
                }

                echo $OUTPUT->box_start('feedback2_item_box_'.$align.$dependstyle);
                $value = '';
                //get the value
                $frmvaluename = $feedback2item->typ . '_'. $feedback2item->id;
                if (isset($savereturn)) {
                    $value = isset($formdata->{$frmvaluename}) ? $formdata->{$frmvaluename} : null;
                    $value = feedback2_clean_input_value($feedback2item, $value);
                } else {
                    if (isset($feedback2completedtmp->id)) {
                        $value = feedback2_get_item_value($feedback2completedtmp->id,
                                                         $feedback2item->id,
                                                         sesskey());
                    }
                }
                if ($feedback2item->hasvalue == 1 AND $feedback2->autonumbering) {
                    $itemnr++;
                    echo $OUTPUT->box_start('feedback2_item_number_'.$align);
                    echo $itemnr;
                    echo $OUTPUT->box_end();
                }
                if ($feedback2item->typ != 'pagebreak') {
                    echo $OUTPUT->box_start('box generalbox boxalign_'.$align);
                    feedback2_print_item_complete($feedback2item, $value, $highlightrequired);
                    echo $OUTPUT->box_end();
                }

                echo $OUTPUT->box_end();

                $lastbreakposition = $feedback2item->position; //last item-pos (item or pagebreak)
                if ($feedback2item->typ == 'pagebreak') {
                    break;
                } else {
                    $lastitem = $feedback2item;
                }
            }
            echo $OUTPUT->box_end();
            echo '<input type="hidden" name="id" value="'.$id.'" />';
            echo '<input type="hidden" name="feedback2id" value="'.$feedback2->id.'" />';
            echo '<input type="hidden" name="lastpage" value="'.$gopage.'" />';
            if (isset($feedback2completedtmp->id)) {
                $inputvalue = 'value="'.$feedback2completedtmp->id;
            } else {
                $inputvalue = 'value=""';
            }
            echo '<input type="hidden" name="completedid" '.$inputvalue.' />';
            echo '<input type="hidden" name="courseid" value="'. $courseid . '" />';
            echo '<input type="hidden" name="preservevalues" value="1" />';
            if (isset($startitem)) {
                echo '<input type="hidden" name="startitempos" value="'.$startitem->position.'" />';
                echo '<input type="hidden" name="lastitempos" value="'.$lastitem->position.'" />';
            }

            if ($ispagebreak AND $lastbreakposition > $firstpagebreak->position) {
                $inputvalue = 'value="'.get_string('previous_page', 'feedback2').'"';
                echo '<input name="gopreviouspage" type="submit" '.$inputvalue.' />';
            }
            if ($lastbreakposition < $maxitemcount) {
                $inputvalue = 'value="'.get_string('next_page', 'feedback2').'"';
                echo '<input name="gonextpage" type="submit" '.$inputvalue.' />';
            }
            if ($lastbreakposition >= $maxitemcount) { //last page
                $inputvalue = 'value="'.get_string('save_entries', 'feedback2').'"';
                echo '<input name="savevalues" type="submit" '.$inputvalue.' />';
            }

            echo '</form>';
            echo $OUTPUT->box_end();

            echo $OUTPUT->box_start('feedback2_complete_cancel');
            if ($courseid) {
                $action = 'action="'.$CFG->wwwroot.'/course/view.php?id='.$courseid.'"';
            } else {
                if ($course->id == SITEID) {
                    $action = 'action="'.$CFG->wwwroot.'"';
                } else {
                    $action = 'action="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'"';
                }
            }
            echo '<form '.$action.' method="post" onsubmit=" ">';
            echo '<input type="hidden" name="sesskey" value="'.sesskey().'" />';
            echo '<input type="hidden" name="courseid" value="'. $courseid . '" />';
            echo '<button type="submit">'.get_string('cancel').'</button>';
            echo '</form>';
            echo $OUTPUT->box_end();
            $SESSION->feedback2->is_started = true;
        }
    }
} else {
    echo $OUTPUT->box_start('generalbox boxaligncenter');
    echo $OUTPUT->notification(get_string('this_feedback2_is_already_submitted', 'feedback2'));
    echo $OUTPUT->continue_button($CFG->wwwroot.'/course/view.php?id='.$course->id);
    echo $OUTPUT->box_end();
}
/// Finish the page
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

echo $OUTPUT->footer();

