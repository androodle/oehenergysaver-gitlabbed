<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * prints the form so the user can fill out the feedback2
 *
 * @author Andreas Grabs
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package feedback2
 */

require_once("../../config.php");
require_once("lib.php");
require_once($CFG->libdir . '/completionlib.php');
require_once($CFG->libdir.'/gradelib.php');

feedback2_init_feedback2_session();

$id = required_param('id', PARAM_INT);
$completedid = optional_param('completedid', false, PARAM_INT);
$preservevalues  = optional_param('preservevalues', 0,  PARAM_INT);
$courseid = optional_param('courseid', false, PARAM_INT);
$gopage = optional_param('gopage', -1, PARAM_INT);
$lastpage = optional_param('lastpage', false, PARAM_INT);
$startitempos = optional_param('startitempos', 0, PARAM_INT);
$lastitempos = optional_param('lastitempos', 0, PARAM_INT);
$anonymous_response = optional_param('anonymous_response', 0, PARAM_INT); //arb

$highlightrequired = false;

if (($formdata = data_submitted()) AND !confirm_sesskey()) {
    print_error('invalidsesskey');
}

//if the use hit enter into a textfield so the form should not submit
if (isset($formdata->sesskey) AND
    !isset($formdata->savevalues) AND
    !isset($formdata->gonextpage) AND
    !isset($formdata->gopreviouspage)) {

    $gopage = $formdata->lastpage;
}

if (isset($formdata->savevalues)) {
    $savevalues = true;
} else {
    $savevalues = false;
}

if ($gopage < 0 AND !$savevalues) {
    if (isset($formdata->gonextpage)) {
        $gopage = $lastpage + 1;
        $gonextpage = true;
        $gopreviouspage = false;
    } else if (isset($formdata->gopreviouspage)) {
        $gopage = $lastpage - 1;
        $gonextpage = false;
        $gopreviouspage = true;
    } else {
        print_error('missingparameter');
    }
} else {
    $gonextpage = $gopreviouspage = false;
}

if (! $cm = get_coursemodule_from_id('feedback2', $id)) {
    print_error('invalidcoursemodule');
}

if (! $course = $DB->get_record("course", array("id"=>$cm->course))) {
    print_error('coursemisconf');
}

if (! $feedback2 = $DB->get_record("feedback2", array("id"=>$cm->instance))) {
    print_error('invalidcoursemodule');
}

$context = context_module::instance($cm->id);

$feedback2_complete_cap = false;

if (has_capability('mod/feedback2:complete', $context)) {
    $feedback2_complete_cap = true;
}

//check whether the feedback2 is located and! started from the mainsite
if ($course->id == SITEID AND !$courseid) {
    $courseid = SITEID;
}

//check whether the feedback2 is mapped to the given courseid
if ($course->id == SITEID AND !has_capability('mod/feedback2:edititems', $context)) {
    if ($DB->get_records('feedback2_sitecourse_map', array('feedback2id'=>$feedback2->id))) {
        $params = array('feedback2id'=>$feedback2->id, 'courseid'=>$courseid);
        if (!$DB->get_record('feedback2_sitecourse_map', $params)) {
            print_error('notavailable', 'feedback2');
        }
    }
}

if ($feedback2->anonymous != FEEDBACK_ANONYMOUS_YES) {
    if ($course->id == SITEID) {
        require_login($course, true);
    } else {
        require_login($course, true, $cm);
    }
} else {
    if ($course->id == SITEID) {
        require_course_login($course, true);
    } else {
        require_course_login($course, true, $cm);
    }
}

//check whether the given courseid exists
if ($courseid AND $courseid != SITEID) {
    if ($course2 = $DB->get_record('course', array('id'=>$courseid))) {
        require_course_login($course2); //this overwrites the object $course :-(
        $course = $DB->get_record("course", array("id"=>$cm->course)); // the workaround
    } else {
        print_error('invalidcourseid');
    }
}

if (!$feedback2_complete_cap) {
    print_error('error');
}

// Mark activity viewed for completion-tracking
$completion = new completion_info($course);
$completion->set_module_viewed($cm);

/// Print the page header
$strfeedback2s = get_string("modulenameplural", "feedback2");
$strfeedback2  = get_string("modulename", "feedback2");

if ($course->id == SITEID) {
    $PAGE->set_cm($cm, $course); // set's up global $COURSE
    $PAGE->set_pagelayout('incourse');
}

$PAGE->navbar->add(get_string('feedback2:complete', 'feedback2'));
$urlparams = array('id'=>$cm->id, 'gopage'=>$gopage, 'courseid'=>$course->id);
$PAGE->set_url('/mod/feedback2/complete.php', $urlparams);
$PAGE->set_heading($course->fullname);
$PAGE->set_title($feedback2->name);
$PAGE->requires->js('/mod/feedback2/feedback2.js');
$PAGE->requires->js_init_call('M.mod_feedback2.init');
echo $OUTPUT->header();

//ishidden check.
//feedback2 in courses
if ((empty($cm->visible) AND
        !has_capability('moodle/course:viewhiddenactivities', $context)) AND
        $course->id != SITEID) {
    notice(get_string("activityiscurrentlyhidden"));
}

//ishidden check.
//feedback2 on mainsite
if ((empty($cm->visible) AND
        !has_capability('moodle/course:viewhiddenactivities', $context)) AND
        $courseid == SITEID) {
    notice(get_string("activityiscurrentlyhidden"));
}

//check, if the feedback2 is open (timeopen, timeclose)
$checktime = time();
$feedback2_is_closed = ($feedback2->timeopen > $checktime) ||
                      ($feedback2->timeclose < $checktime &&
                            $feedback2->timeclose > 0);

if ($feedback2_is_closed) {
    echo $OUTPUT->heading(format_string($feedback2->name));
    echo $OUTPUT->box_start('generalbox boxaligncenter');
    echo $OUTPUT->notification(get_string('feedback2_is_not_open', 'feedback2'));
    echo $OUTPUT->continue_button($CFG->wwwroot.'/course/view.php?id='.$course->id);
    echo $OUTPUT->box_end();
    echo $OUTPUT->footer();
    exit;
}

//additional check for multiple-submit (prevent browsers back-button).
//the main-check is in view.php
$feedback2_can_submit = true;
if ($feedback2->multiple_submit == 0 ) {
    if (feedback2_is_already_submitted($feedback2->id, $courseid)) {
        $feedback2_can_submit = false;
    }
}
if ($feedback2_can_submit) {
    //preserving the items
    if ($preservevalues == 1) {
        if (!isset($SESSION->feedback2->is_started) OR !$SESSION->feedback2->is_started == true) {
            print_error('error', '', $CFG->wwwroot.'/course/view.php?id='.$course->id);
        }
        // Check if all required items have a value.
        if (feedback2_check_values($startitempos, $lastitempos)) {
            $userid = $USER->id; //arb
            if ($completedid = feedback2_save_values($USER->id, true)) {
                if (!$gonextpage AND !$gopreviouspage) {
                    $preservevalues = false;// It can be stored.
                }

            } else {
                $savereturn = 'failed';
                if (isset($lastpage)) {
                    $gopage = $lastpage;
                } else {
                    print_error('missingparameter');
                }
            }
        } else {
            $savereturn = 'missing';
            $highlightrequired = true;
            if (isset($lastpage)) {
                $gopage = $lastpage;
            } else {
                print_error('missingparameter');
            }

        }
    }

    //saving the items
    if ($savevalues AND !$preservevalues) {
        //exists there any pagebreak, so there are values in the feedback2_valuetmp
        $userid = $USER->id; //arb

        //add a grade if required
        if (!empty($feedback2->grade)) {
            // Add feedback2 grade
            $grade = new stdClass;
            $grade->userid = $userid;
            $grade->rawgrade = 100;
            grade_update('mod/feedback2', $feedback2->course, 'mod', 'feedback2', $feedback2->id, 0, $grade);
        }

        if ($feedback2->anonymous == FEEDBACK_ANONYMOUS_NO) {
            $feedback2completed = feedback2_get_current_completed($feedback2->id, false, $courseid);
        } else {
            $feedback2completed = false;
        }
        $params = array('id' => $completedid);
        $feedback2completedtmp = $DB->get_record('feedback2_completedtmp', $params);
        //fake saving for switchrole
        $is_switchrole = feedback2_check_is_switchrole();
        if ($is_switchrole) {
            $savereturn = 'saved';
            feedback2_delete_completedtmp($completedid);
        } else {
            $new_completed_id = feedback2_save_tmp_values($feedback2completedtmp,
                                                         $feedback2completed,
                                                         $userid);
            if ($new_completed_id) {
                $savereturn = 'saved';
                if ($feedback2->anonymous == FEEDBACK_ANONYMOUS_NO) {
                    feedback2_send_email($cm, $feedback2, $course, $userid);
                } else {
                    feedback2_send_email_anonym($cm, $feedback2, $course, $userid);
                }
                //tracking the submit
                $tracking = new stdClass();
                $tracking->userid = $USER->id;
                $tracking->feedback2 = $feedback2->id;
                $tracking->completed = $new_completed_id;
                $DB->insert_record('feedback2_tracking', $tracking);
                unset($SESSION->feedback2->is_started);

                // Update completion state
                $completion = new completion_info($course);
                if ($completion->is_enabled($cm) && $feedback2->completionsubmit) {
                    $completion->update_state($cm, COMPLETION_COMPLETE);
                }

            } else {
                $savereturn = 'failed';
            }
        }

    }


    if ($allbreaks = feedback2_get_all_break_positions($feedback2->id)) {
        if ($gopage <= 0) {
            $startposition = 0;
        } else {
            if (!isset($allbreaks[$gopage - 1])) {
                $gopage = count($allbreaks);
            }
            $startposition = $allbreaks[$gopage - 1];
        }
        $ispagebreak = true;
    } else {
        $startposition = 0;
        $newpage = 0;
        $ispagebreak = false;
    }

    //get the feedback2items after the last shown pagebreak
    $select = 'feedback2 = ? AND position > ?';
    $params = array($feedback2->id, $startposition);
    $feedback2items = $DB->get_records_select('feedback2_item', $select, $params, 'position');

    //get the first pagebreak
    $params = array('feedback2' => $feedback2->id, 'typ' => 'pagebreak');
    if ($pagebreaks = $DB->get_records('feedback2_item', $params, 'position')) {
        $pagebreaks = array_values($pagebreaks);
        $firstpagebreak = $pagebreaks[0];
    } else {
        $firstpagebreak = false;
    }
    $maxitemcount = $DB->count_records('feedback2_item', array('feedback2'=>$feedback2->id));

    //get the values of completeds before done. Anonymous user can not get these values.
    if ((!isset($SESSION->feedback2->is_started)) AND
                          (!isset($savereturn)) AND
                          ($feedback2->anonymous == FEEDBACK_ANONYMOUS_NO)) {

        $feedback2completedtmp = feedback2_get_current_completed($feedback2->id, true, $courseid);
        if (!$feedback2completedtmp) {
            $feedback2completed = feedback2_get_current_completed($feedback2->id, false, $courseid);
            if ($feedback2completed) {
                //copy the values to feedback2_valuetmp create a completedtmp
                $feedback2completedtmp = feedback2_set_tmp_values($feedback2completed);
            }
        }
    } else {
        $feedback2completedtmp = feedback2_get_current_completed($feedback2->id, true, $courseid);
    }

    /// Print the main part of the page
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    $analysisurl = new moodle_url('/mod/feedback2/analysis.php', array('id'=>$id));
    if ($courseid > 0) {
        $analysisurl->param('courseid', $courseid);
    }
    echo $OUTPUT->heading(format_string($feedback2->name));

    if ( (intval($feedback2->publish_stats) == 1) AND
            ( has_capability('mod/feedback2:viewanalysepage', $context)) AND
            !( has_capability('mod/feedback2:viewreports', $context)) ) {

        $params = array('userid' => $USER->id, 'feedback2' => $feedback2->id);
        if ($multiple_count = $DB->count_records('feedback2_tracking', $params)) {
            echo $OUTPUT->box_start('mdl-align');
            echo '<a href="'.$analysisurl->out().'">';
            echo get_string('completed_feedback2s', 'feedback2').'</a>';
            echo $OUTPUT->box_end();
        }
    }

    if (isset($savereturn) && $savereturn == 'saved') {
        if ($feedback2->page_after_submit) {

            require_once($CFG->libdir . '/filelib.php');

            $page_after_submit_output = file_rewrite_pluginfile_urls($feedback2->page_after_submit,
                                                                    'pluginfile.php',
                                                                    $context->id,
                                                                    'mod_feedback2',
                                                                    'page_after_submit',
                                                                    0);

            echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');
            echo format_text($page_after_submit_output,
                             $feedback2->page_after_submitformat,
                             array('overflowdiv' => true));
            echo $OUTPUT->box_end();
        } else {
            echo '<p align="center">';
            echo '<b><font color="green">';
            echo get_string('entries_saved', 'feedback2');
            echo '</font></b>';
            echo '</p>';
            if ( intval($feedback2->publish_stats) == 1) {
                echo '<p align="center"><a href="'.$analysisurl->out().'">';
                echo get_string('completed_feedback2s', 'feedback2').'</a>';
                echo '</p>';
            }
        }

        if ($feedback2->site_after_submit) {
            $url = feedback2_encode_target_url($feedback2->site_after_submit);
        } else {
            if ($courseid) {
                if ($courseid == SITEID) {
                    $url = $CFG->wwwroot;
                } else {
                    $url = $CFG->wwwroot.'/course/view.php?id='.$courseid;
                }
            } else {
                if ($course->id == SITEID) {
                    $url = $CFG->wwwroot;
                } else {
                    $url = $CFG->wwwroot.'/course/view.php?id='.$course->id;
                }
            }
        }
        echo $OUTPUT->continue_button($url);
    } else {
        if (isset($savereturn) && $savereturn == 'failed') {
            echo $OUTPUT->box_start('mform');
            echo '<span class="error">'.get_string('saving_failed', 'feedback2').'</span>';
            echo $OUTPUT->box_end();
        }

        if (isset($savereturn) && $savereturn == 'missing') {
            echo $OUTPUT->box_start('mform');
            echo '<span class="error">'.get_string('saving_failed_because_missing_or_false_values', 'feedback2').'</span>';
            echo $OUTPUT->box_end();
        }

        //print the items
        if (is_array($feedback2items)) {
            echo $OUTPUT->box_start('feedback2_form');
            echo '<form action="complete.php" class="mform" method="post" onsubmit=" ">';
            echo '<input type="hidden" name="sesskey" value="'.sesskey().'" />';
            echo $OUTPUT->box_start('feedback2_anonymousinfo');
            switch ($feedback2->anonymous) {
                case FEEDBACK_ANONYMOUS_YES:
                    echo '<input type="hidden" name="anonymous" value="1" />';
                    $inputvalue = 'value="'.FEEDBACK_ANONYMOUS_YES.'"';
                    echo '<input type="hidden" name="anonymous_response" '.$inputvalue.' />';
                    echo get_string('mode', 'feedback2').': '.get_string('anonymous', 'feedback2');
                    break;
                case FEEDBACK_ANONYMOUS_NO:
                    echo '<input type="hidden" name="anonymous" value="0" />';
                    $inputvalue = 'value="'.FEEDBACK_ANONYMOUS_NO.'"';
                    echo '<input type="hidden" name="anonymous_response" '.$inputvalue.' />';
                    echo get_string('mode', 'feedback2').': ';
                    echo get_string('non_anonymous', 'feedback2');
                    break;
            }
            echo $OUTPUT->box_end();
            //check, if there exists required-elements
            $params = array('feedback2' => $feedback2->id, 'required' => 1);
            $countreq = $DB->count_records('feedback2_item', $params);
            if ($countreq > 0) {
                echo '<span class="fdescription required">';
                echo get_string('somefieldsrequired', 'form', '<img alt="'.get_string('requiredelement', 'form').
                    '" src="'.$OUTPUT->pix_url('req') .'" class="req" />');
                echo '</span>';
            }
            echo $OUTPUT->box_start('feedback2_items');

            unset($startitem);
            $select = 'feedback2 = ? AND hasvalue = 1 AND position < ?';
            $params = array($feedback2->id, $startposition);
            $itemnr = $DB->count_records_select('feedback2_item', $select, $params);
            $lastbreakposition = 0;
            $align = right_to_left() ? 'right' : 'left';

            $parent_ids = $parent_values = array();
            
            foreach ($feedback2items as $feedback2item) {
                if (!isset($startitem)) {
                    //avoid showing double pagebreaks
                    if ($feedback2item->typ == 'pagebreak') {
                        continue;
                    }
                    $startitem = $feedback2item;
                }

				$dependstyle = '';
                if ($feedback2item->dependitem > 0) {
				    $dependstyle = ' item_' .$feedback2item->id. ' parent_' .$feedback2item->dependitem. ' feedback2_complete_depend'. $dependstyleadd;
				    $parent_ids[] = 'parent_ids[' .$feedback2item->id. "] = ". $feedback2item->dependitem .";";
				    $parent_values[] = 'parent_values[' .$feedback2item->id. "] = '". $feedback2item->dependvalue ."';";
                }

                echo $OUTPUT->box_start('feedback2_item_box_'.$align.$dependstyle);
                $value = '';
                //get the value
                $frmvaluename = $feedback2item->typ . '_'. $feedback2item->id;
                if (isset($savereturn)) {
                    $value = isset($formdata->{$frmvaluename}) ? $formdata->{$frmvaluename} : null;
                    $value = feedback2_clean_input_value($feedback2item, $value);
                } else {
                    if (isset($feedback2completedtmp->id)) {
                        $value = feedback2_get_item_value($feedback2completedtmp->id,
                                                         $feedback2item->id,
                                                         true);
                    }
                }
                if ($feedback2item->hasvalue == 1 && $feedback2->autonumbering) {
                    $itemnr++;
                    echo $OUTPUT->box_start('feedback2_item_number_'.$align);
                    echo $itemnr;
                    echo $OUTPUT->box_end();
                }
                if ($feedback2item->typ != 'pagebreak') {
                    echo $OUTPUT->box_start('box generalbox boxalign_'.$align);
                    feedback2_print_item_complete($feedback2item, $value, $highlightrequired);
                    echo $OUTPUT->box_end();
                }

                echo $OUTPUT->box_end();

                $lastbreakposition = $feedback2item->position; //last item-pos (item or pagebreak)
                if ($feedback2item->typ == 'pagebreak') {
                    break;
                } else {
                    $lastitem = $feedback2item;
                }
            }
			
            if (!empty($parent_ids) || !empty($parent_values)) {
                // init JS variables for dependable items
                echo '<script type="text/javascript">';
                echo 'parent_ids = [];';
                echo 'parent_values = [];';
                echo implode("\n", $parent_ids);
                echo implode("\n", $parent_values);
                echo '</script>';
            }
            echo $OUTPUT->box_end();
            echo '<input type="hidden" name="id" value="'.$id.'" />';
            echo '<input type="hidden" name="feedback2id" value="'.$feedback2->id.'" />';
            echo '<input type="hidden" name="lastpage" value="'.$gopage.'" />';
            if (isset($feedback2completedtmp->id)) {
                $inputvalue = 'value="'.$feedback2completedtmp->id.'"';
            } else {
                $inputvalue = 'value=""';
            }
            echo '<input type="hidden" name="completedid" '.$inputvalue.' />';
            echo '<input type="hidden" name="courseid" value="'. $courseid . '" />';
            echo '<input type="hidden" name="preservevalues" value="1" />';
            if (isset($startitem)) {
                echo '<input type="hidden" name="startitempos" value="'.$startitem->position.'" />';
                echo '<input type="hidden" name="lastitempos" value="'.$lastitem->position.'" />';
            }

            if ( $ispagebreak AND $lastbreakposition > $firstpagebreak->position) {
                $inputvalue = 'value="'.get_string('previous_page', 'feedback2').'"';
                echo '<input name="gopreviouspage" type="submit" '.$inputvalue.' />';
            }
            if ($lastbreakposition < $maxitemcount) {
                $inputvalue = 'value="'.get_string('next_page', 'feedback2').'"';
                echo '<input name="gonextpage" type="submit" '.$inputvalue.' />';
            }
            if ($lastbreakposition >= $maxitemcount) { //last page
                $inputvalue = 'value="'.get_string('save_entries', 'feedback2').'"';
                echo '<input name="savevalues" type="submit" '.$inputvalue.' />';
            }

            echo '</form>';
            echo $OUTPUT->box_end();

            echo $OUTPUT->box_start('feedback2_complete_cancel');
            if ($courseid) {
                $action = 'action="'.$CFG->wwwroot.'/course/view.php?id='.$courseid.'"';
            } else {
                if ($course->id == SITEID) {
                    $action = 'action="'.$CFG->wwwroot.'"';
                } else {
                    $action = 'action="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'"';
                }
            }
            echo '<form '.$action.' method="post" onsubmit=" ">';
            echo '<input type="hidden" name="sesskey" value="'.sesskey().'" />';
            echo '<input type="hidden" name="courseid" value="'. $courseid . '" />';
            echo '<button type="submit">'.get_string('cancel').'</button>';
            echo '</form>';
            echo $OUTPUT->box_end();
            $SESSION->feedback2->is_started = true;
        }
    }
} else {
    echo $OUTPUT->heading(format_string($feedback2->name));
    echo $OUTPUT->box_start('generalbox boxaligncenter');
    echo $OUTPUT->notification(get_string('this_feedback2_is_already_submitted', 'feedback2'));
    echo $OUTPUT->continue_button($CFG->wwwroot.'/course/view.php?id='.$course->id);
    echo $OUTPUT->box_end();
}
/// Finish the page
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

echo $OUTPUT->footer();
