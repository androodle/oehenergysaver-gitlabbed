<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

function xmldb_feedback2_install() {
    global $DB;
    
    // Copy all mod_feedback activities and switch them to mod_feedback2.
    
    $sql = "INSERT INTO {feedback2} SELECT * FROM {feedback}";
    $DB->execute($sql);
    
    $sql = "INSERT INTO {feedback2_template} SELECT * FROM {feedback_template}";
    $DB->execute($sql);
    
    $sql = "INSERT INTO {feedback2_item} SELECT * FROM {feedback_item}";
    $DB->execute($sql);
    
    $sql = "INSERT INTO {feedback2_completed} SELECT * FROM {feedback_completed}";
    $DB->execute($sql);
    
    $sql = "INSERT INTO {feedback2_completed_history} SELECT * FROM {feedback_completed_history}";
    $DB->execute($sql);
    
    $sql = "INSERT INTO {feedback2_completedtmp} SELECT * FROM {feedback_completedtmp}";
    $DB->execute($sql);
    
    $sql = "INSERT INTO {feedback2_value} SELECT * FROM {feedback_value}";
    $DB->execute($sql);
    
    $sql = "INSERT INTO {feedback2_value_history} SELECT * FROM {feedback_value_history}";
    $DB->execute($sql);
    
    $sql = "INSERT INTO {feedback2_valuetmp} SELECT * FROM {feedback_valuetmp}";
    $DB->execute($sql);
    
    $sql = "INSERT INTO {feedback2_tracking} SELECT * FROM {feedback_tracking}";
    $DB->execute($sql);
    
    $sql = "INSERT INTO {feedback2_sitecourse_map} SELECT * FROM {feedback_sitecourse_map}";
    $DB->execute($sql);
    
    $moduleid = $DB->get_field('modules', 'id', array('name' => 'feedback'));
    $moduleid2 = $DB->get_field('modules', 'id', array('name' => 'feedback2'));
    $sql = "UPDATE {course_modules} SET module = ? WHERE module = ?";
    $DB->execute($sql, array($moduleid2, $moduleid));
}
