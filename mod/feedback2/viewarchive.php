<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2010 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mod
 * @subpackage feedback2
 * @author     Russell England <russell.england@catalyst-eu.net>
 * @copyright  Catalyst IT Ltd 2013 <http://catalyst-eu.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 *
 */

require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/mod/feedback2/lib.php');
require_once($CFG->dirroot . '/mod/feedback2/viewarchive_form.php');
require_once($CFG->dirroot . '/course/lib.php');

$filters['feedback2id'] = required_param('feedback2id', PARAM_INT); // Feedback ID
$filters['page'] = optional_param('page', 0, PARAM_INT);
$filters['perpage'] = optional_param('perpage', 20, PARAM_INT);
$filters['historyid'] = optional_param('historyid', null, PARAM_INT);    // feedback2_completed_history id
$filters['username'] = optional_param('username', null, PARAM_TEXT);
$filters['lastname'] = optional_param('lastname', null, PARAM_TEXT);
$filters['firstname'] = optional_param('firstname', null, PARAM_TEXT);

if (!$feedback2 = $DB->get_record('feedback2', array('id' => $filters['feedback2id']))) {
    print_error(get_string('error:feedback2notfound', 'feedback2', $filters['feedback2id']));
}
$filters['courseid'] = $feedback2->course;

if (!$course = $DB->get_record('course', array('id' => $feedback2->course))) {
    print_error("coursemisconf");
}

if (!$cm = get_coursemodule_from_instance('feedback2', $feedback2->id, $course->id)) {
    print_error("invalidcoursemodule");
}

require_login($course, false, $cm);

$context = context_module::instance($cm->id);

if (!empty($filters['historyid']) && (!$DB->record_exists('feedback2_completed_history', array('id' => $filters['historyid'])))) {
    print_error(get_string('error:completedhistorynotfound', 'feedback2', $filters['historyid']));
}

require_capability('mod/feedback2:viewarchive', $context);

$heading = get_string('viewarchive', 'feedback2');
$PAGE->set_context($context);
$PAGE->set_heading(format_string($heading));
$PAGE->set_title(format_string($heading));
$PAGE->set_url('/mod/feedback2/viewarchive.php', $filters);

echo $OUTPUT->header();
echo $OUTPUT->heading($heading);

if (!empty($filters['historyid'])) {
    // Display feedback2 answers

    $sql = 'SELECT c.id as completedid,
                    c.timemodified,
                    f.id as feedback2id,
                    f.name AS feedback2name,
                    f.autonumbering,
                    u.firstname,
                    u.lastname,
                    course.shortname coursename
            FROM {feedback2_completed_history} c
            INNER JOIN {feedback2} f ON f.id = c.feedback2
            INNER JOIN {user} u ON u.id = c.userid
            INNER JOIN {course} course ON course.id = f.course
            WHERE c.id = :completedid';
    if (!$feedback2 = $DB->get_record_sql($sql, array('completedid' => $filters['historyid']))) {
        echo $OUTPUT->notify(get_string('error:completedhistorynotfound', 'feedback2', $filters['historyid']));
    } else {
        echo $OUTPUT->heading(format_text($feedback2->coursename . ' : ' . $feedback2->feedback2name));

        $sql = 'SELECT i.*, v.value
                FROM {feedback2_item} i
                LEFT JOIN {feedback2_value_history} v ON v.item = i.id AND v.completed = :completedid
                WHERE i.feedback2 = :feedback2id
                AND i.typ <> :typ
                ORDER BY i.position';
        if ($items = $DB->get_records_sql($sql, array('completedid' => $filters['historyid'], 'feedback2id' => $feedback2->feedback2id, 'typ' => 'pagebreak'))) {
            $align = right_to_left() ? 'right' : 'left';

            echo $OUTPUT->heading(userdate($feedback2->timemodified).' ('.fullname($feedback2).')', 3);

            echo $OUTPUT->box_start('feedback2_items');
            $count = 0;
            foreach ($items as $item) {
                echo $OUTPUT->box_start();
                if ($feedback2->autonumbering && $item->hasvalue) {
                    echo $OUTPUT->box_start();
                    echo ++$count;
                    echo $OUTPUT->box_end();
                }

                echo $OUTPUT->box_start('box generalbox');
                feedback2_print_item_show_value($item, $item->value);
                echo $OUTPUT->box_end();
                echo $OUTPUT->box_end();
            }
            echo $OUTPUT->box_end();
        }
    }
} else {

    // Display list of archived feedback2

    $mform = new view_archive_form();

    if ($formdata = $mform->get_data()) {
        // New filters so reset the page number
        $filters['page'] = 0;
        $filters['feedback2id'] = $formdata->feedback2id;
        $filters['username'] = $formdata->username;
        $filters['lastname'] = $formdata->lastname;
        $filters['firstname'] = $formdata->firstname;
    } else {
        $formdata = new stdClass();
        $formdata->feedback2id = $filters['feedback2id'];
        $formdata->username = $filters['username'];
        $formdata->lastname = $filters['lastname'];
        $formdata->firstname = $filters['firstname'];
    }

    $mform->set_data($formdata);
    $mform->display();

    $totalcount = 0;
    $archives = feedback2_archive_get_list($filters, $totalcount);
    $filters['totalcount'] = $totalcount;

    echo feedback2_archive_display_list($archives, $filters);
}
echo $OUTPUT->footer();
