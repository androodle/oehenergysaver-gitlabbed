<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * the first page to view the feedback2
 *
 * @author Andreas Grabs
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package feedback2
 */
require_once("../../config.php");
require_once("lib.php");

$id = required_param('id', PARAM_INT);
$courseid = optional_param('courseid', false, PARAM_INT);

$current_tab = 'view';

if (! $cm = get_coursemodule_from_id('feedback2', $id)) {
    print_error('invalidcoursemodule');
}

if (! $course = $DB->get_record("course", array("id"=>$cm->course))) {
    print_error('coursemisconf');
}

if (! $feedback2 = $DB->get_record("feedback2", array("id"=>$cm->instance))) {
    print_error('invalidcoursemodule');
}

$context = context_module::instance($cm->id);

$feedback2_complete_cap = false;

if (has_capability('mod/feedback2:complete', $context)) {
    $feedback2_complete_cap = true;
}

if (isset($CFG->feedback2_allowfullanonymous)
            AND $CFG->feedback2_allowfullanonymous
            AND $course->id == SITEID
            AND (!$courseid OR $courseid == SITEID)
            AND $feedback2->anonymous == FEEDBACK_ANONYMOUS_YES ) {
    $feedback2_complete_cap = true;
}

//check whether the feedback2 is located and! started from the mainsite
if ($course->id == SITEID AND !$courseid) {
    $courseid = SITEID;
}

//check whether the feedback2 is mapped to the given courseid
if ($course->id == SITEID AND !has_capability('mod/feedback2:edititems', $context)) {
    if ($DB->get_records('feedback2_sitecourse_map', array('feedback2id'=>$feedback2->id))) {
        $params = array('feedback2id'=>$feedback2->id, 'courseid'=>$courseid);
        if (!$DB->get_record('feedback2_sitecourse_map', $params)) {
            print_error('invalidcoursemodule');
        }
    }
}

if ($feedback2->anonymous != FEEDBACK_ANONYMOUS_YES) {
    if ($course->id == SITEID) {
        require_login($course, true);
    } else {
        require_login($course, true, $cm);
    }
} else {
    if ($course->id == SITEID) {
        require_course_login($course, true);
    } else {
        require_course_login($course, true, $cm);
    }
}

//check whether the given courseid exists
if ($courseid AND $courseid != SITEID) {
    if ($course2 = $DB->get_record('course', array('id'=>$courseid))) {
        require_course_login($course2); //this overwrites the object $course :-(
        $course = $DB->get_record("course", array("id"=>$cm->course)); // the workaround
    } else {
        print_error('invalidcourseid');
    }
}

// Trigger module viewed event.
$event = \mod_feedback2\event\course_module_viewed::create(array(
    'objectid' => $feedback2->id,
    'context' => $context,
    'other' => array(
        'cmid' => $cm->id,
        'instanceid' => $feedback2->id,
        'anonymous' => $feedback2->anonymous,
        'content' => 'feedback2moduleview'
        )
    ));
$event->add_record_snapshot('course_modules', $cm);
$event->add_record_snapshot('course', $course);
$event->add_record_snapshot('feedback2', $feedback2);
$event->trigger();

/// Print the page header
$strfeedback2s = get_string("modulenameplural", "feedback2");
$strfeedback2  = get_string("modulename", "feedback2");

if ($course->id == SITEID) {
    $PAGE->set_context($context);
    $PAGE->set_cm($cm, $course); // set's up global $COURSE
    $PAGE->set_pagelayout('incourse');
}
$PAGE->set_url('/mod/feedback2/view.php', array('id'=>$cm->id, 'do_show'=>'view'));
$PAGE->set_title($feedback2->name);
$PAGE->set_heading($course->fullname);
echo $OUTPUT->header();

//ishidden check.
//feedback2 in courses
$cap_viewhiddenactivities = has_capability('moodle/course:viewhiddenactivities', $context);
if ((empty($cm->visible) and !$cap_viewhiddenactivities) AND $course->id != SITEID) {
    notice(get_string("activityiscurrentlyhidden"));
}

//ishidden check.
//feedback2 on mainsite
if ((empty($cm->visible) and !$cap_viewhiddenactivities) AND $courseid == SITEID) {
    notice(get_string("activityiscurrentlyhidden"));
}

/// Print the main part of the page
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

$previewimg = $OUTPUT->pix_icon('t/preview', get_string('preview'));
$previewlnk = new moodle_url('/mod/feedback2/print.php', array('id' => $id));
$preview = html_writer::link($previewlnk, $previewimg);

echo $OUTPUT->heading(format_string($feedback2->name) . $preview);

// Print the tabs.
require('tabs.php');

//show some infos to the feedback2
if (has_capability('mod/feedback2:edititems', $context)) {
    //get the groupid
    $groupselect = groups_print_activity_menu($cm, $CFG->wwwroot.'/mod/feedback2/view.php?id='.$cm->id, true);
    $mygroupid = groups_get_activity_group($cm);

    echo $OUTPUT->box_start('boxaligncenter boxwidthwide');
    echo $groupselect.'<div class="clearer">&nbsp;</div>';
    $completedscount = feedback2_get_completeds_group_count($feedback2, $mygroupid);
    echo $OUTPUT->box_start('feedback2_info');
    echo '<span class="feedback2_info">';
    echo get_string('completed_feedback2s', 'feedback2').': ';
    echo '</span>';
    echo '<span class="feedback2_info_value">';
    echo $completedscount;
    echo '</span>';
    echo $OUTPUT->box_end();

    $params = array('feedback2'=>$feedback2->id, 'hasvalue'=>1);
    $itemscount = $DB->count_records('feedback2_item', $params);
    echo $OUTPUT->box_start('feedback2_info');
    echo '<span class="feedback2_info">';
    echo get_string('questions', 'feedback2').': ';
    echo '</span>';
    echo '<span class="feedback2_info_value">';
    echo $itemscount;
    echo '</span>';
    echo $OUTPUT->box_end();

    if ($feedback2->timeopen) {
        echo $OUTPUT->box_start('feedback2_info');
        echo '<span class="feedback2_info">';
        echo get_string('feedback2open', 'feedback2').': ';
        echo '</span>';
        echo '<span class="feedback2_info_value">';
        echo userdate($feedback2->timeopen);
        echo '</span>';
        echo $OUTPUT->box_end();
    }
    if ($feedback2->timeclose) {
        echo $OUTPUT->box_start('feedback2_info');
        echo '<span class="feedback2_info">';
        echo get_string('feedback2close', 'feedback2').': ';
        echo '</span>';
        echo '<span class="feedback2_info_value">';
        echo userdate($feedback2->timeclose);
        echo '</span>';
        echo $OUTPUT->box_end();
    }
    echo $OUTPUT->box_end();
}

if (has_capability('mod/feedback2:edititems', $context)) {
    echo $OUTPUT->heading(get_string('description', 'feedback2'), 3);
}
echo $OUTPUT->box_start('generalbox boxwidthwide');
$options = (object)array('noclean'=>true);
echo format_module_intro('feedback2', $feedback2, $cm->id);
echo $OUTPUT->box_end();

if (has_capability('mod/feedback2:edititems', $context)) {
    require_once($CFG->libdir . '/filelib.php');

    $page_after_submit_output = file_rewrite_pluginfile_urls($feedback2->page_after_submit,
                                                            'pluginfile.php',
                                                            $context->id,
                                                            'mod_feedback2',
                                                            'page_after_submit',
                                                            0);

    echo $OUTPUT->heading(get_string("page_after_submit", "feedback2"), 3);
    echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');
    echo format_text($page_after_submit_output,
                     $feedback2->page_after_submitformat,
                     array('overflowdiv'=>true));

    echo $OUTPUT->box_end();
}

if ( (intval($feedback2->publish_stats) == 1) AND
                ( has_capability('mod/feedback2:viewanalysepage', $context)) AND
                !( has_capability('mod/feedback2:viewreports', $context)) ) {

    $params = array('userid'=>$USER->id, 'feedback2'=>$feedback2->id);
    if ($multiple_count = $DB->count_records('feedback2_tracking', $params)) {
        $url_params = array('id'=>$id, 'courseid'=>$courseid);
        $analysisurl = new moodle_url('/mod/feedback2/analysis.php', $url_params);
        echo '<div class="mdl-align"><a href="'.$analysisurl->out().'">';
        echo get_string('completed_feedback2s', 'feedback2').'</a>';
        echo '</div>';
    }
}

//####### mapcourse-start
if (has_capability('mod/feedback2:mapcourse', $context)) {
    if ($feedback2->course == SITEID) {
        echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');
        echo '<div class="mdl-align">';
        echo '<form action="mapcourse.php" method="get">';
        echo '<fieldset>';
        echo '<input type="hidden" name="sesskey" value="'.sesskey().'" />';
        echo '<input type="hidden" name="id" value="'.$id.'" />';
        echo '<button type="submit">'.get_string('mapcourses', 'feedback2').'</button>';
        echo $OUTPUT->help_icon('mapcourse', 'feedback2');
        echo '</fieldset>';
        echo '</form>';
        echo '<br />';
        echo '</div>';
        echo $OUTPUT->box_end();
    }
}
//####### mapcourse-end

//####### completed-start
if ($feedback2_complete_cap) {
    echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');
    //check, whether the feedback2 is open (timeopen, timeclose)
    $checktime = time();
    if (($feedback2->timeopen > $checktime) OR
            ($feedback2->timeclose < $checktime AND $feedback2->timeclose > 0)) {

        echo $OUTPUT->notification(get_string('feedback2_is_not_open', 'feedback2'));
        echo $OUTPUT->continue_button($CFG->wwwroot.'/course/view.php?id='.$course->id);
        echo $OUTPUT->box_end();
        echo $OUTPUT->footer();
        exit;
    }

    //check multiple Submit
    $feedback2_can_submit = true;
    if ($feedback2->multiple_submit == 0 ) {
        if (feedback2_is_already_submitted($feedback2->id, $courseid)) {
            $feedback2_can_submit = false;
        }
    }
    if ($feedback2_can_submit) {
        //if the user is not known so we cannot save the values temporarly
        if (!isloggedin() or isguestuser()) {
            $completefile = 'complete_guest.php';
            $guestid = sesskey();
        } else {
            $completefile = 'complete.php';
            $guestid = false;
        }
        $url_params = array('id'=>$id, 'courseid'=>$courseid, 'gopage'=>0);
        $completeurl = new moodle_url('/mod/feedback2/'.$completefile, $url_params);

        $feedback2completedtmp = feedback2_get_current_completed($feedback2->id, true, $courseid, $guestid);
        if ($feedback2completedtmp) {
            if ($startpage = feedback2_get_page_to_continue($feedback2->id, $courseid, $guestid)) {
                $completeurl->param('gopage', $startpage);
            }
            echo '<a href="'.$completeurl->out().'">'.get_string('continue_the_form', 'feedback2').'</a>';
        } else {
            echo '<a href="'.$completeurl->out().'">'.get_string('complete_the_form', 'feedback2').'</a>';
        }
    } else {
        echo $OUTPUT->notification(get_string('this_feedback2_is_already_submitted', 'feedback2'));
        if ($courseid) {
            echo $OUTPUT->continue_button($CFG->wwwroot.'/course/view.php?id='.$courseid);
        } else {
            echo $OUTPUT->continue_button($CFG->wwwroot.'/course/view.php?id='.$course->id);
        }
    }
    echo $OUTPUT->box_end();
}
//####### completed-end

/// Finish the page
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

echo $OUTPUT->footer();

