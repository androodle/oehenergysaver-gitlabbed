<?php

$string['choosereadme'] = '
<div class="clearfix">
        <div class="theme_screenshot">
                <h2>OEH - Energy Saver (Business Case)</h2>
                <img src="oehes_businesscase/pix/screenshot.jpg" />
                <h3>Theme Credits</h3>
                <p>Created by OEH - Energy Saver (Business Case)</p>
                <h3>Report a bug:</h3>
                <p><a href="https://androgogic.livetime.com/" target="_blank">https://androgogic.livetime.com/</a></p>
        </div>
        <div class="theme_description">
                <h2>About</h2>
                <p>OEH Energy Saver (Business Case) theme is a flexible responsive theme</p>
                <h2>Tweaks</h2>
                <p>This theme is built upon the Androtheme.</p>
        </div>
</div>
';

$string['pluginname'] = 'OEH - Energy Saver (Business Case)';