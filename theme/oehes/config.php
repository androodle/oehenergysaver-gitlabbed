<?php

$THEME->name = 'oehes';
$THEME->doctype = 'html5';
$THEME->parents = array('androtheme', 'bootstrapbase', 'standardtotararesponsive');
$THEME->sheets = array(
    'custom',
    'customhome'
);
$THEME->supportscssoptimisation = false;
$THEME->yuicssmodules = array();
$THEME->editor_sheets = array();
$THEME->plugins_exclude_sheets = array();
$THEME->layouts = array(
// The site home page (CUSTOM)
    'frontpage' => array(
        'file' => 'frontpage.php',
        'regions' => array(
            'hero-unit',
            'four-cols-left1',
            'four-cols-left2',
            'four-cols-right1',
            'four-cols-right2',
            'side-pre',
            'side-post'
        ),
        'defaultregion' => 'side-pre',
    ),
);

// special class tells Moodle to look for renderers first within the theme and then in all of the other default locations.
$THEME->rendererfactory = 'theme_overridden_renderer_factory';
$THEME->csspostprocess = 'theme_oehes_process_css';

$THEME->blockrtlmanipulations = array(
    'side-pre' => 'side-post',
    'side-post' => 'side-pre'
);

$THEME->enable_dock = true;