<div id="region-hero-unit">
    <?php echo $OUTPUT->blocks('hero-unit');?>
</div>

<div id="region-four-cols-wrap" class="clearfix">
    <div class="four-cols-header"><?php echo $OUTPUT->blocks('four-cols-head'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('four-cols-left1'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('four-cols-left2'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('four-cols-right1'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('four-cols-right2'); ?></div>
</div>