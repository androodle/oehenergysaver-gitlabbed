<?php

$THEME->name = 'oehes_refrigeration';
$THEME->doctype = 'html5';
$THEME->parents = array('oehes', 'androtheme', 'bootstrapbase', 'standardtotararesponsive');
$THEME->sheets = array();
$THEME->rendererfactory = 'theme_overridden_renderer_factory';
$THEME->enable_dock = true;