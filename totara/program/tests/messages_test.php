<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2010 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Maria Torres <maria.torres@totaralms.com>
 * @package totara_program
 */

defined('MOODLE_INTERNAL') || die();

global $CFG;

require_once($CFG->dirroot . '/totara/program/lib.php');
require_once($CFG->dirroot . '/totara/program/cron.php');

/**
 * Test events in programs.
 *
 * To test, run this from the command line from the $CFG->dirroot
 * vendor/bin/phpunit totara_program_events_testcase
 *
 */
class totara_program_messages_testcase extends advanced_testcase {

    private $user = null;

    public function setUp() {
        parent::setup();
        $this->resetAfterTest(true);
    }

    public function test_program_enrolment_messages() {
        global $DB, $CFG, $UNITTEST;

        $this->preventResetByRollback();
        $this->resetAfterTest(true);
        $this->setAdminUser();

        // Function in lib/moodlelib.php email_to_user require this.
        if (!isset($UNITTEST)) {
            $UNITTEST = new stdClass();
            $UNITTEST->running = true;
        }

        unset_config('noemailever');
        $sink = $this->redirectEmails();
        ob_start(); // Start a buffer to catch all the mtraces in the task.

        // Create a default structure for the program.
        $todb = new \stdClass();
        $todb->fullname = 'Program Fullname';
        $todb->availablefrom = 0;
        $todb->availableuntil = 0;
        $todb->timecreated = time();
        $todb->timemodified = time();
        $todb->usermodified = 2;
        $todb->shortname = 'progshort';
        $todb->idnumber = '';
        $todb->available = 1;
        $todb->sortorder = 0;
        $todb->icon = 1;
        $todb->exceptionssent = 0;
        $todb->visible = 1;
        $todb->summary = '';
        $todb->endnote = '';
        $todb->audiencevisible = 2;
        $todb->certifid = null;
        $todb->category = 1;

        // Create 8 users.
        $this->assertEquals(2, $DB->count_records('user'));
        for ($i = 1; $i <= 8; $i++) {
            $this->{'user'.$i} = $this->getDataGenerator()->create_user();
        }
        $this->assertEquals(10, $DB->count_records('user'));

        // Create two programs.
        $this->assertEquals(0, $DB->count_records('prog'));

        // Create program and message manager to add default messages.
        $newid = $DB->insert_record('prog', $todb);
        $program = new program($newid);
        new prog_messages_manager($newid, true);

        $this->assertEquals(1, $DB->count_records('prog'));

        // Make sure the mail is redirecting and the sink is clear.
        $this->assertTrue(phpunit_util::is_redirecting_phpmailer());
        $sink->clear();

        // Assign users to program.
        $usersprogram = array($this->user1->id, $this->user2->id, $this->user3->id);

        // Create a default structure for assignments.
        $data = new stdClass();
        $data->id = $program->id;
        $data->item = array(ASSIGNTYPE_INDIVIDUAL => array());
        $data->completiontime = array(ASSIGNTYPE_INDIVIDUAL => array());
        $data->completionevent = array(ASSIGNTYPE_INDIVIDUAL => array());
        $data->completioninstance = array(ASSIGNTYPE_INDIVIDUAL => array());

        $category = new individuals_category();
        foreach ($usersprogram as $userid) {
            $data->item[ASSIGNTYPE_INDIVIDUAL][$userid] = 1;
            $data->completiontime[ASSIGNTYPE_INDIVIDUAL][$userid] = -1;
            $data->completionevent[ASSIGNTYPE_INDIVIDUAL][$userid] = 0;
            $data->completioninstance[ASSIGNTYPE_INDIVIDUAL][$userid] = 0;
        }
        $category->update_assignments($data);

        $assignments = $program->get_assignments();
        $assignments->init_assignments($program->id);
        $program->update_learner_assignments(true);

        // Attempt to send any program messages.
        program_cron_enrolment_messages(array($program));

        $this->assertEquals(3, $DB->count_records('prog_user_assignment'));

        // Check the right amount of messages were caught.
        $emails = $sink->get_messages();
        $this->assertCount(3, $emails);
        $sink->clear();

        // Check that they all had logs created.
        $this->assertEquals(3, $DB->count_records('prog_messagelog'));

        // Reset the default structure for assignments.
        $data = new stdClass();
        $data->id = $program->id;
        $data->item = array(ASSIGNTYPE_INDIVIDUAL => array());
        $data->completiontime = array(ASSIGNTYPE_INDIVIDUAL => array());
        $data->completionevent = array(ASSIGNTYPE_INDIVIDUAL => array());
        $data->completioninstance = array(ASSIGNTYPE_INDIVIDUAL => array());

        // Assign more users to program and make sure only the new users get the message.
        $usersprogram = array($this->user1->id, $this->user2->id, $this->user4->id, $this->user5->id);
        $category = new individuals_category();
        foreach ($usersprogram as $userid) {
            $data->item[ASSIGNTYPE_INDIVIDUAL][$userid] = 1;
            $data->completiontime[ASSIGNTYPE_INDIVIDUAL][$userid] = -1;
            $data->completionevent[ASSIGNTYPE_INDIVIDUAL][$userid] = 0;
            $data->completioninstance[ASSIGNTYPE_INDIVIDUAL][$userid] = 0;
        }
        $category->update_assignments($data);

        $assignments = $program->get_assignments();
        $assignments->init_assignments($program->id);
        $program->update_learner_assignments(true);

        // Attempt to send any program messages.
        program_cron_enrolment_messages(array($program));

        // Check the right amount of messages were caught.
        $emails = $sink->get_messages();
        $this->assertCount(2, $emails);
        $sink->clear();

        // Check that they all had logs created.
        $this->assertEquals(5, $DB->count_records('prog_messagelog'));

        ob_end_clean(); // Throw away the buffer content.
    }
}
