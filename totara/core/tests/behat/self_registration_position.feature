@totara @totara_core
Feature: Test the ability to set your own
  position assignments on email-based self-enrolment

  Background:
    Given the following "users" exist:
      | username | firstname    | lastname | email               |
      | manager  | Frederick    | Newman   | manager@example.com |
    And I log in as "admin"
    And I navigate to "Manage organisations" node in "Site administration > Hierarchies > Organisations"
    And I press "Add new organisation framework"
    And I set the following fields to these values:
      | Name      | Organisation Framework |
      | ID Number | oframe                 |
    And I press "Save changes"
    And I click on "Organisation Framework" "link" in the ".editorganisation" "css_element"
    And I press "Add new organisation"
    And I set the following fields to these values:
      | Name      | Organisation One  |
      | Organisation ID number | org1 |
    And I press "Save changes"
    And I navigate to "Manage positions" node in "Site administration > Hierarchies > Positions"
    And I press "Add new position framework"
    And I set the following fields to these values:
      | Name      | Position Framework |
      | ID Number | pframe             |
    And I press "Save changes"
    And I click on "Position Framework" "link" in the ".editposition" "css_element"
    And I press "Add new position"
    And I set the following fields to these values:
      | Name      | Position One  |
      | Position ID number | pos1 |
    And I press "Save changes"
    And I navigate to "Email-based self-registration" node in "Site administration > Plugins > Authentication"
    And I click on "Yes" "option" in the "#menuallowsignupposition" "css_element"
    And I click on "Yes" "option" in the "#menuallowsignuporganisation" "css_element"
    And I click on "Yes" "option" in the "#menuallowsignupmanager" "css_element"
    And I press "Save changes"
    And I click on "Email-based self-registration" "option" in the "#id_s__registerauth" "css_element"
    And I press "Save changes"
    And I log out

  @javascript
  Scenario: Testing position assignment fields on email-based self-registration
    When I press "Create new account"
    Then I should see "Position"
    And I should see "Organisation"
    And I should see "Manager"

    When I set the following fields to these values:
      | Username      | gregnick             |
      | Password      | Greg_Nick01          |
      | Email address | gregnick@example.com |
      | Email (again) | gregnick@example.com |
      | First name    | Gregory              |
      | Surname       | Nickleson            |
    And I press "Choose position"
    And I click on "Position One" "link" in the "position" "totaradialogue"
    And I click on "OK" "button" in the "position" "totaradialogue"
    And I press "Choose organisation"
    And I click on "Organisation One" "link" in the "organisation" "totaradialogue"
    And I click on "OK" "button" in the "organisation" "totaradialogue"
    And I press "Choose manager"
    And I click on "Frederick Newman" "link" in the "manager" "totaradialogue"
    And I click on "OK" "button" in the "manager" "totaradialogue"
    And I press "Create my new account"
    And I press "Continue"
    And I log in as "admin"
    And I navigate to "Browse list of users" node in "Site administration > Users > Accounts"
    And I click on "Confirm" "link" in the "Gregory Nickleson" "table_row"
    And I click on "Gregory Nickleson" "link"
    And I navigate to "Primary position" node in "Profile settings for Gregory Nickleson > Positions"
    Then I should see "Position One"
    And I should see "Organisation One"
    And I should see "Frederick Newman"
