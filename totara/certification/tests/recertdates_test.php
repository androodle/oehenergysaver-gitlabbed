<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2010 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Jon Sharp <jon.sharp@catalyst-eu.net>
 * @package totara
 * @subpackage certification
 */

defined('MOODLE_INTERNAL') || die();

global $CFG;

require_once($CFG->dirroot . '/totara/certification/lib.php');

/**
 * Certification module PHPUnit archive test class
 *
 * To test, run this from the command line from the $CFG->dirroot
 * vendor/bin/phpunit --verbose totara_certification_recertdates_testcase totara/certification/tests/recertdates_test.php
 *
 * @package    totara_certifications
 * @category   phpunit
 * @group      totara_certifications
 * @author     Jon Sharp <jonathans@catalyst-eu.net>
 * @copyright  Catalyst IT Ltd 2013 <http://catalyst-eu.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class totara_certification_recertdates_testcase extends advanced_testcase {

    /**
     * Test get_certiftimebase, get_timeexpires and get_timewindowopens.
     */
    public function test_recertdates() {

        $unuseddate = strtotime('22-July-2010 02:22'); // This date can be used to ensure that a parameter is correctly ignored.
        $unusedperiod = '7 year'; // This period can be used to ensure that a parameter is correctly ignored.

        // Certification is using CERTIFRECERT_EXPIRY to calculate the next expiry date.
        // User is in the recertification stage with the window open.
        $activeperiod = '1 year';
        $windowperiod = '1 month';
        $curtimeexpires = strtotime('3-May-2013 08:14');
        $timecompleted = strtotime('15-April-2013 12:01');

        $base = get_certiftimebase(CERTIFRECERT_EXPIRY, $curtimeexpires, $timecompleted, $activeperiod, $windowperiod);
        $newtimeexpires = get_timeexpires($base, $activeperiod);
        $timewindowopens = get_timewindowopens($newtimeexpires, $windowperiod);

        $this->assertEquals(strtotime('3-May-2014 08:14'), $newtimeexpires); // One year after previous expiry date.
        $this->assertEquals(strtotime('3-Apr-2014 08:14'), $timewindowopens);

        unset($activeperiod, $windowperiod, $curtimeexpires, $timecompleted);

        // Certification is using CERTIFRECERT_EXPIRY to calculate the next expiry date.
        // User is in the recertification stage before the window is open (caused by completion upload).
        $activeperiod = '1 year';
        $windowperiod = '1 month';
        $curtimeexpires = strtotime('3-May-2013 08:14');
        $timecompleted = strtotime('15-February-2013 12:01');

        $base = get_certiftimebase(CERTIFRECERT_EXPIRY, $curtimeexpires, $timecompleted, $activeperiod, $windowperiod);
        $newtimeexpires = get_timeexpires($base, $activeperiod);
        $timewindowopens = get_timewindowopens($newtimeexpires, $windowperiod);

        $this->assertEquals(strtotime('3-May-2013 08:14'), $newtimeexpires); // Equal to current expiry date!
        $this->assertEquals(strtotime('3-Apr-2013 08:14'), $timewindowopens);

        unset($activeperiod, $windowperiod, $curtimeexpires, $timecompleted);

        // Certification is using CERTIFRECERT_EXPIRY to calculate the next expiry date.
        // User is in the primary certification stage.
        $activeperiod = '1 year';
        $windowperiod = '1 month';
        $timecompleted = strtotime('15-April-2013 12:01');

        $base = get_certiftimebase(CERTIFRECERT_EXPIRY, 0, $timecompleted, $activeperiod, $windowperiod);
        $newtimeexpires = get_timeexpires($base, $activeperiod);
        $timewindowopens = get_timewindowopens($newtimeexpires, $windowperiod);
        $this->assertEquals(strtotime('15-April-2014 12:01'), $newtimeexpires);
        $this->assertEquals(strtotime('15-March-2014 12:01'), $timewindowopens);

        unset($activeperiod, $windowperiod, $timedue, $timecompleted);

        // Certification is using CERTIFRECERT_EXPIRY to calculate the next expiry date.
        // User is in the primary certification stage, with no assignment due date set.
        $activeperiod = '1 year';
        $windowperiod = '1 month';
        $timecompleted = strtotime('15-April-2013 12:01');

        $base = get_certiftimebase(CERTIFRECERT_EXPIRY, 0, $timecompleted, $activeperiod, $windowperiod);
        $newtimeexpires = get_timeexpires($base, $activeperiod);
        $timewindowopens = get_timewindowopens($newtimeexpires, $windowperiod);

        $this->assertEquals(strtotime('15-April-2014 12:01'), $newtimeexpires); // One year after date completed.
        $this->assertEquals(strtotime('15-March-2014 12:01'), $timewindowopens);

        unset($activeperiod, $windowperiod, $timecompleted);

        // Certification is using CERTIFRECERT_COMPLETION to calculate the next expiry date.
        // User is in the recertification stage.
        $activeperiod = '1 year';
        $windowperiod = '1 month';
        $timecompleted = strtotime('15-April-2013 12:01');

        $base = get_certiftimebase(CERTIFRECERT_COMPLETION, $unuseddate, $timecompleted, $activeperiod, $windowperiod);
        $newtimeexpires = get_timeexpires($base, $activeperiod);
        $timewindowopens = get_timewindowopens($newtimeexpires, $windowperiod);

        $this->assertEquals(strtotime('15-April-2014 12:01'), $newtimeexpires); // One year after the completion date.
        $this->assertEquals(strtotime('15-March-2014 12:01'), $timewindowopens);

        unset($activeperiod, $windowperiod, $timecompleted);

        // Certification is using CERTIFRECERT_COMPLETION to calculate the next expiry date.
        // User is in the primary certification stage, with an assignment due date set.
        $activeperiod = '1 year';
        $windowperiod = '1 month';
        $timecompleted = strtotime('15-April-2013 12:01');

        $base = get_certiftimebase(CERTIFRECERT_COMPLETION, 0, $timecompleted, $activeperiod, $windowperiod);
        $newtimeexpires = get_timeexpires($base, $activeperiod);
        $timewindowopens = get_timewindowopens($newtimeexpires, $windowperiod);

        $this->assertEquals(strtotime('15-April-2014 12:01'), $newtimeexpires); // One year after the completion date.
        $this->assertEquals(strtotime('15-March-2014 12:01'), $timewindowopens);

        unset($activeperiod, $windowperiod, $timecompleted);

        // Certification is using CERTIFRECERT_COMPLETION to calculate the next expiry date.
        // User is in the primary certification stage, with no assignment due date set.
        $activeperiod = '1 year';
        $windowperiod = '1 month';
        $timecompleted = strtotime('15-April-2013 12:01');

        $base = get_certiftimebase(CERTIFRECERT_COMPLETION, 0, $timecompleted, $activeperiod, $windowperiod);
        $newtimeexpires = get_timeexpires($base, $activeperiod);
        $timewindowopens = get_timewindowopens($newtimeexpires, $windowperiod);

        $this->assertEquals(strtotime('15-April-2014 12:01'), $newtimeexpires); // One year after the completion date.
        $this->assertEquals(strtotime('15-March-2014 12:01'), $timewindowopens);

        unset($activeperiod, $windowperiod, $timecompleted);

        // Certification is using CERTIFRECERT_COMPLETION to calculate the next expiry date (window period is weeks).
        // User is in the recertification stage.
        $activeperiod = '1 year';
        $windowperiod = '3 week';
        $curtimeexpires = strtotime('3-May-2013 08:14');
        $timecompleted = strtotime('15-April-2013 12:01');

        $base = get_certiftimebase(CERTIFRECERT_COMPLETION, $curtimeexpires, $timecompleted, $activeperiod, $windowperiod);
        $newtimeexpires = get_timeexpires($base, $activeperiod);
        $timewindowopens = get_timewindowopens($newtimeexpires, $windowperiod);

        $this->assertEquals(strtotime('15-April-2014 12:01'), $newtimeexpires); // One year after the completion date.
        $this->assertEquals(strtotime('25-March-2014 12:01'), $timewindowopens);

        unset($activeperiod, $windowperiod, $curtimeexpires, $timecompleted);

    }
}
