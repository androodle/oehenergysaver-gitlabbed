<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/*
 * @package    format
 * @subpackage tiles
 * @author     Greg Newton, Androgogic <greg.newton@androgogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  2014 Androgogic, Ltd.
 *
 * TODO: Description goes here
 */

require_once('../../../config.php');
require_once($CFG->dirroot . '/course/format/tiles/locallib.php');

require_login();

$courseid = required_param('courseid', PARAM_INT);
$sectionid = required_param('sectionid', PARAM_INT);

$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_url('/format/tiles/removeimage.php');
$PAGE->set_heading($SITE->fullname);
$PAGE->set_pagelayout('course');
$PAGE->set_title(get_string('remove_background', 'format_tiles'));
$PAGE->navbar->add(get_string('remove_background', 'format_tiles'));

require_capability('format/tiles:changetilebackground', $context);

require_once('./forms/removeimage_form.php');

$form = new removeimage_form(null, array("courseid" => $courseid, "sectionid" => $sectionid));

if ($form->is_cancelled()) {
    // Go back to course if cancelled.
    redirect(new moodle_url($CFG->wwwroot . '/course/view.php', array('id' => $courseid)));
} else if ($formdata = $form->get_data()) {
    // Submitted
    $existing = $DB->get_record('format_tiles_tile_image', array('courseid' => $courseid, 'sectionid' => $sectionid));
    if ($existing) {
        $coursecontext = context_course::instance($courseid);
        $fs = get_file_storage();
        $file = $fs->get_file($coursecontext->id, 'course', 'section', $sectionid,
            '/format_tiles/', $existing->filename);
        if ($file) {
            $file->delete();
        }
        $DB->delete_records('format_tiles_tile_image', array('courseid' => $courseid, 'sectionid' => $sectionid));
    }
    redirect(new moodle_url($CFG->wwwroot . '/course/view.php', array('id' => $courseid)));
}

// Output content.
echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('remove_background', 'format_tiles'), 3, 'main');
if (!empty($errormessage)) {
    echo $errormessage;
}

$form->display();

echo $OUTPUT->footer();
