<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/*
 * @package    format
 * @subpackage tiles
 * @author     Greg Newton, Androgogic <greg.newton@androgogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright  2014 Androgogic, Ltd.
 *
 * TODO: Description goes here
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

    $settings->add(new admin_setting_heading(
                        'format_tiles_bottom_tabs',
                        get_string('bottom_tabs', 'format_tiles'),
                        ''
                    ));
    $settings->add(new admin_setting_configcheckbox(
                        'format_tiles/allow_bottom_tabs',
                        get_string('allow_bottom_tabs', 'format_tiles'),
                        get_string('allow_bottom_tabs_desc', 'format_tiles'),
                        0
                    ));
    $settings->add(new admin_setting_configcheckbox(
                        'format_tiles/bottom_tabs_default',
                        get_string('bottom_tabs_default', 'format_tiles'),
                        get_string('bottom_tabs_default_desc', 'format_tiles'),
                        0
                    ));
    $settings->add(new admin_setting_heading(
                        'format_tiles_title_tabs',
                        get_string('title_tabs', 'format_tiles'),
                        ''
                    ));
    $settings->add(new admin_setting_configcheckbox(
                        'format_tiles/allow_title_tabs',
                        get_string('allow_title_tabs', 'format_tiles'),
                        get_string('allow_title_tabs_desc', 'format_tiles'),
                        0
                    ));
    $settings->add(new admin_setting_configcheckbox(
                        'format_tiles/title_tabs_default',
                        get_string('title_tabs_default', 'format_tiles'),
                        get_string('title_tabs_default_desc', 'format_tiles'),
                        0
                    ));
    $settings->add(new admin_setting_configtext(
                        'format_tiles/max_title_length',
                        get_string('max_title_length', 'format_tiles'),
                        get_string('max_title_length_desc', 'format_tiles'),
                        '20',
                        PARAM_INT
                    ));
    $settings->add(new admin_setting_heading(
                        'format_tiles_allsectionsview',
                        get_string('allsectionsview', 'format_tiles'),
                        ''
                    ));
    $settings->add(new admin_setting_configcheckbox(
                        'format_tiles/allow_all_sections_view',
                        get_string('allow_all_sections_view', 'format_tiles'),
                        get_string('allow_all_sections_view_desc', 'format_tiles'),
                        0
                    ));
    $settings->add(new admin_setting_configcheckbox(
                        'format_tiles/allow_all_sections_tile',
                        get_string('allow_all_sections_tile', 'format_tiles'),
                        get_string('allow_all_sections_tile_desc', 'format_tiles'),
                        0
                    ));
/*    $settings->add(new admin_setting_configcheckbox(
                        'format_tiles/all_sections_default',
                        get_string('all_sections_default', 'format_tiles'),
                        get_string('all_sections_default_desc', 'format_tiles'),
                        0
                    ));*/
    $settings->add(new admin_setting_configselect(
                        'format_tiles/all_sections_default',
                        get_string('all_sections_default', 'format_tiles'),
                        get_string('all_sections_default_desc', 'format_tiles'),
                        0,
                        array(
                            '0'=>get_string('allsect_show_neither', 'format_tiles'),
                            '1'=>get_string('allsect_show_tab_only', 'format_tiles'),
                            '2'=>get_string('allsect_show_both', 'format_tiles')
                    )
                ));
    $settings->add(new admin_setting_heading(
                        'format_tiles_custom_tiles',
                        get_string('custom_tiles', 'format_tiles'),
                        ''
                    ));
    $settings->add(new admin_setting_configcheckbox(
                        'format_tiles/allow_custom_tiles',
                        get_string('allow_custom_tiles', 'format_tiles'),
                        get_string('allow_custom_tiles_desc', 'format_tiles'),
                        0
                    ));
}
